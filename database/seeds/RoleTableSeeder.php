<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = ['Super Admin', 'Admin', 'Operation Manager', 'Attorney', 'User'];

        Schema::disableForeignKeyConstraints();
        DB::table('roles')->truncate();
        $faker = Faker::create();
        foreach ($roles as $role) {

            $first_name = $faker->name('male');
            $last_name = $faker->name('male');
            DB::table('roles')->insert([
                'name' => $role,
                'guard_name' => 'api',
                'created_at' => now(),
            ]);
        }
        Schema::enableForeignKeyConstraints();
        
    }
}
