<?php

use Illuminate\Database\Seeder;
use App\Helpers\Helper;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Hash;
use App\Models\User\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        // DB::table('users')->truncate();
        $faker = Faker::create();
        foreach (range(1,20) as $index) {

            $user = User::create([
                'email' => $faker->safeEmail,
                'email_verified_at' => $faker->date(),
                'gender' => $faker->numberBetween(1,3),
                'is_block' => 0,
                'status' => 1,//$faker->numberBetween(0,1),
                'created_by' => 1,
                'deleted_at'=> null,
                'created_at' => now(),
                'password' => Hash::make('legalzoom123')
            ]);

            $user->assignRole(5);
        }
        Schema::enableForeignKeyConstraints();
    }
}
