<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnsAddOptionExps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('add_option_exps', function (Blueprint $table) {
            $table->dropColumn('fm_adoption_cost_checkbox');
            $table->tinyInteger('fm_add_adoptions_checkbox')->nullable()->comment("0=not,1=yes")->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
