<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRoleColumnToUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('role')->default('USER');
        });
        Schema::disableForeignKeyConstraints();

        DB::table('users')->truncate();
        DB::table('users')->insertGetId([
            'email' => 'admin@aitaxonline.com',
            'email_verified_at' => now(),
            'password' => Hash::make('Adm0N12..!!Ang'),
            'current_device' => '',
            'gender' => 1,
            'is_block' => 0,
            'status' => 1,
            'created_at' => now(),
            'deleted_at' => null,
            'created_by' => 1,
            'role' => 'ADMIN'
        ]);
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('users', 'role')) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('role');
            });
        }
    }
}
