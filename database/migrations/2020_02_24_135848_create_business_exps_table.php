<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBusinessExpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_exps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('ebe_total_parking_fees_radio')->nullable()->comment("0=not,1=yes")->default(0);
            $table->tinyInteger('ebe_tollway_costs_radio')->nullable()->comment("0=not,1=yes")->default(0);
            $table->tinyInteger('ebe_total_local_transportation_radio')->nullable()->comment("0=not,1=yes")->default(0);
            $table->tinyInteger('ebe_total_travel_costs_radio')->nullable()->comment("0=not,1=yes")->default(0);

            $table->integer('ebe_total_parking_fees')->nullable();
            $table->integer('ebe_tollway_costs')->nullable();
            $table->integer('ebe_total_local_transportation')->nullable();
            $table->integer('ebe_total_travel_costs')->nullable();


            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('updated_by')->unsigned()->nullable();
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('session_id')->unsigned();
            $table->foreign('session_id')->references('id')->on('user_sessions')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_exps');
    }
}
