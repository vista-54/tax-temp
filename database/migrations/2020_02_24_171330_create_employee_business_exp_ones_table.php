<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeBusinessExpOnesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_business_exp_ones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('et_office_furniture_description_radio')->nullable()->default(0)->comment("0=not,1=yes");
            $table->tinyInteger('et_office_equipment_description_radio')->nullable()->default(0)->comment("0=not,1=yes");
            $table->tinyInteger('et_seeking_expense_description_radio')->nullable()->default(0)->comment("0=not,1=yes");

            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('updated_by')->unsigned()->nullable();
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('session_id')->unsigned();
            $table->foreign('session_id')->references('id')->on('user_sessions')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_business_exp_ones');
    }
}
