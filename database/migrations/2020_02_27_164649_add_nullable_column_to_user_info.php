<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNullableColumnToUserInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_additional_info', function (Blueprint $table) {
            $table->boolean('plan_to_change_immigration')->nullable()->default('0')->change();
            $table->boolean('plan_to_depart')->nullable()->default('0')->change();
            $table->boolean('estate_plan_in_place')->nullable()->default('0')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_additional_info', function (Blueprint $table) {
            //
        });
    }
}
