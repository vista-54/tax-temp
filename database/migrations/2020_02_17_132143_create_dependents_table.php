<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDependentsTable extends Migration {

	public function up()
	{
		Schema::create('dependents', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('di_name', 255)->nullable();
			$table->date('di_dob')->nullable();
			$table->string('di_relation', 255)->nullable();
			$table->string('di_days_lived', 100)->nullable();
			$table->string('di_itin', 100)->nullable();
			$table->tinyInteger('di_married_as')->default('0');
			$table->tinyInteger('di_fulltime_student')->default('0');
			$table->tinyInteger('di_received_income')->default('0');
			$table->bigInteger('user_id')->unsigned()->nullable();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
			$table->bigInteger('created_by')->unsigned()->nullable();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('updated_by')->unsigned()->nullable();
			$table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
			$table->timestamps();
			$table->softDeletes()->index();
		});
	}

	public function down()
	{
		Schema::drop('dependents');
	}
}