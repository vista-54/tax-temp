<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLeaseColumnForVehicleExp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vehicle_exps', function (Blueprint $table) {
            $table->tinyInteger('own_lease')->nullable()->comment("0=not,1=yes")->default(0);
            $table->tinyInteger('spouse_own_lease')->nullable()->comment("0=not,1=yes")->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
