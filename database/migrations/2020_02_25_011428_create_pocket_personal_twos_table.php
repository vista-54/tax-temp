<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePocketPersonalTwosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pocket_personal_twos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('out_of_pocket_medications_radio')->nullable()->comment("0=not,1=yes")->default(0);
            $table->integer('out_of_pocket_medications')->nullable();
            $table->tinyInteger('out_of_pocket_dental_radio')->nullable()->comment("0=not,1=yes")->default(0);
            $table->integer('out_of_pocket_dental')->nullable();
            $table->tinyInteger('out_of_pocket_other_radio')->nullable()->comment("0=not,1=yes")->default(0);
            $table->integer('out_of_pocket_other')->nullable();


            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('updated_by')->unsigned()->nullable();
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('session_id')->unsigned();
            $table->foreign('session_id')->references('id')->on('user_sessions')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pocket_personal_twos');
    }
}
