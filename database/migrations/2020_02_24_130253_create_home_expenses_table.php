<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHomeExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_expenses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('hm_own_rent')->comment("0=not,1=yes")->default(0);
            $table->tinyInteger('hm_mortage_interest')->comment("0=not,1=yes")->default(0);
            $table->tinyInteger('hm_real_estate_tax_checkbox')->nullable()->comment("0=not,1=yes")->default(0);
            $table->tinyInteger('hm_energy_efficient_upgrades_radio')->nullable()->comment("0=not,1=yes")->default(0);
            //need rewrite to integerf
            $table->string('hm_real_estate_tax')->nullable();
            $table->string('hm_mortgage_insurance')->nullable();
            $table->string('hm_loan_mandated_homeowner_ins')->nullable();
            $table->string('hm_energy_efficient_total_amount')->nullable();

            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('updated_by')->unsigned()->nullable();
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('session_id')->unsigned();
            $table->foreign('session_id')->references('id')->on('user_sessions')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_expenses');
    }
}
