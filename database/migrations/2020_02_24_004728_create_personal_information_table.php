<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_information', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('last_name');
            $table->string('email');
            $table->string('phone');
            $table->date('date_of_birth');
            $table->string('job_title');
            $table->text('street_address');
            $table->string('country');
            $table->string('city');
            $table->string('state');
            $table->tinyInteger('citizenship_status')->comment("0=US Citizen,1=Green Card Holder,2=Visa Holder")->default(0);
            $table->string('visa_type')->comment("If selectedVisa")->nullable();
            $table->string('citizenship_other')->nullable();
            $table->tinyInteger('marital_status')->comment("0=Unmarried,1=Married")->default(0);
            $table->tinyInteger('active_military')->comment("0=No,1=Yes")->default(0);
            $table->string('active_military_state')->nullable();

            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('updated_by')->unsigned()->nullable();
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');

            $table->bigInteger('session_id')->unsigned();
            $table->foreign('session_id')->references('id')->on('user_sessions')->onDelete('cascade');
            $table->softDeletes()->index();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal_information');
    }
}
