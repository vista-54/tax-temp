<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehicleExpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_exps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ve_make');
            $table->string('ve_model');
            $table->integer('ve_price');

            $table->string('ve_spouse_make')->nullable();
            $table->string('ve_spouse_model')->nullable();
            $table->integer('ve_spouse_price')->nullable();


            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('updated_by')->unsigned()->nullable();
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('session_id')->unsigned();
            $table->foreign('session_id')->references('id')->on('user_sessions')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_exps');
    }
}
