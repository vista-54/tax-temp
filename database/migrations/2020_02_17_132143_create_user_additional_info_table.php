<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserAdditionalInfoTable extends Migration {

	public function up()
	{
		Schema::create('user_additional_info', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->date('us_tax_residency')->nullable();
			$table->tinyInteger('plan_to_change_immigration')->default('0');
			$table->tinyInteger('plan_to_depart')->default('0');
			$table->tinyInteger('estate_plan_in_place')->default('0');
			$table->bigInteger('created_by')->unsigned()->nullable();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('updated_by')->unsigned()->nullable();
			$table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
			$table->timestamps();
			$table->softDeletes()->index();
		});
	}

	public function down()
	{
		Schema::drop('user_additional_info');
	}
}