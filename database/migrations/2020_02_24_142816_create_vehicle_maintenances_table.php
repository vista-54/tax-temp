<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehicleMaintenancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_maintenances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('eve_weekly_estimated_total');
            $table->integer('eve_spouse_weekly_estimated_total')->nullable();
            $table->integer('eve_no_Oil_Changes');
            $table->integer('eve_Cost_Per_Change');
            $table->integer('eve_spouse_no_Oil_Changes')->nullable();
            $table->integer('eve_spouse_Cost_Per_Change')->nullable();
            $table->tinyInteger('eve_Total_Tire_Costs_radio')->nullable()->default(0);
            $table->integer('eve_Total_Tire_Costs')->nullable();
            $table->integer('eve_spouse_Total_Tire_Costs')->nullable();
            $table->tinyInteger('eve_Total_Repairs_Cost_radio')->nullable()->default(0);
            $table->integer('eve_Total_Repairs_Cost')->nullable();
            $table->integer('eve_spouse_Total_Repairs_Cost')->nullable();

            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('updated_by')->unsigned()->nullable();
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('session_id')->unsigned();
            $table->foreign('session_id')->references('id')->on('user_sessions')->onDelete('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_maintenances');
    }
}
