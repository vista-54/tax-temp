<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveColumnsAdditionalSpouseInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_additional_info', function (Blueprint $table) {
            $table->string('us_tax_residency_spouse')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('user_additional_info', 'us_tax_residency_spouse')) {
            Schema::table('user_additional_info', function (Blueprint $table) {
                $table->dropColumn('us_tax_residency_spouse');
            });
        }
    }
}
