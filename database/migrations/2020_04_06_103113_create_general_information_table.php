<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGeneralInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_information', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('is_student_loan')->nullable()->comment("0=not,1=yes")->default(0);
            $table->tinyInteger('is_donated_charity')->nullable()->comment("0=not,1=yes")->default(0);
            $table->tinyInteger('is_own_home')->nullable()->comment("0=not,1=yes")->default(0);
            $table->tinyInteger('is_kid_dependent')->nullable()->comment("0=not,1=yes")->default(0);
            $table->tinyInteger('is_vehicle')->nullable()->comment("0=not,1=yes")->default(0);
            $table->tinyInteger('is_emergency_personal')->nullable()->comment("0=not,1=yes")->default(0);
            
            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('updated_by')->unsigned()->nullable();
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('session_id')->unsigned();
            $table->foreign('session_id')->references('id')->on('user_sessions')->onDelete('cascade');
            
            $table->softDeletes()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('general_information');
    }
}
