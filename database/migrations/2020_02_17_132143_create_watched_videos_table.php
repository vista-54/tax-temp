<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWatchedVideosTable extends Migration {

	public function up()
	{
		Schema::create('watched_videos', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->tinyInteger('annual_tax_video')->default('0');
			$table->tinyInteger('cohan_rule_video')->default('0');
			$table->bigInteger('user_id')->unsigned()->nullable();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
			$table->bigInteger('created_by')->unsigned()->nullable();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('updated_by')->unsigned()->nullable();
			$table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
			$table->timestamps();
			$table->softDeletes()->index();
		});
	}

	public function down()
	{
		Schema::drop('watched_videos');
	}
}