<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlueCollarExpTwosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blue_collar_exp_twos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('expenses_uniforms_radio')->nullable()->comment("0=not,1=yes")->default(0);
            $table->tinyInteger('work_clothing_radio')->nullable()->comment("0=not,1=yes")->default(0);
            $table->tinyInteger('dry_cleaning_radio')->nullable()->comment("0=not,1=yes")->default(0);



            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('updated_by')->unsigned()->nullable();
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('session_id')->unsigned();
            $table->foreign('session_id')->references('id')->on('user_sessions')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blue_collar_exp_twos');
    }
}
