<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeBusinessExpThreesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_business_exp_threes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('exams_or_licence_radio')->nullable()->default(0)->comment("0=not,1=yes");
            $table->tinyInteger('membership_fees_radio')->nullable()->default(0)->comment("0=not,1=yes");
            $table->integer('ets_Monthly_Cell_Phone_Bill');
            $table->integer('ets_Spouse_Monthly_Cell_Phone_Bill')->nullable();
            $table->integer('ets_Networking_Expenses')->nullable();
            $table->integer('ets_spouse_Networking_Expenses')->nullable();

            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('updated_by')->unsigned()->nullable();
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('session_id')->unsigned();
            $table->foreign('session_id')->references('id')->on('user_sessions')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_business_exp_threes');
    }
}
