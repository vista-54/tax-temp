<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDependantInformationChildrenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dependant_information_children', function (Blueprint $table) {
            $table->bigInteger('dependent_id')->unsigned();
            $table->foreign('dependent_id')->references('id')->on('dependant_information')->onDelete('CASCADE');
            $table->string('full_legal_name');
            $table->date('date_of_birth');
            $table->string('relation');
            $table->integer('month_lived');
            $table->tinyInteger('married_as')->default(0);
            $table->string('ssn');
            $table->tinyInteger('fulltime_student')->default(0);
            $table->tinyInteger('received_income')->default(0);

            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('updated_by')->unsigned()->nullable();
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('session_id')->unsigned();
            $table->foreign('session_id')->references('id')->on('user_sessions')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dependant_information_children');
    }
}
