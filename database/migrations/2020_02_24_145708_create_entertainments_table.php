<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntertainmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entertainments', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->tinyInteger('eme_time_per_week_radio')->nullable()->comment("0=not,1=yes")->default(0);
            $table->tinyInteger('eme_gift_Description_radio')->nullable()->comment("0=not,1=yes")->default(0);
            $table->integer('eme_time_per_week');
            $table->integer('eme_meal_cost');
            $table->integer('eme_spouse_time_per_week')->nullable();
            $table->integer('eme_spouse_meal_cost')->nullable();



            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('updated_by')->unsigned()->nullable();
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('session_id')->unsigned();
            $table->foreign('session_id')->references('id')->on('user_sessions')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entertainments');
    }
}
