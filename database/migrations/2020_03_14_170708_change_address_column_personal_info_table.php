<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeAddressColumnPersonalInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //personal_information
        Schema::table('personal_information', function (Blueprint $table) {
            $table->string('address');
            $table->text('street_address')->nullable()->change();
            $table->string('country')->nullable()->change();;
            $table->string('city')->nullable()->change();;
            $table->string('state')->nullable()->change();;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('personal_information', function (Blueprint $table) {
            $table->dropColumn('address');
        });
    }
}
