<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSmallItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('small_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('sii_traditional_IRA_contributions_radio')->nullable()->comment("0=not,1=yes")->default(0);
            $table->tinyInteger('are_you_teacher_radio')->nullable()->comment("0=not,1=yes")->default(0);
            $table->tinyInteger('sii_self_employed_radio')->nullable()->comment("0=not,1=yes")->default(0);
            $table->tinyInteger('sii_educator_expenses_radio')->nullable()->comment("0=not,1=yes")->default(0);
            $table->integer('sii_traditional_IRA_contributions')->nullable();
            $table->integer('sii_educator_expenses')->nullable();
            $table->integer('sii_roth_IRA_contributions')->nullable();
            $table->integer('sii_total_mortgage_payments')->nullable();
            $table->integer('sii_total_HOA')->nullable();
            $table->integer('sii_total_utilities')->nullable();


            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('updated_by')->unsigned()->nullable();
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('session_id')->unsigned();
            $table->foreign('session_id')->references('id')->on('user_sessions')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('small_items');
    }
}
