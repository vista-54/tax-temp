<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepositInfromationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deposit_infromations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('refund_deposit')->nullable()->default(0);
            $table->tinyInteger('refund_between_acc')->nullable()->default(0);
            $table->string('bank_routing')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('account_number')->nullable();
            $table->string('account_owner')->nullable();
            $table->string('secondary_bank')->nullable();
            $table->string('secondary_account_owner')->nullable();
            $table->string('mailing_address')->nullable();
            $table->string('swift')->nullable();
            $table->string('bsb_number')->nullable();
            $table->tinyInteger('address_record')->nullable()->default(0);
            $table->tinyInteger('address_record_than')->nullable()->default(0);
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('country')->nullable();


            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('updated_by')->unsigned()->nullable();
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('session_id')->unsigned();
            $table->foreign('session_id')->references('id')->on('user_sessions')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposit_infromations');
    }
}
