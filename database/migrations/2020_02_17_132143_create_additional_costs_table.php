<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdditionalCostsTable extends Migration {

	public function up()
	{
		Schema::create('additional_costs', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('cost_type', 255)->nullable();
			$table->string('cost_description', 255)->nullable();
			$table->decimal('cost_amount', 9,3)->nullable();
			$table->bigInteger('user_id')->unsigned()->nullable();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
			$table->bigInteger('created_by')->unsigned()->nullable();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('updated_by')->unsigned()->nullable();
			$table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
			$table->timestamps();
			$table->softDeletes()->index();
		});
	}

	public function down()
	{
		Schema::drop('additional_costs');
	}
}