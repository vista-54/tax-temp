<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmergencyPersonalExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emergency_personal_expenses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('weapons_radio')->nullable()->comment("0=not,1=yes")->default(0);
            $table->string('weapons_description')->nullable();
            $table->integer('weapons_cost')->nullable();
            $table->string('emergency_description');
            $table->integer('emergency_cost');
            $table->string('training_description');
            $table->integer('training_cost');


            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('updated_by')->unsigned()->nullable();
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('session_id')->unsigned();
            $table->foreign('session_id')->references('id')->on('user_sessions')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emergency_personal_expenses');
    }
}
