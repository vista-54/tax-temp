<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlueCollarExpTwoChildrenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blue_collar_exp_two_children', function (Blueprint $table) {
            $table->bigInteger('blue_collar_two_id')->unsigned();
            $table->foreign('blue_collar_two_id')->references('id')->on('blue_collar_exp_twos')->onDelete('CASCADE');
            $table->string('type')->nullable();
            $table->string('cost_description')->nullable();
            $table->integer('cost_value')->nullable();


            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('updated_by')->unsigned()->nullable();
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('session_id')->unsigned();
            $table->foreign('session_id')->references('id')->on('user_sessions')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blue_collar_exp_two_children');
    }
}
