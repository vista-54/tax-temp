@php
                if($viewData->doc_pref_singlemember_multimember == "single-member"){
                    if($viewData->doc_pref_refer_to_owner == "members"){
                        $member = 'Member';
                        $The_member = 'The Member';
                        $the_member = 'the Member';
                        $member_has = 'Member has';
                        $member_accepts = 'Member accepts';
                        $member_determines = 'Member determines';
                        $member_its = "its";
                        $member_is = "Member is";
                        $member_individual = "No individual";
                    }else if($viewData->doc_pref_refer_to_owner == "unitholders"){
                        $member = 'Unitholder';
                        $The_member = 'The Unitholder';
                        $the_member = 'the Unitholder';
                        $member_has = 'Unitholder has';
                        $member_accepts = 'Unitholder accepts';
                        $member_determines = 'Unitholder determines';
                        $member_its = "its";
                        $member_is = "Unitholder is";
                        $member_individual = "No individual";
                    }
                }else{
                    if($viewData->doc_pref_refer_to_owner == "members"){
                        $member = 'Members';
                        $The_member = 'The Members';
                        $the_member = 'the Members';
                        $member_has = 'Member have';
                        $member_accepts = 'Members accept';
                        $member_determines = 'Members determine';
                        $member_its = "its";
                        $member_is = "Members are";
                        $member_individual = "No individual";
                    }else if($viewData->doc_pref_refer_to_owner == "unitholders"){
                        $member = 'Unitholder';
                        $The_member = 'The Unitholders';
                        $the_member = 'the Unitholders';
                        $member_has = 'Unitholders have';
                        $member_accepts = 'Unitholders accept';
                        $member_determines = 'Unitholders determine';
                        $member_its = "its";
                        $member_is = "Unitholders are";
                        $member_individual = "No individual";
                    }
                }
                
                if($viewData->doc_pref_refer_to_business == "copmany"){
                    $company = "Company";
                    $company_determines = "Company determines";
                }else if($viewData->doc_pref_refer_to_business == "llc"){
                    $company = "LLC";
                    $company_determines = "LLC determines";
                }else{
                    $company = "Company";
                    $company_determines = "Company determines";
                }
                
                if($viewData->doc_pref_refer_to_equity == "interests"){
                    $interests = "Interests";
                    $interest = "Interest";
                    $interest_is = "Interest is";
                    $interest_an = "an Interest";
                }else if($viewData->doc_pref_refer_to_equity == "membership interests"){
                    $interests = "Membership Interests";
                    $interest = "Membership Interest";
                    $interest_is = "Membership Interest is";
                    $interest_an = "a Membership Interest";
                }else if($viewData->doc_pref_refer_to_equity == "units"){
                    $interests = "Units";
                    $interest = "Unit";
                    $interest_is = "Unit is";
                    $interest_an = "a Unit";
                }

                if($viewData->doc_pref_title_llc_operating_agreement == "operating agreement"){
                    $operating_agreement = "Operating Agreement";
                }else if($viewData->doc_pref_title_llc_operating_agreement == "llc agreement"){
                    $operating_agreement = "LLC +Agreement";
                }else if($viewData->doc_pref_title_llc_operating_agreement == "company operating agreement"){
                    $operating_agreement = "Company Operating Agreement";
                } 

                if($viewData->comp_formation_title_of_doc_filed_form_company == "articles of organization"){
                    $title_of_doc_filed = "Articles of Organization";
                }else if($viewData->comp_formation_title_of_doc_filed_form_company == "certificate of formation"){
                    $title_of_doc_filed = "Certificate of Formation";
                }else if($viewData->comp_formation_title_of_doc_filed_form_company == "certificate of organization"){
                    $title_of_doc_filed = "Certificate of Organization";
                }else if($viewData->comp_formation_title_of_doc_filed_form_company == "organizer statement"){
                    $title_of_doc_filed = "Organizer Statement";
                }       

                if(!empty($viewData->comp_formation_company_formed_in)){
                    $comp_formation_state_limited_act = $viewData->comp_formation_state_limited_act;
                    $company_formed_state = $viewData->comp_formation_company_formed_in;
                    $company_formed_state_of = "State of ".$viewData->comp_formation_company_formed_in;

                }

                $member_info_member_name = $viewData->member_info_mermber_name;
                $member_info_initial_capital_amount = isset($viewData->member_info_amount_of_initial_capital_contribution) ? $viewData->member_info_amount_of_initial_capital_contribution : '';
                $member_info_contribution_date = $viewData->member_info_contribution_date;
                $member_info_assumed_debt = $viewData->member_info_assumed_debt;
                $member_info_tax_basis = $viewData->member_info_tax_basis;
                $entity_info_entity_name = ucwords($viewData->entity_info_entity_name);
                $entity_info_street = ucwords($viewData->entity_info_street_address);
                $entity_info_city = ucwords($viewData->entity_info_city);
                $entity_info_state = ucwords($viewData->entity_info_state);
                $entity_info_zip = $viewData->entity_info_zip;
                $entity_info_registered_agent_title = $viewData->entity_info_registered_agent_title;
                $attorney_name = $viewData->attorney_attorney_name;
                $attorney_street = $viewData->attorney_street_address;
                $attorney_city = $viewData->attorney_city;
                $attorney_state = $viewData->attorney_state;
                $attorney_zip = $viewData->attorney_zip;

                $attorney_firm_name = strtoupper($viewData->attorney_firm_name);
                $attorney_firm_description = strtoupper($viewData->attorney_firm_description);
                $attorney_street_address = strtoupper($viewData->attorney_street_address);
                $attorney_city = strtoupper($viewData->attorney_city);
                $attorney_state = strtoupper($viewData->attorney_state);
                $attorney_zip = strtoupper($viewData->attorney_zip);
                $signing_info_signing_date_of_operating_agreement = isset($viewData->signing_info_signing_date_of_operating_agreement) ? $viewData->signing_info_signing_date_of_operating_agreement : '';
            @endphp










<html>
    <head>
        <title>Operating Agreement</title>
        <style>
            @page {
                /* margin: 220px 60px 103px 60px; */
                /* header: html_MyCustomHeader; */
                footer: html_MyCustomFooter;
            }
            @page :first {
                footer: html_MyFooter;
            }
        </style>
    </head>
    <body>
        <htmlpagefooter name="MyFooter">
            <p>  </p>
        </htmlpagefooter>
        <htmlpagefooter name="MyCustomFooter">
            <p style="text-align:center;"> {{$operating_agreement}} of {{$entity_info_entity_name}} </p>
            <p style="text-align:center;">{PAGENO} of {nbpg}</p>
        </htmlpagefooter>
        <div>
            <p>
                
                <h1 style="text-align:center;">{{ $operating_agreement }}
                <br/>
                of
                <br/>
                {{$entity_info_entity_name}}</h1>
            
            </p>
            <p style="text-align:center;">
            <h1 style="text-align:center;">A {{ $entity_info_state }} Limited Liability Company</h1>
            </p>
            <p style="text-align:center; margin-top:500px;">
                LAW OFFICES
            </p>
            <p style="text-align:center;">
                {{$attorney_firm_name}}
            </p>
            <p style="text-align:center;">
                {{$attorney_firm_description}}
            </p>
            <p style="text-align:center;">
                {{$attorney_street_address}}
                <br/>
                {{$attorney_city}}, {{$attorney_state}} {{$attorney_zip}}
            </p>
        </div>
        <pagebreak>
        <div>
            <p style="text-align:center;">
                <h1 style="text-align:center;">
                    {{ $operating_agreement }}
                <br/>
                of
                <br/>
                {{$entity_info_entity_name}}
            </h1>
            </p>
            <p style="text-align:center;">
                <h1 style="text-align:center;">
                Table of Contents
            </h1>
            </p>
        </div>
        <pagebreak>
        <br clear="all"/>
        <div>
            <p style="text-align:center;">
                <h1 style="text-align:center;">{{ $operating_agreement }}
                    <br/>
                    of
                    <br/>
                    {{$entity_info_entity_name}}</h1>
            </p>
            <p>
                This {{ $operating_agreement }} (<em>Agreement</em>) of {{$entity_info_entity_name}}, a
                {{ $entity_info_state }} limited liability company (<em>{{$company}}</em>), is made by the
     to provide for the governance and operations of the {{$company}}. The
     shall serve as the so of the {{$company}}. This Agreement is
                effective on {{$signing_info_signing_date_of_operating_agreement}}, and will apply to any Addition
                admitted in accordance with its terms.
            </p>
            <h1 style="text-align:center;">
                Article 1
                <br/>
                Definitions and Interpretation
            </h1>
            <h2>
                
                Section 1.01 Definitions
            </h2>
            <p>
                For purposes of this Agreement, the following terms have the following
                meanings.
            </p>
            <h3>
                (a) Act
            </h3>
            <p>
                <em>Act</em>
                means the {{ $comp_formation_state_limited_act }}, as amended from time
                to time.
            </p>
            <h3>
                (b) Addition
            </h3>
            <p>
                <em>Additional </em>
                <</em>
                means any person not previously who acquires {{$viewData->doc_pref_refer_to_equity}} and
                is admitted as.
            </p>
            <h3>
                (c) Affiliate
            </h3>
            <p>
                <em>Affiliate</em>
                means any of the following persons or any person who controls, is
                controlled by, or is under common control with any of the following
                persons:
            </p>
            <p>
                 a {{$member}};
            </p>
            <p>
                a {{$member}}'s Immediate Family member; or
            </p>
            <p>
                a Legal Representative, successor, Assignee, or trust for the benefit
                of {{$member}} or the {{$member}}'s Immediate Family members.
            </p>
            <p>
                For purposes of this definition, <em>control </em>means the direct or
                indirect power to direct or cause the direction of the person’s
                management and policies, whether by owning voting securities,
                partnership, or other ownership interests; by contract; or otherwise.
            </p>
            <h3>
                (d) Agreement
            </h3>
            <p>
                <em>Agreement</em>
                means this {{ $operating_agreement }}, as amended from time to time.
            </p>
            <h3>
                (e) Applicable Law
            </h3>
            <p>
                <em>Applicable Law</em>
                means the Act, the Code, the Securities Act, all pertinent provisions
                of any agreements with any Governmental Authority and all pertinent
                provisions of any Governmental Authority’s:
            </p>
            <p>
                constitutions, treaties, statutes, laws, common law, rules,
                regulations, decrees, ordinances, codes, proclamations, declarations,
                or orders;
            </p>
            <p>
                consents or approvals; and
            </p>
            <p>
                orders, decisions, advisory opinions, interpretative opinions,
                injunctions, judgments, awards, and decrees.
            </p>
            <h3>
                (f) {{ucwords($viewData->comp_formation_title_of_doc_filed_form_company)}} 
            </h3>
            <p>
                <em>{{ucwords($viewData->comp_formation_title_of_doc_filed_form_company)}} </em>
                means the {{ucwords($viewData->comp_formation_title_of_doc_filed_form_company)}} filed with the {{$viewData->comp_formation_company_formed_in}} of
                State as required by the Act, or any other similar instrument required
                to be filed by the laws of any other state in which the {{$company}} intends
                to conduct business.
            </p>
            <h3>
                (g) Assignee
            </h3>
            <p>
                <em>Assignee</em>
                means the recipient of {{$viewData->doc_pref_refer_to_equity}} by assignment.
            </p>
            <h3>
                (h) Capital Contribution
            </h3>
            <p>
                <em>Capital Contribution</em>
                means the total cash and other consideration contributed and agreed to
        be contributed to the {{$company}} by t. Each initial        <em>Capital Contribution</em> is shown in the Schedule A, attached and
        incorporated into this Agreement. Additional        <em>Capital Contribution</em> means the total cash and other
                consideration contributed to the {{$company}} by t (including any
                Addition) other than the initial Capital Contribution. Any
                reference in this Agreement to the Capital Contribution of a current
     includes any Capital Contribution previously made by any prior
     regarding that {{$member}}'s  Interest. The value of a {{$member}}'s
                Capital Contribution is the amount of cash plus the Fair Market Value
                of other property contributed to the {{$company}}.
            </p>
            <h3>
                (i) Code
            </h3>
            <p>
                References to the <em>Code</em> or to its provisions are to the
                Internal Revenue Code of 1986, as amended from time to time, and any
        corresponding Treasury Regulations. References to the        <em>Treasury Regulations</em> are to the Treasury Regulations under the
                Code in effect. If a particular provision of the Code is renumbered or
                a subsequent federal tax law supersedes the Code, any reference is to
                the renumbered provision or to the corresponding provision of the
                subsequent law, unless the result would be clearly contrary to the
                {{$member}}'s  intent as expressed in this Agreement. The same rule applies
                to Treasury Regulations references.
            </p>
            <h3>
                (j) Company
            </h3>
            <p>
                <em>Company</em>
                means {{$entity_info_city}}, a {{ $entity_info_state }} limited liability company.
            </p>
            <h3>
                (k) Fair Market Value
            </h3>
            <p>
                <em>Fair Market Value</em>
                is defined in Section 10.07.
            </p>
            <h3>
                (l) Governmental Authority
            </h3>
            <p>
                <em>Governmental Authority</em>
                means any local, state, federal, or foreign government or its political
                subdivision; any agency or instrumentality of a government or its
                political subdivision; or any self-regulated organization or other
                nongovernmental regulatory authority or quasi-Governmental Authority
                whose rules, regulations, or orders have the force of law. Governmental
                Authority also means any arbitrator, court, or tribunal of competent
                jurisdiction.
            </p>
            <h3>
                (m) Immediate Family
            </h3>
            <p>
                <em>Immediate Family</em>
                means any Member’s spouse (but not a spouse who is legally separated
                from the person under a decree of divorce or separate maintenance),
                parents, parents-in-law, descendants (including descendants by
                adoption), spouses of descendants (but not a spouse who is legally
                separated from the person under a decree of divorce or separate
                maintenance), brothers, sisters, sons-in-law, daughters-in-law,
                brothers-in-law, sisters-in-law, and grandchildren-in-law.
            </p>
            <h3>
                (n) Indemnity Losses
            </h3>
            <p>
                <em>Indemnity Losses</em>
                is defined in Section 9.04.
            </p>
            <h3>
                (o) Interest
            </h3>
            <p>
                <em>Interest</em>
                means the ownership interest and rights of a {{$member}} in the {{$company}},
                including the {{$member}}'s  right to a distributive share of the profits and
                losses, the distributions, and the property of the {{$company}} and the
                right to consent or approve {{$company}} actions. All Interests are subject
                to the restrictions on transfer imposed by this Agreement. Each
                {{$member}}'s  Interest is personal property and  will acquire any
                interest in any of the assets of the {{$company}}.<em> </em> <em></em>
            </p>
            <h3>
                (p) Legal Representative
            </h3>
            <p>
                With respect to any individual,<em> Legal Representative</em> means a
                person’s guardian, conservator, executor, administrator, trustee, or
                any other person representing a person or the person’s estate. With
                respect to any person,<em> Legal Representative</em> means all
                directors, officers, employees, consultants, financial advisors,
                counsel, accountants, and other agents of the person.
            </p>
            <h3>
                (q) Majority Vote; Supermajority Vote
            </h3>
            <p>
                <em>Majority Vote</em>
                means a ratio of more than 50 votes out of every 100 votes that may be
        cast will determine the matter subject to the vote.        <em>Supermajority Vote</em> means a ratio of at least 70 votes per 100
                votes that may be cast will determine the matter subject to the vote.
            </p>
            <h3>
                (r) Member
            </h3>
            <p>
                <em>Member</em>
                means any person designated in this Agreement as a Member or any person
                who becomes a Member under this Agreement.
            </p>
            <h3>
                (s) {{$member}} Joinder
            </h3>
            <p>
                <em>{{$member}} Joinder </em>
                means the joinder agreement in form and substance attached to this
                Agreement.
            </p>
            <h3>
                (t) Protected Person
            </h3>
            <p>
                <em>Protected Person</em>
                means:
            </p>
            <p>
                {{$the_member}};
            </p>
            <p>
                {{$the_member}}'s' officer, director, shareholder, partner, member,
                controlling Affiliate, employee, agent, or Legal Representative and
                each of their controlling Affiliates; and
            </p>
            <p>
                each of the {{$company}}'s employees, and agents or Legal Representatives.
            </p>
            <h3>
                (u) Securities Act
            </h3>
            <p>
                <em>Securities Act</em>
                refers to the Securities Act of 1933, as amended, or any successor
                federal statute, and the rules and regulations under it that are in
                effect at the time.
            </p>
            <h3>
                (v) Taxable Year
            </h3>
            <p>
                <em>Taxable Year </em>
                means the calendar year or any other accounting period selected by
                {{$the_member}}. Taxable Year is synonymous with fiscal year for all purposes of
                this Agreement.
            </p>
            <h3>
                (w) Third Party
            </h3>
            <p>
                <em>Third Party </em>
                means any person who:
            </p>
            <p>
                is not a {{$member}} of the {{$company}};
            </p>
            <p>
                does not directly or indirectly own or have the right to acquire any
                outstanding {{$interests}}; and
            </p>
            <p>
                is not an Affiliate.
            </p>
            <p>
        With respect to any controversy concerning the {{$company}},        <em>Third Party</em> means an individual who is not related to or
                subordinate to a claimant or respondent and has no personal or
                financial stake in the resolution of the controversy other than fair
                and reasonable compensation for services provided to resolve the
                controversy.
            </p>
            <h3>
                (x) Unprotected Act
            </h3>
            <p>
                <em>Unprotected Act</em>
                means any act, omission, or forbearance by a Protected Person that:
            </p>
            <p>
                with respect to any criminal proceeding, the Protected Person would
                have reasonable cause to believe was unlawful; or
            </p>
            <p>
                constitutes fraud or willful misconduct.
            </p>
            <h2>
                Section 1.02 Interpretation
            </h2>
            <p>
                The following general provisions and rules of construction apply to
                this Agreement.
            </p>
            <h3>
                (a) Singular and Plural; Gender
            </h3>
            <p>
                Unless the context requires otherwise, words denoting the singular may
                be construed as plural and words of the plural may be construed as
                denoting the singular. Words of one gender may be construed as denoting
        another gender as is appropriate within the context. The word        <em>or,</em> when used in a list of more than two items, may function
                as both a conjunction and a disjunction as the context requires or
                permits.
            </p>
            <h3>
                (b) Headings of Articles, Sections, and Subsections
            </h3>
            <p>
                The headings of Articles, Sections, and Subsections used within this
                Agreement are included solely for the reader’s convenience and
                reference. They have no significance in the interpretation or
                construction of this Agreement.
            </p>
            <h3>
                (c) Days and Business Days
            </h3>
            <p>
                In this Agreement, <em>days</em>, without further qualification, means
                calendar days and <em>business days </em>means any day other than a
                Saturday, Sunday or a day on which national banks are allowed by the
                Federal Reserve to be closed.
            </p>
            <h3>
                (d) Delivery
            </h3>
            <p>
                <em>Delivery</em>
                is taken in its ordinary sense and includes:
            </p>
            <p>
                personal delivery to a party;
            </p>
            <p>
                mailing by certified United States mail to the last known address of
                the party to whom delivery is made, with return receipt requested to
                the party making delivery;
            </p>
            <p>
                facsimile transmission to a party when receipt is confirmed in writing
                or by electronic transmission back to the sending party; or
            </p>
            <p>
                electronic mail transmission to a party when receipt is confirmed in
                writing or by electronic mail transmission back to the sending party.
            </p>
            <p>
                The effective date of delivery is the date of personal delivery or the
                date of the return receipt, if received by the sending party. If no
                return receipt is provided, the effective date is the date the
                transmission would have normally been received by certified mail if
                there is evidence of mailing.
            </p>
            <h3>
                (e) Include, Includes, and Including
            </h3>
            <p>
        In this Agreement, the words <em>include, includes</em>, and        <em>including</em> mean include without limitation, includes without
        limitation, and including without limitation, respectively.        <em>Include, includes,</em> and <em>including</em> are words of
                illustration and enlargement, not words of limitation or exclusivity.
            </p>
            <h3>
                (f) Words of Obligation and Discretion
            </h3>
            <p>
                Unless otherwise specifically provided in this Agreement or by the
                context in which used, the word <em>shall</em> is used to impose a
                duty, to command, to direct, or to require. Terms such as
                <em>
                    may, is authorized to, is permitted to, is allowed to, has the
                    right to
                </em>
                , or any variation or other words of discretion are used to allow, to
                permit, or to provide the discretion to choose what should be done in a
                particular situation, without any other requirement. Unless the
                decision of another party is expressly required by this Agreement,
                words of permission give the decision-maker the sole and absolute
                discretion to make the decision required in the context.
            </p>
            <h3>
                (g) Assignment
            </h3>
            <p>
                In this Agreement, <em>assignment</em> includes any method—direct or
                indirect, voluntary or involuntary—by which the legal or beneficial
                ownership of any interest in the {{$company}} is transferred or changed,
                including:
            </p>
            <p>
                any sale, exchange, gift, or any other form of conveyance, assignment,
                or transfer;
            </p>
            <p>
                a change in the beneficial interests of any trust or estate that holds
                any interest in the {{$company}} and a distribution from any trust or
                estate;
            </p>
            <p>
                a change in the ownership of {{$the_member}} that is a corporation,
                partnership, limited liability Company, or other legal entity,
                including the dissolution of the entity;
            </p>
            <p>
                a change in legal or beneficial ownership or other form of transfer
                resulting from the death or divorce of {{$the_member}} or the death of the
                spouse of {{$the_member}};
            </p>
            <p>
                any transfer or charge under a charging order issued by any court; and
            </p>
            <p>
                any levy, foreclosure, or similar seizure associated with the exercise
                of a creditor’s rights in connection with a mortgage, pledge,
                encumbrance, or security interest.
            </p>
            <p>
                <em>Assignment</em>
                does not include any mortgage, pledge, or similar voluntary encumbrance
                or grant of a security interest in any {{$interests}} in the {{$company}}.
            </p>
            <h3>
                (h) References to Transfer, Transferor, and Transferee
            </h3>
            <p>
                In this Agreement, <em>transfer</em> includes any direct or indirect
                sale, transfer, assignment, pledge, encumbrance, hypothecation, or
                other disposition or attempted disposition. The term includes any
                involuntary transfer, such as a transfer that occurs by operation of
                law. If a person enters into a contract, option, or other arrangement
                or understanding to make a transfer, that contract, option, or other
        arrangement or understanding will itself be considered a        <em>transfer</em>. When used as a verb, <em>transfer</em> has a
                correlative meaning. A person who makes a transfer may be referred to
                as a <em>transferor</em>, and a person who receives a transfer may be
                referred to as a <em>transferee.</em>
            </p>
            <h3>
                (i) References to Property or Assets
            </h3>
            <p>
                Any reference in this Agreement to <em>property </em>or<em> assets</em>
                , without further qualification, must be construed broadly to include,
                as to any person, all property of any kind—real or personal, tangible
                or intangible, legal or equitable—whether now owned or subsequently
        acquired. The following items are each considered <em>assets</em> or        <em>property</em> of a person: money, stock, accounts receivable,
                contract rights, franchises, value as a going concern, causes of
                action, undivided fractional ownership interests, intellectual property
                rights, and anything of any value that can be made available for or
                appropriated to the payment of debts.
            </p>
            <h3>
                (j) References to Individuals and Entities
            </h3>
            <p>
                Unless further qualified in the context, any reference in this
                Agreement to a <em>person</em>, <em>party</em>, or <em>individual</em>,
        or the use of indefinite pronouns like <em>anyone</em>,        <em>everyone</em>, <em>someone</em>, or <em>no one</em> must be
                construed broadly to include any individual, trust, estate,
                partnership, association, company, corporation, or other entity or
                non-entity capable of having legal rights and duties. <em>Person</em>,
                without further qualification,<em> </em>has the same broad meaning as
                defined in Code Section 7701(a)(1) and includes any individual, trust,
                estate, partnership, association, company, or corporation. The {{$company}}
                and its successors and assigns and {{$the_member}} or Assignee and their
                successors, assigns, heirs, and personal representatives are all
        considered <em>persons</em> for purposes of this Agreement.<em>Natural person</em> is used to distinguish a human being from a        <em>juridical person</em>, such as a trust, estate, partnership,
                association, company, or corporation.
            </p>
            <h3>
                (k) Internal References
            </h3>
            <p>
                Unless the context otherwise requires:
            </p>
            <p>
                reference to Articles, Sections, and Exhibits mean the Articles and
                Sections of, and Exhibits attached to, this Agreement;
            </p>
            <p>
                reference to an agreement, instrument or other document means the
                agreement, instrument, or other document as amended, supplemented, and
                modified from time to time to the extent permitted by its provisions;
                and
            </p>
            <p>
                reference to a statute means the statute as amended from time to time
                and includes any successor legislation to it and any regulations
                promulgated under it.
            </p>
            <p>
                The Exhibits referred to in this Agreement must be construed with, and
                as an integral part of, this Agreement to the same extent as if they
                were set forth verbatim in this Agreement.
            </p>
            <h1 style="text-align:center;">
                Article Two
                <br/>
                Organizational Matters
            </h1>
            <h2>
                Section 2.01 {{$company}} Formation
            </h2>
            <p>
                The {{$company}} became a limited liability company under the laws of the
                {{$company_formed_state_of}}, and specifically under the {{ $comp_formation_state_limited_act }}, upon filing the {{$title_of_doc_filed}} as required by
                the {{ $comp_formation_state_limited_act }}.
            </p>
            <h2>
                Section 2.02 Company’s Name
            </h2>
            <p>
            The {{$company}}’s name is {{$entity_info_entity_name}}. The {{$member}} may change the name of
                the {{$company}}, subject to the terms of this Agreement and Applicable Law.
            </p>
            <h2>
            Section 2.03 {{$company}}’s Purpose
            </h2>
            <p>
                The {{$company}}’s purpose is to engage in any lawful act or activity for
                which limited liability companies may be formed under the Act and all
                activities necessary or incidental to that purpose. The {{$company}} has all
                the powers necessary or convenient to carry out its purposes, including
                the powers granted by the Act.
            </p>
            <h2>
                Section 2.04 {{$company}}’s Principal Office and Location of Records
            </h2>
            <p>
                The street address of the principal office in the United States where
                the {{$company}} maintains its records is {{$entity_info_street}}, {{$entity_info_city}}, {{$entity_info_state}}
                {{$entity_info_zip}}.
            </p>
            <h2>
                
                    Section 2.05 {{$entity_info_registered_agent_title}} and Registered Office
                
            </h2>
            <p>
                The {{$company}}’s initial {{$entity_info_registered_agent_title}} is {{$attorney_name}} Hunt, and the
                {{$company}}’s initial registered office is located at {{$attorney_street}}, {{$attorney_city}}, {{$attorney_state}} {{$attorney_zip}}.
            </p>
            <h2>
                Section 2.06 {{$company}}’s Term
            </h2>
            <p>
                The {{$company}}’s duration is perpetual. The {{$company}} began on the date the
                {{$title_of_doc_filed}} were filed with the {{$company_formed_state}} Department of
                State and will continue until terminated or dissolved as provided in
                this Agreement.
            </p>
            <h2>
                Section 2.07 Taxation as a C Corporation
            </h2>
            <p>
                {{$The_member}} shall elect to have the {{$company}} taxed as a corporation under
                Code Subsection C for federal, state, and local income tax purposes by
                filing Internal Revenue Service Form 8832 and any other tax form or
                document required by the Code or Treasury Regulations.
            </p>
            <h2>
                Section 2.08 Member’s Capital Contributions
            </h2>
            <p>
                {{$member_has}} has made a Capital Contribution to the {{$company}} in exchange
                for an interest in the {{$company}}. {{$member_is}} the sole {{$member}} of the
                {{$company}} and owns all of the {{$interest}} in the {{$company}}. {{$The_member}} may
                make voluntary Capital Contributions to the {{$company}}.
            </p>
            <h2>
                Section 2.09 Admitting New {{$member}}
            </h2>
            <p>
                Subject to the requirements of Article Seven, Additional {{$member}} may be
                admitted when the {{$company}} issues new {{$interests}} or {{$the_member}} transfers
                its {{$interest}}. Upon compliance with Article Seven, a person will be
                admitted as an Additional Member, listed as such on the {{$company}}’s
                books, and issued the {{$interest}}.
            </p>
            <p>
                The {{$company}} may adopt and revise rules, conventions, and procedures as
                the {{$company_determines}} appropriate regarding the admission of
                Additional {{$member}} to reflect the Interests at the end of the calendar
            year in accordance with the {{$member}}’s intentions.
            </p>
            <h2>
                Section 2.10 Transferability of Interest
            </h2>
            <p>
                The transferability of {{$the_member}}’s {{$interest}} is restricted by Article
                Seven.
            </p>
            <h2>
                Section 2.11 Mandatory Additional Capital Contributions Prohibited
            </h2>
            <p>
                The {{$company}} has no authority to require additional Capital
                Contributions.
            </p>
            <h2>
                Section 2.12 No Mandatory Loans
            </h2>
            <p>
                The {{$company}} has no authority to require {{$the_member}} to make loans of
                additional capital to the {{$company}}.
            </p>
            <h1 style="text-align:center;">
                Article Three
                <br/>
                Distributions
            </h1>
            <h2>
                Section 3.01 Distributions to {{$member}}
            </h2>
            <p>
                {{$The_member}} may cause the {{$company}} to make distributions to {{$the_member}}
                when {{$the_member}} determines. These distributions must comply with
                Section 3.02.
            </p>
            <h2>
                Section 3.02 No Unlawful Distributions
            </h2>
            <p>
                Despite any provision to the contrary in this Agreement, the {{$company}}
                must not make any distribution that would violate any contract or
                agreement to which the {{$company}} is then a party or any law, rule,
                regulation, order or directive of any Governmental Authority then
                applicable to the Company.
            </p>
            <h2>
                Section 3.03 In-Kind Distributions
            </h2>
            <p>
                The {{$company}} may make in-kind distributions to {{$the_member}} in the form of
                securities or other noncash property held by the {{$company}}.
            </p>
            <h2>
                Section 3.04 No Interest or Demand Rights
            </h2>
            <p>
                All distributions will be made under this Article or Section 8.03(c).
                Except as specifically set forth in this Article, {{$the_member}} may not
                demand distributions. If {{$the_member}} does not withdraw all or any
                portion of {{$the_member}}’s share of any cash distribution, {{$the_member}} will
                not receive any interest on the unwithdrawn amount.
            </p>
            <h2>
                Section 3.05 Proceeds from Capital Transactions
            </h2>
            <p>
                Except as otherwise provided in this Agreement, before making any
                distribution to Member, proceeds of any capital transaction will be
                applied to:
            </p>
            <p>
                the principal balance at that time of that portion (or any greater
                portion thereof that the Company determines should be repaid) of any
                loans that the {{$company_determines}} are attributable to the capital
                transaction;
            </p>
            <p>
                the amount of all costs and expenses paid or to be paid by the {{$company}}
                in connection with the capital transaction; and
            </p>
            <p>
                a reasonable reserve for future payments that may need to be made by
                the {{$company}} with respect to the capital transaction.
            </p>
            <h2>
                Section 3.06 Return of Distribution
            </h2>
            <p>
                Any distribution made to {{$the_member}} will be considered to comply with
                Applicable Law if the distribution is made from available assets of the
                {{$company}}. If a court of competent jurisdiction finds that a distribution
                violates Applicable Law, {{$the_member}} must return that distribution.
            </p>
            <h1 style="text-align:center;">
                Article Four
                <br/>
                {{$company}} Management
            </h1>
            <h2>
                Section 4.01 Management by {{$member}}
            </h2>
            <p>
                The Company is managed by the Member. The Member may take all actions
                necessary, useful, or appropriate for the ordinary management and
                conduct of the Company’s business. The Member has the exclusive
                authority to manage the Company’s operations and affairs, subject in
                all cases to Applicable Law.
            </p>
            
            <h2>
                Section 4.02 {{$member}}’s Agency Authority
            </h2>
            <p>
            {{$The_member}} has the right and the authority to bind the {{$company}} in
                contracts and other dealings with Third Parties.
            </p>
            <h2>
                Section 4.03 {{$member}}’s Fiduciary Duties
            </h2>
            <p>
                This Agreement does not create or impose any fiduciary duty on any
                {{$member}}. {{$The_member}} and the {{$company}} waive all fiduciary duties that,
                absent this waiver, may be implied by Applicable Law. The provisions of
                this Agreement that restrict the {{$member}}’s duties and liabilities
                replace any duties and liabilities otherwise existing at law or in
                equity.
            </p>
            <h1 style="text-align:center;">
                Article Five
                <br/>
                {{$member}} Rights and Obligations
            </h1>
            <h2>
                Section 5.01 Limited Liability of {{$member}}
            </h2>
            <p>
                Except as required by Applicable Law, a {{$member}}’s status as a {{$member}}
                does not personally obligate the {{$member}} for any debt, obligation, or
                liability of the {{$company}} whether arising in contract, tort, or
                otherwise.
            </p>
            <p>
                No {{$member}} will be required to contribute capital to the {{$company}} for the
                payment of any losses or for any other purposes. No {{$member}} will be
                responsible or obligated to any Third Party for any debts or
                liabilities of the {{$company}} in excess of the amount of:
            </p>
            <p>
            {{$the_member}}’s unpaid required Capital Contributions;
            </p>
            <p>
                unrecovered Capital Contributions; and
            </p>
            <p>
            {{$the_member}}’s share of any undistributed {{$company}} profits.
            </p>
            <h2>
                Section 5.02 Power of Member
            </h2>
            <p>
                The {{$member_has}} the power to exercise all rights or powers granted to
                the {{$member}} under the express terms of this Agreement and the Act.
            </p>
            <h2>
                    Section 5.03 Restrictions on Withdrawal or Dissociation Rights
            </h2>
            <p>
                A {{$member}} does not dissociate, withdraw, or otherwise cease to be a
                {{$member}} because of the {{$member}}’s bankruptcy or because of any event
                specified in the Act.
            </p>
            <h2>
                Section 5.04 {{$company}} Continues after the {{$member}}’s Death
            </h2>
            <p>
                A {{$member}}’s death will not cause the {{$company}} to dissolve.
            </p>
            <h1 style="text-align:center;">
                Article Six
                <br/>
                Books, Records, and Bank Accounts
            </h1>
            <h2>
                Section 6.01 Books and Records
            </h2>
            <p>
                The {{$company}} shall keep books of account regarding the operation of the
                {{$company}} at the principal office of the {{$company}} or at any other place
                the {{$company_determines}}. The Company shall keep the following records:
            </p>
            <p>
                a current list of the full names and last known addresses of each past
                and present {{$member}};
            </p>
            <p>
                a copy of the {{$title_of_doc_filed}} (and any amendments) and copies
                of any powers of attorney under which any certificate has been signed;
            </p>
            <p>
                copies of the {{$company}}’s federal, state, and local income tax returns
                and any reports for the three most recent Taxable Years;
            </p>
            <p>
                copies of this Agreement (and any amendments);
            </p>
            <p>
                copies of any financial statements of the {{$company}} for the three most
                recent Taxable Years; and
            </p>
            <p>
                any other documents required by Applicable Law.
            </p>
            <h2>
                Section 6.02 Accounting and Taxable Year
            </h2>
            <p>
                The {{$company}}’s Taxable Year is the calendar year unless otherwise
                designated by the {{$member}}. The {{$member}} will determine the accounting
                method and the {{$company}} will file tax returns based on that accounting
                method. The {{$member}} is responsible for all the {{$company}}’s accounting
                matters.
            </p>
            <h2>
                    Section 6.03 Bank Accounts and {{$company}} Funds
            </h2>
            <p>
                The {{$company}} shall deposit all cash receipts in the {{$company}}’s depository
                accounts. All accounts used by or on behalf of the {{$company}} are the
                {{$company}}’s property, and will be received, held, and disbursed by the
                {{$company}} for the purposes specified in this Agreement. The {{$member}} may
                not commingle {{$company}} funds with any other funds.
            </p>
            <h1 style="text-align:center;text-transform: uppercase;">
                Article Seven
                <br/>
                Transfer of {{$interests}}
            </h1>
            <h2>
                Section 7.01 Transferability of {{$interests}}
            </h2>
            <p>
                Any {{$member}} may voluntarily transfer its {{$interest}} without the consent of
                any other {{$member}} as long as the proposed transfer does not:
            </p>
            <p>
                cause the {{$company}} to terminate for federal income tax purposes;
            </p>
            <p>
                result in any event of default as to any secured or unsecured
                obligation of the {{$company}};
            </p>
            <p>
                cause a reassessment of any real property owned by the {{$company}}; or
            </p>
            <p>
                cause any other adverse material impact to the {{$company}}.
            </p>
            <p>
                The transferee of a voluntary transfer of {{$interest}} permitted by this
                Section will be admitted as an Additional {{$member}} and will not have any
                rights as a {{$member}} without the written consent of the {{$member}} in
                connection with the voluntary transfer of the {{$member}}’s {{$interest}}.
            </p>
            <h2>
                Section 7.02 Additional {{$member}}’s Effective Admission Date
            </h2>
            <p>
                The effective date of an Additional {{$member}}’s admission is the date on
                which the {{$member_accepts}} the Assignee as an Additional {{$member}} under
                this Agreement.
            </p>
            <h2>
                Section 7.03 Amending {{$operating_agreement}} and {{$title_of_doc_filed}}
            </h2>
            <p>
                If required by Applicable Law, upon the admission of an Additional
                {{$member}}, the {{$company}} may amend the {{$operating_agreement}}, the {{$title_of_doc_filed}}, or both to reflect any substitution or addition of the
                Additional {{$member}}. The {{$company}} may assess any associated fees, costs,
                or other expenses associated with that Additional {{$member}}.
            </p>
            <h2>
                Section 7.04 Creditor Rights; Charging Order Sole and Exclusive Remedy
            </h2>
            <p>
                If a creditor obtains a judgment by a court of competent jurisdiction
                against {{$the_member}}, the court may charge the {{$member}}’s {{$interest}} with
                payment of the unsatisfied amount of the judgment from distributions
                attributable to the affected {{$interest}}, but only to the extent permitted
                by the Act. To the extent any {{$interest_is}} charged with satisfaction of
                a judgment, the judgment creditor will receive no more than the rights
                of an Assignee and will not be admitted as a {{$member}} of the {{$company}}.
            </p>
            <p>
                The charging order is the exclusive remedy by which a judgment creditor
                of {{$the_member}} may obtain any satisfaction from the {{$company}} toward any
                judgment against the {{$member}}. This Section does not deprive {{$the_member}}
                of rights under any exemption laws available to the {{$member}}.
            </p>
            <h2>
                Section 7.05 Assignee or Charging Order Holder Assumes Tax Liability
            </h2>
            <p>
                The Assignee of {{$interest_an}} and any person who acquires a charging
                order against {{$interest_an}} shall report income, gains, losses,
                deductions and credits regarding the interest for the period in which
                the Assignee interest is held or for the period the charging order is
                outstanding.
            </p>
            <h1 style="text-align:center;text-transform:uppercase;">
                Article Eight
                <br/>
                Dissolution and Liquidation
            </h1>
            <h2>
                Section 8.01 Dissolution Events
            </h2>
            <p>
                The {{$company}} will be dissolved only if an event described in this
                Section occurs.
            </p>
            <h3>
                (a) Dissolution by the {{$member}}
            </h3>
            <p>
                The {{$company}} will be dissolved by the {{$member}}.
            </p>
            <h3>
                (b) Judicial Dissolution
            </h3>
            <p>
                The {{$company}} will be dissolved upon the entry of a decree of judicial
                dissolution by a court of competent jurisdiction.
            </p>
            <p>
                After dissolution, the {{$company}} may only conduct activities necessary to
                wind up its affairs.
            </p>
            <h2>
                Section 8.02 Effect of Dissolution
            </h2>
            <p>
                Dissolution of the {{$company}} will be effective on the day on which the
                event described in Section 8.01 occurs, but the {{$company}} will not
                terminate until the winding up of the {{$company}} has been completed, the
                assets of the {{$company}} have been distributed as provided in Section
                8.03, and the {{$company}}’s {{$title_of_doc_filed}} has been cancelled as
                provided in Section 8.06.
            </p>
            <h2>
                Section 8.03 Liquidation
            </h2>
            <p>
                After dissolving the {{$company}}, the {{$member}} will have full authority to
                sell, assign, and encumber any or all of the {{$company}}’s assets and to
                wind up and liquidate the affairs of the {{$company}} in an orderly and
                businesslike manner. The {{$member}} shall liquidate the {{$company}} assets and
                apply and distribute proceeds from the liquidation of the assets as
                follows.
            </p>
            <h3>
                (a) Creditor Payment
            </h3>
            <p>
                The proceeds from the liquidated property will first be applied toward
                or paid to any creditor of the {{$company}} in the order of payment required
                by Applicable Law.
            </p>
            <h3>
                (b) Provision for Reserves
            </h3>
            <p>
                After paying liabilities owed to creditors, the {{$member}} shall set up
                such reserves as the {{$member_determines}} is reasonably necessary. The
                {{$member}} may, but need not, pay over any reserves for contingent
                liabilities to a bank to hold in escrow for later payment.
            </p>
            <p>
                After the {{$member}} is reasonably satisfied that any liabilities have been
                adequately resolved, the {{$member}} shall distribute any remaining reserves
                to the {{$member}} or {{$member_its}} assigns as provided in Section 8.03(c).
            </p>
            <h3>
                (c) Distributions to {{$member}}
            </h3>
            <p>
                After paying liabilities owed to creditors and establishing reserves,
                the {{$member}} shall satisfy any debts owed to the {{$member}} with any
                remaining net assets of the {{$company}}, and then distribute any remaining
                assets to the {{$member}}.
            </p>
            <h2>
                    Section 8.04 In-Kind Distributions in Liquidation
            </h2>
            <p>
                Despite the provisions of Section 8.03 that require the liquidation of
                the {{$company}}’s assets but subject to the order of priorities set forth
                in Section 8.03(c), if upon dissolution of the {{$company}} the {{$member_determines}} that an immediate sale of part or all of the {{$company}}’s
                assets would be impractical or could cause undue loss to the {{$member}},
                the {{$member}} may defer the liquidation of any assets except those
                necessary to satisfy {{$company}} liabilities and reserves. If the {{$member_determines}}
                the assets are not suitable for liquidation, the {{$member}} may
                distribute undivided interests in the {{$company}}’s assets to the {{$member}}
                instead of cash. Any in-kind distribution will be subject to any
                conditions relating to the disposition and management of the properties
                that the {{$member_determines}} to be reasonable and equitable and to any
                agreements governing the operating of such properties at that time.
            </p>
            <h2>
                Section 8.05 {{$company}} Property Sole Source
            </h2>
            <p>
            {{$company}} property is the sole source for the payment of any debts or
                liabilities owed by the {{$company}}. Any return of Capital Contributions or
                liquidation amounts to the {{$member}} will be satisfied only to the extent
                that the {{$company}} has adequate assets.
            </p>
            <h2>
                    Section 8.06 Cancellation of {{$title_of_doc_filed}}
            </h2>
            <p>
                Upon completing the distribution of the {{$company}}’s assets as provided in
                Section 8.03(c), the {{$company}} will be terminated and the {{$member}} shall
                cause the cancellation of the {{$title_of_doc_filed}} in the {{$company_formed_state_of}} and of all qualifications and registrations of the {{$company}} as a
                foreign limited liability company in any other jurisdictions and shall
                take any other actions necessary to terminate the {{$company}}.
            </p>
            <h2>
                Section 8.07 Survival of Indemnity Rights, Duties, and Obligations
            </h2>
            <p>
                For purposes of Article Nine, including the {{$member}}’s right to
                indemnification under Section 9.04, the {{$company}}’s dissolution,
                liquidation, winding up, or termination for any reason will not release
                any party from any loss that, at the time of the dissolution,
                liquidation, winding up, or termination, had already accrued to any
                other party or which may accrue because of any act or omission
                occurring before the dissolution, liquidation, winding up, or
                termination.
            </p>
            <h2>
                
                    Section 8.08 {{$company}} Asset Sales during Term of the {{$company}}
            </h2>
            <p>
                The sale of {{$company}} assets during the term of the {{$company}} does not
                constitute liquidation, dissolution, or termination of the {{$company}} as
                defined under this Article. The {{$company}} may reinvest the sale proceeds
                in other assets consistent with the business purposes for the {{$company}}.
                Further, the {{$company}} may participate in any real property exchange as
                defined in Code Section 1031 if the exchange fulfills the business
                purposes of the {{$company}}.
            </p>
            <h1 style="text-align:center;text-transform:uppercase;">
                Article Nine
                <br/>
                Exculpation and Indemnification
            </h1>
            <h2>
                
                    Section 9.01 Exculpation of Protected Persons
                
            </h2>
            <p>
                No Protected Person is liable to the {{$company}} or any other Protected
                Person for any loss, damage, or claim incurred because of any action
                taken or not taken by the Protected Person in good-faith reliance on
                the provisions of this Agreement.
            </p>
            <h2>
                Section 9.02 Good-Faith Reliance
            </h2>
            <p>
                A Protected Person is fully protected if the Protected Person relies in
                good faith on the {{$company}}’s records or on information, opinions,
                reports, or statements of the following Persons or groups:
            </p>
            <p>
                one or more employees of the {{$company}};
            </p>
            <p>
                any attorney, independent accountant, appraiser, or other expert or
                professional employed or engaged by or on behalf of the {{$company}}; or
            </p>
            <p>
                any other person selected in good faith by or on behalf of the {{$company}},
                in each case as to matters that the relying person reasonably believes
                to be within the other person’s area of professional expertise.
            </p>
            <p>
                The information, opinions, reports, or statements referred to above
                include financial statements; information, opinions, reports, or
                statements as to the value or amount of the {{$company}}’s assets,
                liabilities, income, or losses; and any facts pertinent to the
                existence and amount of assets from which distributions might properly
                be paid.
            </p>
            <p>
                In no way does this provision limit any person’s right to rely on
                information as provided in the Act. Any act, omission, or forbearance
                by a Protected Person on the advice of the {{$company}}’s counsel must be
                conclusively presumed to have been in good faith.
            </p>
            <h2>
                Section 9.03 Decision-Making Standards
            </h2>
            <p>
                When this Agreement permits or requires a Protected Person to make a
                decision (including discretionary decisions and other grants of similar
                authority or latitude), the Protected Person is entitled to consider
                only the interests and factors as the Protected Person chooses,
                including its own interests, with no obligation to give any
                consideration to any interest of or factors affecting the {{$company}} or
                any other person. When this Agreement permits or requires a Protected
                Person to make a good-faith decision, the Protected Person shall act
                under this express standard and is not subject to any other standard
                imposed by this Agreement or any Applicable Law.
            </p>
            <h2>
                Section 9.04 Indemnification
            </h2>
            <p>
                The {{$company}} shall indemnify, hold harmless, defend, pay, and reimburse
                any Protected Person against all losses, claims, damages, judgments,
                fines, or liabilities, including reasonable legal fees or other
                expenses incurred in their investigation or defense, that arise in
                connection with any actual or alleged act, omission, or forbearance
                performed or omitted on behalf of the {{$company}} or the {{$member}} in
                connection with the {{$company}}’s business. If the act or omission is not
                an Unprotected Act, the {{$company}} shall also reimburse any amounts
        expended in settling any claims (collectively,        <em>Indemnity Losses</em>) to which the Protected Person may become
                subject because:
            </p>
            <p>
                of any act or omission or alleged act or omission on behalf of the
                {{$company}}, or the {{$member}};
            </p>
            <p>
                the Protected Person is or was acting in connection with the {{$company}}’s
                business as a partner, member, stockholder, controlling Affiliate,
                manager, director, officer, employee, or agent of the {{$company}}; {{$the_member}}; or any of their respective controlling Affiliates; or
            </p>
            <p>
                the Protected Person is or was serving at the {{$company}}’s request as a
                partner, member, manager, director, officer, employee, or agent of any
                person including the {{$company}}.
            </p>
            <p>
                A Protected Person’s conduct will be determined under a final,
                nonappealable order of a court of competent jurisdiction. The
                termination of any action, suit, or proceeding by judgment, order,
                settlement, conviction, or a plea of <em>nolo contendere</em> or its
                equivalent, does not, of itself, create a presumption that the
                Protected Person did not act in good faith or, with respect to any
                criminal proceeding, had reasonable cause to believe that the conduct
                was unlawful or constituted fraud or willful misconduct.
            </p>
            <p>
                The indemnity provided by this Article extends to the full extent
                permitted by the Act as it now exists or may later be amended,
                substituted, or replaced, but only if the amendment, substitution, or
                replacement permits the {{$company}} to provide broader indemnification
                rights than those the Act permits.
            </p>
            <h2>
                Section 9.05 Reimbursement
            </h2>
            <p>
                The {{$company}} shall promptly reimburse and may provide advancements to
                each Protected Person for reasonable legal or other expenses incurred
                in connection with investigating, preparing to defend, or defending any
                claim, lawsuit, or other proceeding relating to any Indemnity Losses
                for which such Protected Person may be indemnified under Section 9.04.
                If it is finally judicially determined that the Protected Person is not
                entitled to the indemnification provided by Section 9.04, the Protected
                Person shall promptly reimburse the {{$company}} for any reimbursed or
                advanced expenses.
            </p>
            <h2>
                Section 9.06 Entitlement to Indemnity
            </h2>
            <p>
                The indemnification provided by Section 9.04 does not exclude any other
                indemnification rights under any separate agreement or otherwise.
                Section 9.04 will continue to protect each Protected Person regardless
                of whether the Protected Person remains in the position or capacity
                under which the Protected Person became entitled to indemnification
                under Section 9.04 and will inure to the benefit of the Protected
                Person’s executors, administrators, legatees, and distributees.
            </p>
            <h2>
                Section 9.07 Insurance
            </h2>
            <p>
                To the extent available on commercially reasonable terms, the {{$company}}
                may purchase, at the {{$company}}’s expense, insurance to cover Indemnity
                Losses covered by these indemnification provisions and to cover
                Indemnity Losses for any Protected Person’s breach or alleged breach of
                the Protected Person’s duties. The {{$company}} will determine the coverage
                amounts and the deductibles. A decision not to purchase insurance will
                not affect a Protected Person’s right to indemnification (including the
                right to be reimbursed, advanced expenses, or indemnified for Indemnity
                Losses under any other provisions of this Agreement) under this
                Agreement. A Protected Person that recovers any amount for any
                Indemnity Losses from any insurance coverage shall reimburse the
                {{$company}} for any amount previously received from the {{$company}} for those
                Indemnity Losses.
            </p>
            <h2>
                Section 9.08 Indemnification Obligation Funding
            </h2>
            <p>
                Despite anything in this Agreement to the contrary, any indemnity by
                the {{$company}} relating to Section 9.04 will be provided out of and to the
                extent of the {{$company}}’s assets. No Member will have any personal
                liability or will be required to make Capital Contributions to help
                satisfy the indemnity unless the {{$member}} otherwise agrees in writing.
            </p>
            <h2>
                Section 9.09 Securities Indemnity
            </h2>
            <p>
                Each {{$member}} agrees to hold the {{$company}} harmless from all expenses,
                liabilities, and damages (including reasonable attorneys’ fees) arising
                from a disposition of {{$interest}} in any manner that violates the
                Securities Act, any applicable state securities law, or this Agreement.
                This indemnification includes the {{$company}}’s {{$member}}, {{$member}} principals,
                organizers, and controlling persons (as defined in the Securities Act),
                and any persons affiliated with any of them or with the distribution of
                the {{$interest}}.
            </p>
            <h2>
                Section 9.10 Savings Clause
            </h2>
            <p>
                Article Nine survives the {{$company}}’s dissolution, liquidation, winding
                up, and termination. If Article Nine or any portion of it is
                invalidated on any ground by any court of competent jurisdiction, the
                {{$company}} shall indemnify and hold harmless each Protected Person under
                any applicable portion of this Article that was not invalidated and to
                the full extent permitted by Applicable Law. To the extent possible,
                Article Nine supersedes any {{$company_formed_state}} law to the contrary.
            </p>
            <h2>
                Section 9.11 Amendment
            </h2>
            <p>
                Article Nine is a contract between the {{$company}} and, collectively, each
                Protected Person who serves in that capacity at any time while Article
                Nine is in effect. The {{$company}} and each Protected Person intend to be
                legally bound under this contract. No amendment, modification, or
                repeal of Article Nine that adversely affects a Protected Person’s
                indemnification rights for Indemnity Losses incurred or relating to a
                state of facts existing before the amendment, modification, or repeal
                will apply without the Protected Person’s prior written consent.
            </p>
            <h1 style="text-align:center;text-transform:uppercase;">
                Article Ten
                <br/>
                General Matters
            </h1>
            <h2>
                Section 10.01 Expenses
            </h2>
            <p>
                Except as otherwise expressly provided in this Agreement, the incurring
                party must pay all expenses (including fees and disbursements of
                counsel, financial advisors, and accountants) incurred in preparing and
                executing this Agreement, making any amendment or waiver to it, and
                completing the transactions contemplated by it.
            </p>
            <h2>
                Section 10.02 Binding Effect
            </h2>
            <p>
                Subject to the restrictions on transfer in this Agreement, this
                Agreement binds and inures to the benefit of the {{$member}} and to {{$member_its}}
                respective successors, personal representatives, heirs, and assigns.
            </p>
            <h2>
                Section 10.03 Governing Law
            </h2>
            <p>
                The affairs of the {{$company}} and the conduct of its business are governed
                by the provisions of this Agreement to the extent such provisions are
                not in conflict with nonwaivable provisions of Applicable Law or the
                {{$title_of_doc_filed}}. This Agreement is governed, construed, and
                administered according to the laws of {{$company_formed_state}}, as from time to time
                amended, and any applicable federal law. No effect is given to any
                choice-of-law or conflict-of-law provision or rule (whether of the
                {{$company_formed_state_of}} or any other jurisdiction) that would cause the
                application of the law of any jurisdiction other than those of the
                {{$company_formed_state_of}}.
            </p>
            <h2>
                Section 10.04 Severability
            </h2>
            <p>
                The invalidity or unenforceability of any provision of this Agreement
                does not affect the validity or enforceability of any other provision
                of this Agreement. If a court of competent jurisdiction determines that
                any provision is invalid, the remaining provisions of this Agreement
                are to be construed as if the invalid provision had never been included
                in this Agreement.
            </p>
            <h2>
                Section 10.05 Amendments
            </h2>
            <p>
                No provision of this Agreement may be amended or modified except by a
                written instrument executed by the {{$member}}.
            </p>
            <h2>
                Section 10.06 Multiple Originals; Validity of Copies
            </h2>
            <p>
                This Agreement may be signed in any number of counterparts, each of
                which will be deemed an original. Any person may rely on a copy of this
                Agreement that {{$the_member}} certifies to be a true copy to the same
                effect as if it were an original.
            </p>
            <h2>
                Section 10.07 Determination of Fair Market Value
            </h2>
            <p>
                The <em>Fair Market Value</em> of any asset is the purchase price that
                a willing buyer having reasonable knowledge of relevant facts would pay
                a willing seller for that asset in an arm’s length transaction on any
                date, without time constraints and without being under any compulsion
                to buy or sell. Fair Market Value is a good-faith determination made by
                the {{$company}} based on factors the {{$company}}, in its reasonable business
                judgment, considers relevant.
            </p>
            <p>
                With respect to any other transfer of a {{$member}}’s {{$interest}} to the
                {{$company}} under this Agreement, the Fair Market Value will be the amount
                agreed upon by the {{$company}} and the transferring {{$member}}. If the {{$company}}
                and the transferring {{$member}} are unable to agree about the Fair Market
                Value, they shall attempt to agree upon an appraiser and, if an
                appraiser is agreed upon in writing, the value as determined by that
                appraiser will be final and binding. If the {{$company}} and the
                transferring {{$member}} are unable to agree about the Fair Market Value or
                an appraiser within 30 days from the date of the notice or other
                triggering event for the sale, the {{$company}} shall choose a Qualified
                Appraiser and the value as determined by a Qualified Appraisal by that
                Qualified Appraiser will be final and binding, with the fees and costs
                of such Qualified Appraiser to be paid by or deducted from the amount
                payable to the transferring {{$member}}.
            </p>
            <h2>
                Section 10.08 Notice of Immunity from Liability for Certain Disclosures
            </h2>
            <p>
                {{$member_individual}} shall be held criminally or civilly liable under any
                federal or state trade secret law for a disclosure of a trade secret,
                as long as the disclosure is made:
            </p>
            <p>
                in confidence to a federal, state, or local government official, either
                directly or indirectly, or to an attorney solely for the purpose of
                reporting or investigating a suspected violation of law; or
            </p>
            <p>
                in a complaint or other document filed in a lawsuit or other
                proceeding, if such filing is made under seal.
            </p>
            <p>
                This Section is intended to comply with the immunity provided by the
                United States Code from liability resulting from disclosures of trade
                secrets under the conditions described in this Section. Nothing in this
                {{$operating_agreement}} is intended to conflict with 18 U.S.C. § 1833(b).
                If there is a conflict between this Section and any other Section of
                this {{$operating_agreement}}, this Section will control.
            </p>
            <p>
                <strong></strong>
            </p>
            <p>
                <strong>Signed:</strong>
            </p>
            <p>
            {{$member}}:
            </p>
            <p style="text-align:left;">
                _______________________________
            </p>
            <p style="text-align:left;">
            {{$member_info_member_name}}
            </p>
        </div>
        <br clear="all"/>
        <div>
            <h1 style="text-align:center;">
                Schedule A
                <br/>
                {{$member}} Schedule
            </h1>
            <div style="margin: 0 auto;display: table;">
                <table border="1" cellspacing="0" cellpadding="0" width="782">
                    <thead>
                        <tr>
                            <td width="181" valign="top">
                                <p>
                                {{$member}}
                                </p>
                            </td>
                            <td width="192" valign="top">
                                <p>
                                    Initial Capital Contribution
                                </p>
                            </td>
                            <td width="108" valign="top">
                                <p>
                                    Contribution Date
                                </p>
                            </td>
                            <td width="84" valign="top">
                                <p>
                                    Assumed Debt
                                </p>
                            </td>
                            <td width="96" valign="top">
                                <p>
                                    Tax Basis
                                </p>
                            </td>
                            <td width="121" valign="top">
                                <p>
                                    Ownership
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td width="181" valign="top">
                                <p>
                                {{$member_info_member_name}}
                                </p>
                            </td>
                            <td width="192" valign="top">
                                <p>
                                    ${{$member_info_initial_capital_amount}}
                                </p>
                            </td>
                            <td width="108" valign="top">
                                <p>
                                {{$member_info_contribution_date}}
                                </p>
                            </td>
                            <td width="84" valign="top">
                                <p>
                                    ${{$member_info_assumed_debt}}
                                </p>
                            </td>
                            <td width="96" valign="top">
                                <p>
                                    ${{$member_info_tax_basis}}
                                </p>
                            </td>
                            <td width="121" valign="top">
                                <p>
                                    100%
                                </p>
                            </td>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <pagebreak>
        <h1 style="text-align:center;">
        {{$member}} Joinder in
            <br/>
            {{$operating_agreement}}
            <br/>
            of
            <br/>
            {{$entity_info_entity_name}}
        </h1>
        <p>
            I, _______________________ (<em>{{$member}}</em>), acknowledge that I have read the {{$operating_agreement}}
            of {{$entity_info_entity_name}} dated ______________, 20__ (<em>Agreement</em>),
            that I know its contents, and agree to be bound to the Agreement as a
            {{$member}} of the {{$company}} with the following {{$interest}} in the {{$company}}:
        </p>
        <p>
            Interest in the {{$company}}: _______________________
        </p>
        <p>
            I agree that this Interest is irrevocably bound by the Agreement. By
            signing and delivering this {{$member}} Joinder, I make all representations and
            warranties set forth in the Agreement, effective as of the date of my
            signature below, and agree to fulfill all duties and obligations imposed on
            {{$member}} under the Agreement. It is my intention to be bound to the Agreement
            as a signatory and party to the Agreement just as if I was an original
            signatory and party to the Agreement.
        </p>
        <p>
            I am aware that the legal, financial, and related matters in the Agreement
            are complex and that I am free to seek independent professional guidance or
            counsel with respect to this {{$member}} Joinder. I have either sought guidance
            or counsel or determined that I waive this right after carefully reviewing
            the Agreement.
        </p>
        <p>&nbsp;</p><p>&nbsp;</p>
        <p style="text-align:right;">
            ________________________
        </p>
        <p style="text-align:right;">
            Print Name: ______________
        </p>
        <p style="text-align:right;">
            Date: ______________
        </p>
        <p>
            Agreed and acknowledged:
        </p>
        <p>
        {{$entity_info_entity_name}}
        </p>
        <p>&nbsp;</p>
        <p>
            ________________________
        </p>
        <p>
            Print Name: ______________
        </p>
        <p>
            Date: ___________________
        </p>
    </body>
</html>