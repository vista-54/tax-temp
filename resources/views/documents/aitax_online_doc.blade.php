<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ai Tax Online | A Product of Castro & Co.</title>
<style>
                
#aitax {
  border-collapse: collapse;
  width: 100%;
}

#aitax td, #aitax th {
  border: 1px solid #ddd;
  padding: 8px;
}

#aitax tr:nth-child(even){background-color: #f2f2f2;}

#aitax tr:hover {background-color: #ddd;}

#aitax th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #4c78af;
  color: white;
}

#child_table th {
    padding-top: 2px;
    padding-bottom: 2px;
    text-align: left;
    background-color: #b6b767;
    color: white;
    font-weight: 300;
}

</style>
</head>
<body>

<div style="font-size: 26;font-weight: bold;">
        <IMG SRC="https://aitaxonline.com/images/sLogo.png" alt="[Image]" height="100">
        <div style="margin: -60px 150px 40px;">Ai Tax Details</div>
        </div>
<br/>

<h2>Name: Mo Iqbal(mo.iqbal@gmail.com)</h2>
<span style="float: right;">Application submitted on: 15/02/2020</span>
<table id="aitax">
    <tr style="background-color: white;">
        <th>Question</th>
        <th>Answer</th>
    </tr>
    <tbody>
            <tr>
                <th colspan="2">Personal Information</th>
            </tr>
            <tr>
                <td width="50%">Is watched introduction video?</td>
                <td width="50%">Yes</td>
            </tr>
            <tr>
                <td width="50%">Date of Birth</td>
                <td width="50%">10/15/1980</td>
            </tr>
            <tr>
                <td width="50%">Current Job Title</td>
                <td width="50%">Test job</td>
            </tr>
           
            <tr>
                <td width="50%">
                    
                        Country/Region
                    
                </td>
                <td width="50%">
                    
                        USA
                    
                </td>
            </tr>
           
            <tr>
                <td width="50%">
                    
                        Street
                    
                </td>
                <td width="50%">
                    
                        Test
                    
                </td>
            </tr>
           
            <tr>
                <td width="50%">
                    
                        City
                    
                </td>
                <td width="50%">
                    
                        Miami
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Visa
                    
                </td>
                <td width="50%">
                    
                        USCitizen
                    
                </td>
            </tr>
            <tr>
                <td width="50%">
                    
                        Are You Married?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            <tr>
                <td width="50%">
                    
                        Are You Active Military With A Different Home State Of
                        Record?
                    
                </td>
                <td width="50%">
                    
                        No
                    
                </td>
            </tr>
            <tr>
                <th colspan="2">Spouse Information</th>
            </tr>
            <tr>
                <td width="50%">
                    
                        First Name
                    
                </td>
                <td width="50%">
                    
                        Test
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Last Name
                    
                </td>
                <td width="50%">
                    
                        Spouse
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Email
                    
                </td>
                <td width="50%">
                    
                        Dfsdf@mail.com
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Phone
                    
                </td>
                <td width="50%">
                    
                        2342342
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Date of Birth
                    
                </td>
                <td width="50%">
                    
                        10/10/1985
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Job Title
                    
                </td>
                <td width="50%">
                    
                        Test ob
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Visa
                    
                </td>
                <td width="50%">
                    
                        USCitizen
                    
                </td>
            </tr>
            <tr>
                <th colspan="2">Additional Information</th>
            </tr>
            <tr>
                <td width="50%">
                    
                        Do you currently have an estate plan in place? (Wills
                        Trust POAs etc.)
                    
                </td>
                <td width="50%">
                    
                        No
                    
                </td>
            </tr>
            <tr>
                <td width="50%">
                    
                        Is The Cohan Rule watched?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        State
                    
                </td>
                <td width="50%">
                    
                        Florida
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        County
                    
                </td>
                <td width="50%">
                    
                        Sdfsd
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        City or Township
                    
                </td>
                <td width="50%">
                    
                        Test
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        School District
                    
                </td>
                <td width="50%">
                    
                        Sdf
                    
                </td>
            </tr>
            <tr>
                <th colspan="2">Dependent Information</th>
            </tr>

            <tr>
                <td width="50%">
                    
                        Do you have any dependents to claim? If yes, please
                        enter the information below.
                    
                </td>
                <td width="50%">
                    
                        No
                    
                </td>
            </tr>
            <tr>
                <td colspan="2">
            <table id="child_table" style="width: 96%;margin-left: 2%;">
                    <tr>
                            <th>
                                    Full legal name
                            </th>    
                            <th>
                                    DOB
                            </th>    
                            <th>
                                    Relation
                            </th>    
                            <th>
                                    Month lived
                            </th>    
                            <th>
                                    Married as
                            </th>    
                            <th>
                                    SSN
                            </th>    
                            <th>
                                    Fulltime student
                            </th>    
                            <th>
                                    Received income
                            </th>
                    </tr>
                    <tr>
                            <td width="12%">
                    
                            Test legal
                            
                            </td>
                            <td width="12%">
                            
                            12/10/2000
                            
                            </td>
                            <td width="12%">
                    
                            Son
                            
                            </td>
                            <td width="12%">
                            
                            100
                            
                            </td>
                            <td width="12%">
                    
                            no
                            
                            </td>
                            <td width="12%">
                            
                            123456
                            
                            </td>
                            <td width="12%">
                    
                            Yes
                            
                            </td>
                            <td width="12%">
                            
                            Yes
                            
                            </td>
                    </tr>
            </table>
            <tr>
                <th colspan="2">Report of Foreign Bank and Financial Accounts (FBAR)</th>
            </tr>
            <tr>
                <td width="50%">
                    
                        Did you have more than $10000 in the aggregate combined
                        in any foreign bank accounts outside of the United States?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            <tr>
                <th colspan="2">Rental Property Information</th>
            </tr>
            <tr>
                <td width="50%">
                    
                        Do you own a second house that you rent out all the
                        time? Do you own a vacation home that you rent out when you or your
                        family isn't using it?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            <tr>
                <th colspan="2">Side Income Generating Activities</th>
            </tr>
            <tr>
                <td width="50%">
                    
                        Did you have any income or expenses from a business or
                        hobby in 2019?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            <tr>
                <th colspan="2">Charitable Donations</th>
            </tr>
            <tr>
                <td width="50%">
                    
                        Did you make any cash contributions in 2019?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Frequency
                    
                </td>
                <td width="50%">
                    
                        One Time
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Amount
                    
                </td>
                <td width="50%">
                    
                       $234
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Calculated price of frequency contribution
                    
                </td>
                <td width="50%">
                    
                        $234
                    
                </td>
            </tr>
            <tr>
                <td width="50%">
                    
                        Did you make any donations of property in 2019?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                    <td colspan="2">
                <table id="child_table" style="width: 80%;margin-left: 10%;">
                        <tr>
                                <th>
                                        Description
                                </th>    
                                <th>
                                        Value
                                </th>    
                        </tr>
                    <tr>
                        <td width="50%">
                    
                                Sdfsd
                            
                        </td>
                        <td width="50%">
                            
                               $230 
                            
                        </td>
                    </tr>
                    <tr>
                        <td width="50%">
                    
                                Sdfsdsdfds
                            
                        </td>
                        <td width="50%">
                            
                               $250 
                            
                        </td>
                    </tr>
                </table>
        </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Value
                    
                </td>
                <td width="50%">
                    
                        $21
                    
                </td>
            </tr>
            

            <tr>
                <td width="50%">
                    
                        Did you have any Volunteering Expenses in 2019?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Description
                    
                </td>
                <td width="50%">
                    
                        Dfgdfq
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Value
                    
                </td>
                <td width="50%">
                    
                        $345
                    
                </td>
            </tr>
            <tr>
                <th colspan="2">Education and Tuition related expenses</th>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        In 2019 were you or anyone to be listed on your return
                        enrolled in a college or university?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Description
                    
                </td>
                <td width="50%">
                    
                        Dsfds
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Value
                    
                </td>
                <td width="50%">
                    
                        $23
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Did you pay any student loan interest in 2019?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Description
                    
                </td>
                <td width="50%">
                    
                        Dfgq
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Value
                    
                </td>
                <td width="50%">
                    
                        $12
                    
                </td>
            </tr>
            <tr>
                <th colspan="2">Childcare & Adoption Expenses</th>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Did you have any expenses associated with day care
                        nannies or babysitters in 2019: these are generally only
                        deductible if it was necessary to pursue employment.
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>

            <tr>
                <td width="50%">
                    
                        Description
                    
                </td>
                <td width="50%">
                    
                        Sdfds
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Value
                    
                </td>
                <td width="50%">
                    
                        $23
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        The next major topic has to do with your family. Did
                        you have any new dependents in the household in 2019?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Description
                    
                </td>
                <td width="50%">
                    
                        Ddsfds
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Value
                    
                </td>
                <td width="50%">
                    
                        $232
                    
                </td>
            </tr>
            <tr>
                <th colspan="2">Home Expenses</th>
            </tr>
            <tr>
                <td width="50%">
                    
                        Do you Own or Rent?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Did you have any mortgage interest in 2019?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Did you pay any real estate taxes in 2019?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Real Estate Taxes
                    
                </td>
                <td width="50%">
                    
                        $432
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Mortgage Insurance
                    
                </td>
                <td width="50%">
                    
                        $234
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Loan-Mandated Homeowner's Ins.
                    
                </td>
                <td width="50%">
                    
                        $23
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Did You Have Any Energy Efficient Upgrades?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            <tr>
                <th colspan="2">Small But Important Items</th>
            </tr>
            <tr>
                <td width="50%">
                    
                        Did you make any contribution to a Traditional or Roth
                        IRA in 2019?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            

            <tr>
                <td width="50%">
                    
                        Traditional IRA Contributions
                    
                </td>
                <td width="50%">
                    
                        $12
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Roth IRA Contributions
                    
                </td>
                <td width="50%">
                    
                        $123
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        ARE YOU A TEACHER?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Did you have any educator expenses as a teacher or
                        professor in 2019?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Educator Expenses
                    
                </td>
                <td width="50%">
                    
                        $332
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Are you self employed with a home-office?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Total Mortgage / Rent Payments for the Year
                    
                </td>
                <td width="50%">
                    
                        $33
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Total HOA
                    
                </td>
                <td width="50%">
                    
                        $23
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Total Utilities
                    
                </td>
                <td width="50%">
                    
                        $23
                    
                </td>
            </tr>
            <tr>
                <th colspan="2">Employee Business Expenses</th>
            </tr>
            <tr>
                <td width="50%">
                    
                        Did you have any work-related parking fees or parking
                        garage costs in 2019?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Total Parking Fees
                    
                </td>
                <td width="50%">
                    
                        $233
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Did you have any work-related Tollway charges or Toll
                        Tag costs in 2019?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Tollway or Toll Tag Costs
                    
                </td>
                <td width="50%">
                    
                        $23
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Did you have any local transportation expenses, such as
                        Uber, Lyft, Metro Pass, Taxis, Subways in 2019?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Total Local Transportation
                    
                </td>
                <td width="50%">
                    
                        $4343
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        This may have been related to a conference that you
                        attended or an out-of-town business meeting or temporary job site.
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Total Travel Costs
                    
                </td>
                <td width="50%">
                    
                        $3443
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Make
                    
                </td>
                <td width="50%">
                    
                        Nissan
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Model
                    
                </td>
                <td width="50%">
                    
                        2000
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Price or Value-at-Acquisition
                    
                </td>
                <td width="50%">
                    
                        $3000
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Make
                    
                </td>
                <td width="50%">
                    
                        Toyota
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Model
                    
                </td>
                <td width="50%">
                    
                        1999
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Price or Value-at-Acquisition
                    
                </td>
                <td width="50%">
                    
                        $2500
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Weekly Estimated Total
                    
                </td>
                <td width="50%">
                    
                        $322
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Spouse's Weekly Estimated Total
                    
                </td>
                <td width="50%">
                    
                        $332
                    
                </td>
            </tr>
            

            <tr>
                <td width="50%">
                    
                        Primary Number Of Oil Changes
                    
                </td>
                <td width="50%">
                    
                        4
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Primary Cost Per Change
                    
                </td>
                <td width="50%">
                    
                        $32
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Spouse Number Of Oil Changes
                    
                </td>
                <td width="50%">
                    
                        3
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Spouse Cost Per Change
                    
                </td>
                <td width="50%">
                    
                        $32
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Did you have any tire changes or brakes replaced in
                        2019? If so, what was the total cost?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Total Tire or Brake Costs
                    
                </td>
                <td width="50%">
                    
                        $2332
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Total Tire or Brake Costs (Spouse)
                    
                </td>
                <td width="50%">
                    
                        $232
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Did you have any other repairs or maintenance costs
                        associated with your work vehicle in 2019?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Total Repairs or Maintenance
                    
                </td>
                <td width="50%">
                    
                        $34
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Total Repairs or Maintenance (Spouse)
                    
                </td>
                <td width="50%">
                    
                        $41
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Total Auto Insurance
                    
                </td>
                <td width="50%">
                    
                        $23
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Did you have any annual registration, safety
                        inspection, or vehicle taxes in 2019?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            

            <tr>
                <td width="50%">
                    
                        Total Costs
                    
                </td>
                <td width="50%">
                    
                        $443
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Total Costs (Spouse)
                    
                </td>
                <td width="50%">
                    
                        $34
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Did you have any work-related meal expenses in 2019?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Primary Times Per/Week
                    
                </td>
                <td width="50%">
                    
                        2
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Primary Cost Per/Meal
                    
                </td>
                <td width="50%">
                    
                        $23
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Spouse Times Per/Week
                    
                </td>
                <td width="50%">
                    
                        1
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Spouse Cost Per/Meal
                    
                </td>
                <td width="50%">
                    
                        $23
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Did you have any expenses associated with gifts or
                        entertainment related to any coworkers or clients in 2019?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Description
                    
                </td>
                <td width="50%">
                    
                        Dsfsd
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Value
                    
                </td>
                <td width="50%">
                    
                        $20
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Did you purchase a new office desk, chair, or other
                        office furniture in 2019?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>

            <tr>
                <td width="50%">
                    
                        Description
                    
                </td>
                <td width="50%">
                    
                        Sdfsd
                    
                </td>
            </tr> 
            <tr>
                <td width="50%">
                    
                        Value
                    
                </td>
                <td width="50%">
                    
                        $23
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Did you purchase any new computer, laptop, phone,
                        tablet, printer, ink, other office equipment or software in 2019?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Description
                    
                </td>
                <td width="50%">
                    
                        Fdfg
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Value
                    
                </td>
                <td width="50%">
                    
                        $11
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Did you have any job-seeking expenses in 2019?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Description
                    
                </td>
                <td width="50%">
                    
                        Dsfds
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Value
                    
                </td>
                <td width="50%">
                    
                        $232
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Monthly Cell Phone Bill
                    
                </td>
                <td width="50%">
                    
                        $13
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Monthly Cell Phone Bill (Spouse)
                    
                </td>
                <td width="50%">
                    
                        $23
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Did you have any expenses associated with any
                        networking events or conferences in 2019?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            

            <tr>
                <td width="50%">
                    
                        Networking Expenses
                    
                </td>
                <td width="50%">
                    
                        $344
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Networking Expenses (Spouse)
                    
                </td>
                <td width="50%">
                    
                        $244
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Did you have any continuing education, training, or
                        certification expenses in 2019?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Description
                    
                </td>
                <td width="50%">
                    
                        Ewerew
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Value
                    
                </td>
                <td width="50%">
                    
                        $34
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Did you have any professional exam or license renewal
                        fees in 2019?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Descrtion
                    
                </td>
                <td width="50%">
                    
                        Sdfdsf
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Value
                    
                </td>
                <td width="50%">
                    
                        $23
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Did you have any personal professional development
                        expenses in 2019?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Professional Development
                    
                </td>
                <td width="50%">
                    
                        $33
                    
                </td>
            </tr>

            <tr>
                <td width="50%">
                    
                    Professional Development (Spouse)
                    
                </td>
                <td width="50%">
                    
                        $44
                    
                </td>
            </tr>
            

            <tr>
                <td width="50%">
                    
                        <a name="page12"></a>
                        Did you have any membership fees for any professional
                        organizations in 2019? This includes any union dues in
                        2019?
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Description
                </td>
                <td width="50%">
                    
                        Dfgfd
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Value
                </td>
                <td width="50%">
                    
                        $34
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Did you have any expenses associated with any
                        subscription or research expenses associated with your job?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Description
                    
                </td>
                <td width="50%">
                    
                        Sdfds
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        How much did you pay each month for your home Internet
                        service in 2019?
                    
                </td>
                <td width="50%">
                    
                        $23
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Monthly Internet Bill
                    
                </td>
                <td width="50%">
                    
                        $23
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Did you have any expenses for any equipment,
                        instruments, or tools in 2019?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Description
                    
                </td>
                <td width="50%">
                    
                        Sdfds
                    
                </td>
            </tr>
           
            <tr>
                <td width="50%">
                    
                        Value
                    
                </td>
                <td width="50%">
                    
                        $23
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Did you have any expenses for purchasing any machinery?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Description
                    
                </td>
                <td width="50%">
                    
                        Dfgdf
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Value
                    
                </td>
                <td width="50%">
                    
                        $23
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Did you have any expenses for uniforms?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Description
                    
                </td>
                <td width="50%">
                    
                        Ffdg
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Value
                    
                </td>
                <td width="50%">
                    
                        $33
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Did you have any other work clothing expenses, like
                        work shoes or work boots?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Description
                    
                </td>
                <td width="50%">
                    
                        Sdfdsf
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Value
                    
                </td>
                <td width="50%">
                    
                        $23
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Did you have any dry cleaning, tailoring, or altering
                        expenses for any of the uniforms or work clothing?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Description
                    
                </td>
                <td width="50%">
                    
                        Dfd
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Value
                    
                </td>
                <td width="50%">
                    
                        $34
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Are You Emergency Personnel? (Military, Police, Fire,
                        EMS)
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Did you purchase any new weapons in 2019?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Description
                    
                </td>
                <td width="50%">
                    
                        Sdfd
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Value
                    
                </td>
                <td width="50%">
                    
                        $34
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Any Emergency Personnel equipment; this can include
                        special lightweight body armor camel packs for hot environments
                        magazines tactical flashlights tasers tear gas med kits etc
                    
                </td>
                <td width="50%">
                    
                        Sdfd
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Any Emergency Personnel equipment; this can include
                        special lightweight body armor camel packs for hot environments
                        magazines tactical flashlights tasers tear gas med kits etc
                    
                </td>
                <td width="50%">
                    
                        34
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Any off-duty training expenses for ammo shooting range
                        fees maybe even setting up your own shooting range?
                    
                </td>
                <td width="50%">
                    
                        Fsd
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Any off-duty training expenses for ammo shooting range
                        fees maybe even setting up your own shooting range?
                    
                </td>
                <td width="50%">
                    
                        $23
                    
                </td>
            </tr>
            <tr>
                <th colspan="2">Out of Pocket Medical Expenses</th>
            </tr>
            <tr>
                <td width="50%">
                    
                        Insurance Premiums
                    
                </td>
                <td width="50%">
                    
                        $21
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Insurance Premiums (Non-Working Members)
                    
                </td>
                <td width="50%">
                    
                        $23
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Did you have any out-of-pocket expenses for glasses,
                        contacts, or eye exams in 2019?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Out-of-pocket Vision
                    
                </td>
                <td width="50%">
                    
                        $344
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Out of Pocket Vision (Non-Working Members)
                    
                </td>
                <td width="50%">
                    
                        $32
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Did you have any out-of-pocket expenses for
                        prescriptions?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Out-of-pocket Medications
                    
                </td>
                <td width="50%">
                    
                        $23432
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Did you have any out-of-pocket expenses for dental
                        check-ups or any visits at all to the dentist in 2019?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Out-of-pocket Dental
                    
                </td>
                <td width="50%">
                    
                        $34
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    
                        Did you have any out-of-pocket expenses for any routine
                        checkups, injuries, or surgeries?
                    
                </td>
                <td width="50%">
                    
                        Yes
                    
                </td>
            </tr>
            
            <tr>
                <td width="50%">
                    Out-of-pocket Other Medical
                </td>
                <td width="50%">
                    $34
                </td>
            </tr>
    </tbody>
</table>

</body>

</html>
