<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0 " />
    <meta name="format-detection" content="telephone=no"/>
    <link rel="manifest" href="/manifest.json">
    <meta name="theme-color" content="#ffffff">
    <title>Ai Tax Online | A Product of Castro & Co.</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <!-- Bootstrap -->
    <style type="text/css">
        *{-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;}
        p {margin: 0 0 10px; line-height: 24px;}
        a {text-decoration: none; display: inline-block;}
        .logo {margin-bottom: 30px; text-align: center;}
        table tbody tr td{border-bottom: 1px solid #f0f0f0; padding: 5px 0;}
        table tbody tr td:last-child {text-align: right;}
        table tbody tr:last-child td {border: 0;}
        a {color: #484545;}
    </style>
</head>
<body style="font-size: 13px; font-weight: normal; color: #484545; font-family: 'Open Sans', sans-serif; background-color: #fff">
<div style="width: 600px; margin: auto; padding: 60px 0;">
    <div align="center" class="img-container center fixedwidth" style="padding-right: 0px;padding-left: 0px;">
        <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr style="line-height:0px"><td style="padding-right: 0px;padding-left: 0px;" align="center"><![endif]-->

        <?php /*?><img align="center" alt="Image" border="0" class="center fixedwidth" src="<?php echo $this->request->host().$this->Url->assetUrl('/images/logo.png') ?>" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; clear: both; border: 0; height: auto; float: none; width: 100%; max-width: 158px; display: block;" title="Image" width="158"/><?php */?>
        <!--[if mso]></td></tr></table><![endif]-->
    </div>
    <b style="margin-bottom: 20px; display: block;">Hey There,</b>
    <p>{{$username}} has chosen to unlock the application to make changes</p>
    <br/>
    <br/><br/>
    <div style="">
        <span>Ai Tax Online | A Product of Castro & Co.</span></div>
</div>
</body>
</html>
