<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ai Tax Online | A Product of Castro & Co.</title>
    <style>
        #aitax {
            border-collapse: collapse;
            width: 100%;
        }

        #aitax td,
        #aitax th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #aitax tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        #aitax tr:hover {
            background-color: #ddd;
        }

        #aitax th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4c78af;
            color: white;
        }

        #child_table th {
            padding-top: 2px;
            padding-bottom: 2px;
            text-align: left;
            background-color: #b6b767;
            color: white;
            font-weight: 300;
        }

        .red {
            color: red;
        }

        .removed {
            text-decoration: line-through;
            color: red;
        }
    </style>
</head>

<body>

<div style="font-size: 26;font-weight: bold;">
    <IMG SRC="https://aitaxonline.com/tax-app-v-backend/public/images/sLogo.jpg" alt="[Image]" height="100">
    <div style="margin: -60px 150px 40px;">Ai Tax Details</div>
</div>
<br/>

<strong>Name:</strong>
@if(isset($prev_data['personal_info']))
    @if($personal_info->first_name.' '.$personal_info->last_name !==$prev_data['personal_info']->first_name.' '.$prev_data['personal_info']->last_name)
        {{$prev_data['personal_info']->first_name.' '.$prev_data['personal_info']->last_name}}
        <span class="red">{{$personal_info->first_name.' '.$personal_info->last_name}}</span>
    @else
        {{$personal_info->first_name.' '.$personal_info->last_name}}
    @endif
@else
    {{$personal_info->first_name.' '.$personal_info->last_name}}
@endif

<br/>
<strong>Email:</strong>
@if(isset($prev_data['personal_info']))
    @if($personal_info->email !==$prev_data['personal_info']->email)
        {{$prev_data['personal_info']->email}}
        <span class="red">{{$personal_info->email}}</span>
    @else
        {{$personal_info->email}}
    @endif
@else
    {{$personal_info->email}}
@endif

<br/>
<strong>Phone:</strong>
@if(isset($prev_data['personal_info']))
    @if($personal_info->country_code.' '.$personal_info->phone !== $prev_data['personal_info']->country_code.' '.$prev_data['personal_info']->phone)
        {{$prev_data['personal_info']->country_code.' '.$prev_data['personal_info']->phone}}
        <span class="red">{{$personal_info->country_code.' '.$personal_info->phone}}</span>
    @else
        {{$personal_info->country_code.' '.$personal_info->phone}}
    @endif
@else
    {{$personal_info->country_code.' '.$personal_info->phone}}
@endif

<br/>
<strong>Date submitted:</strong> {{$submited_at}}<br/>

<table id="aitax">
    <tr style="background-color: white;">
        <th>Question</th>
        <th>Answer</th>
    </tr>
    <tbody>
    @if(isset($personal_info))
        @php
            $var_name = 'personal_info'
        @endphp
        <tr>
            <th colspan="2">Personal Information</th>
        </tr>
        <tr>
            <td width="50%">Date of Birth</td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($personal_info->date_of_birth !== $prev_data[$var_name]->date_of_birth)
                        {{$prev_data[$var_name]->date_of_birth}}
                        <span class="red">{{$personal_info->date_of_birth}}</span>
                    @else
                        {{$personal_info->date_of_birth}}
                    @endif
                @else
                    {{$personal_info->date_of_birth}}
                @endif

            </td>
        </tr>
        <tr>
            <td width="50%">Current Job Title</td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($personal_info->job_title !== $prev_data[$var_name]->job_title)
                        {{$prev_data[$var_name]->job_title}}
                        <span class="red">{{$personal_info->job_title}}</span>
                    @else
                        {{$personal_info->job_title}}
                    @endif
                @else
                    {{$personal_info->job_title}}
                @endif
            </td>
        </tr>

        <tr>
            <td width="50%">

                Address

            </td>
            <td width="50%">
                @php
                    $address=$personal_info->address!==''?$personal_info->address:$personal_info->street_address.' '.$personal_info->state.' '.$personal_info->city.' '.$personal_info->country;
                    $prev_address= isset($prev_data['personal_info'])?(isset($prev_data['personal_info']->address)?$prev_data['personal_info']->address:$prev_data['personal_info']->street_address .' '.$prev_data['personal_info']->state.' '.$prev_data['personal_info']->city.' '.$prev_data['personal_info']->country):null;
                @endphp

                @if(isset($prev_address))
                    @if($address !== $prev_address)
                        {{$prev_address}}
                        <span class="red">{{$address}}</span>
                    @else
                        {{$address}}
                    @endif
                @else
                    {{$address}}
                @endif
            </td>
        </tr>
        <tr>
            <td width="50%">

                Zip code

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($personal_info->zip !== $prev_data[$var_name]->zip)
                        {{$prev_data[$var_name]->zip}}
                        <span class="red">{{$personal_info->zip}}</span>
                    @else
                        {{$personal_info->zip}}
                    @endif
                @else
                    {{$personal_info->zip}}
                @endif

            </td>
        </tr>
        <tr>
            <td width="50%">

                Social Security

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($personal_info->social_security !== $prev_data[$var_name]->social_security)
                        {{$prev_data[$var_name]->social_security}}
                        <span class="red">{{$personal_info->social_security}}</span>
                    @else
                        {{$personal_info->social_security}}
                    @endif
                @else
                    {{$personal_info->social_security}}
                @endif

            </td>
        </tr>

        <tr>
            <td width="50%">

                Visa

            </td>
            <td width="50%">
                <?php
                $visa='';
                $prev_visa='';
                switch ($personal_info->citizenship_status) {
                    case '0':
                        $citizen = 'US Citizen';
                        break;
                    case '1':
                        $citizen = 'Green Card Holder';
                        break;
                    case '2':
                        $citizen = 'Visa Holder';
                        $visa = $personal_info->visa_type;
                        break;
                }

                ?>
                @if(isset($prev_data['personal_info']))
                    <?php

                    switch ($prev_data['personal_info']->citizenship_status) {
                        case '0':
                            $prev_citizen = 'US Citizen';
                            break;
                        case '1':
                            $prev_citizen = 'Green Card Holder';
                            break;
                        case '2':
                            $prev_citizen = 'Visa Holder';
                            $prev_visa = $prev_data['personal_info']->visa_type;
                            break;
                    }

                    ?>
                @endif
                @if(isset($prev_data[$var_name]))
                    @if($citizen !== $prev_citizen || $visa!== $prev_visa )
                        {{$prev_citizen}}{{isset($prev_visa)?$prev_visa:''}}
                        <span class="red">{{$citizen}}{{isset($visa)?$visa:''}}</span>
                    @else
                        {{$citizen}}{{isset($visa)?$visa:''}}
                    @endif
                @else
                    {{$citizen}}{{isset($visa)?$visa:''}}
                @endif
            </td>
        </tr>
        <tr>
            <td width="50%">

                Are You Married?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($personal_info->marital_status !== $prev_data[$var_name]->marital_status)
                        {{$prev_data[$var_name]->marital_status?'Yes':'No'}}
                        <span class="red">{{$personal_info->marital_status?'Yes':'No'}}</span>
                    @else
                        {{$personal_info->marital_status?'Yes':'No'}}
                    @endif
                @else
                    {{$personal_info->marital_status?'Yes':'No'}}
                @endif

            </td>
        </tr>
        <tr>
            <td width="50%">

                Are You Active Military With A Different Home State Of
                Record?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($personal_info->active_military !== $prev_data[$var_name]->active_military)
                        {{$prev_data[$var_name]->active_military?'Yes '.$prev_data[$var_name]->active_military_state:'No'}}
                        <span
                            class="red">{{$personal_info->active_military?'Yes '.$personal_info->active_military_state:'No'}}</span>
                    @else
                        {{$personal_info->active_military?'Yes '.$personal_info->active_military_state:'No'}}
                    @endif
                @else
                    {{$personal_info->active_military?'Yes '.$personal_info->active_military_state:'No'}}
                @endif
            </td>
        </tr>
    @endif

    @if(isset($spouse_info) && $personal_info->marital_status=='1')
        @php
            $var_name = 'spouse_info'
        @endphp
        <tr>
            <th colspan="2">Spouse Information</th>
        </tr>

        <tr>
            <td width="50%">

                First Name

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($spouse_info->first_name !== $prev_data[$var_name]->first_name)
                        {{$prev_data[$var_name]->first_name}}
                        <span class="red">{{$spouse_info->first_name}}</span>
                    @else
                        {{$spouse_info->first_name}}
                    @endif
                @else
                    {{$spouse_info->first_name}}
                @endif
            </td>
        </tr>

        <tr>
            <td width="50%">

                Last Name

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($spouse_info->last_name !== $prev_data[$var_name]->last_name)
                        {{$prev_data[$var_name]->last_name}}
                        <span class="red">{{$spouse_info->last_name}}</span>
                    @else
                        {{$spouse_info->last_name}}
                    @endif
                @else
                    {{$spouse_info->last_name}}
                @endif


            </td>
        </tr>

        <tr>
            <td width="50%">

                Email

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($spouse_info->email !== $prev_data[$var_name]->email)
                        {{$prev_data[$var_name]->email}}
                        <span class="red">{{$spouse_info->email}}</span>
                    @else
                        {{$spouse_info->email}}
                    @endif
                @else
                    {{$spouse_info->email}}
                @endif

            </td>
        </tr>
        <tr>
            <td width="50%">

                Social Security

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($spouse_info->social_security !== $prev_data[$var_name]->social_security)
                        {{$prev_data[$var_name]->social_security}}
                        <span class="red">{{$spouse_info->social_security}}</span>
                    @else
                        {{$spouse_info->social_security}}
                    @endif
                @else
                    {{$spouse_info->social_security}}
                @endif

            </td>
        </tr>
        <tr>
            <td width="50%">

                Phone

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($spouse_info->country_code.' '.$spouse_info->phone!== $prev_data[$var_name]->country_code.' '.$prev_data[$var_name]->phone)
                        {{$prev_data[$var_name]->country_code.' '.$prev_data[$var_name]->phone}}
                        <span class="red">{{$spouse_info->country_code.' '.$spouse_info->phone}}</span>
                    @else
                        {{$spouse_info->country_code.' '.$spouse_info->phone}}
                    @endif
                @else
                    {{$spouse_info->country_code.' '.$spouse_info->phone}}
                @endif
            </td>
        </tr>

        <tr>
            <td width="50%">

                Date of Birth

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($spouse_info->date_of_birth !== $prev_data[$var_name]->date_of_birth)
                        {{$prev_data[$var_name]->date_of_birth}}
                        <span class="red">{{$spouse_info->date_of_birth}}</span>
                    @else
                        {{$spouse_info->date_of_birth}}
                    @endif
                @else
                    {{$spouse_info->date_of_birth}}
                @endif

            </td>
        </tr>

        <tr>
            <td width="50%">

                Job Title

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($spouse_info->job_title !== $prev_data[$var_name]->job_title)
                        {{$prev_data[$var_name]->job_title}}
                        <span class="red">{{$spouse_info->job_title}}</span>
                    @else
                        {{$spouse_info->job_title}}
                    @endif
                @else
                    {{$spouse_info->job_title}}
                @endif
            </td>
        </tr>

        <tr>
            <td width="50%">

                Visa

            </td>
            <td width="50%">

                <?php

                switch ($spouse_info->citizenship_status) {
                    case '0':
                        $citizen = 'US Citizen';
                        break;
                    case '1':
                        $citizen = 'Green Card Holder';
                        break;
                    case '2':
                        $citizen = 'Visa Holder';
                        $visa_spouse = $spouse_info->visa_type;
                        break;
                    case '3':
                        $citizen = 'Nonresident';
                        break;
                }

                ?>

                @if(isset($prev_data['spouse_info']))
                    <?php

                    switch ($prev_data['spouse_info']->citizenship_status) {
                        case '0':
                            $prev_citizen = 'US Citizen';
                            break;
                        case '1':
                            $prev_citizen = 'Green Card Holder';
                            break;
                        case '2':
                            $prev_citizen = 'Visa Holder';
                            $prev_visa = $prev_data['spouse_info']->visa_type;
                            break;
                    }

                    ?>
                @endif
                @if(isset($prev_data[$var_name]))
                    @if($citizen !== $prev_citizen || $visa_spouse!== $prev_visa )
                        {{$prev_citizen}}{{isset($prev_visa)?$prev_visa:''}}
                        <span class="red">{{$citizen}}{{isset($visa_spouse)?$visa_spouse:''}}</span>
                    @else
                        {{$citizen}}{{isset($visa_spouse)?$visa_spouse:''}}
                    @endif
                @else
                    {{$citizen}}{{isset($visa_spouse)?$visa_spouse:''}}
                @endif


            </td>
        </tr>
    @endif
    <tr>
        <th colspan="2">Additional Information</th>
    </tr>
    @if(isset($additional_info))
        @php
            $var_name = 'additional_info'
        @endphp
        @if($personal_info->citizenship_status!=='0')
            <tr>
                <td width="50%">

                    When did you first establish U.S. tax residency?

                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))
                        @if($additional_info->us_tax_residency !== $prev_data[$var_name]->us_tax_residency)
                            {{$prev_data[$var_name]->us_tax_residency}}
                            <span class="red">{{$additional_info->us_tax_residency}}</span>
                        @else
                            {{$additional_info->us_tax_residency}}
                        @endif
                    @else
                        {{$additional_info->us_tax_residency}}
                    @endif
                </td>
            </tr>
        @endif
        @if($personal_info->marital_status=='1')
            <tr>
                <td width="50%">
                    When did your spouse first establish U.S. tax residency?
                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))
                        @if($additional_info->us_tax_residency_spouse !== $prev_data[$var_name]->us_tax_residency_spouse)
                            {{$prev_data[$var_name]->us_tax_residency_spouse}}
                            <span class="red">{{$additional_info->us_tax_residency_spouse}}</span>
                        @else
                            {{$additional_info->us_tax_residency_spouse}}
                        @endif
                    @else
                        {{$additional_info->us_tax_residency_spouse}}
                    @endif
                </td>
            </tr>
        @endif
        <tr>
            <td width="50%">

                Do you have plans to change your immigration status?


            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($additional_info->plan_to_change_immigration !== $prev_data[$var_name]->plan_to_change_immigration)
                        @switch($prev_data[$var_name]->plan_to_change_immigration)
                            @case(0)
                            {{'Yes'}}
                            @break @case(1)
                            {{'No'}}
                            @break @case(2)
                            {{'Unsure'}}
                            @break
                        @endswitch
                        <span class="red"> @switch($additional_info->plan_to_change_immigration)
                                @case(0)
                                {{'Yes'}}
                                @break @case(1)
                                {{'No'}}
                                @break @case(2)
                                {{'Unsure'}}
                                @break
                            @endswitch</span>
                    @else
                        @switch($additional_info->plan_to_change_immigration)
                            @case(0)
                            {{'Yes'}}
                            @break @case(1)
                            {{'No'}}
                            @break @case(2)
                            {{'Unsure'}}
                            @break
                        @endswitch
                    @endif
                @else
                    @switch($additional_info->plan_to_change_immigration)
                        @case(0)
                        {{'Yes'}}
                        @break @case(1)
                        {{'No'}}
                        @break @case(2)
                        {{'Unsure'}}
                        @break
                    @endswitch
                @endif


            </td>
        </tr>
        <tr>
            <td width="50%">

                Do you have plans to depart the U.S. this year or the next?


            </td>
            <td width="50%">

                @if(isset($prev_data['additional_info']))
                    @if($additional_info->plan_to_depart !== $prev_data[$var_name]->plan_to_depart)
                        @switch($prev_data[$var_name]->plan_to_depart)
                            @case(0)
                            {{'Yes'}}
                            @break @case(1)
                            {{'No'}}
                            @break @case(2)
                            {{'Unsure'}}
                            @break
                        @endswitch
                        <span class="red"> @switch($additional_info->plan_to_depart)
                                @case(0)
                                {{'Yes'}}
                                @break @case(1)
                                {{'No'}}
                                @break @case(2)
                                {{'Unsure'}}
                                @break
                            @endswitch</span>
                    @else
                        @switch($additional_info->plan_to_depart)
                            @case(0)
                            {{'Yes'}}
                            @break @case(1)
                            {{'No'}}
                            @break @case(2)
                            {{'Unsure'}}
                            @break
                        @endswitch
                    @endif
                @else
                    @switch($additional_info->plan_to_depart)
                        @case(0)
                        {{'Yes'}}
                        @break @case(1)
                        {{'No'}}
                        @break @case(2)
                        {{'Unsure'}}
                        @break
                    @endswitch
                @endif
            </td>
        </tr>
        <tr>
            <td width="50%">

                Do you currently have an estate plan in place? (Wills
                Trust POAs etc.)

            </td>
            <td width="50%">
                @if(isset($prev_data['additional_info']))
                    @if($additional_info->estate_plan_in_place !== $prev_data[$var_name]->estate_plan_in_place)
                        @switch($prev_data[$var_name]->estate_plan_in_place)
                            @case(0)
                            {{'Yes'}}
                            @break @case(1)
                            {{'No'}}
                            @break @case(2)
                            {{'Unsure'}}
                            @break
                        @endswitch
                        <span class="red"> @switch($additional_info->estate_plan_in_place)
                                @case(0)
                                {{'Yes'}}
                                @break @case(1)
                                {{'No'}}
                                @break @case(2)
                                {{'Unsure'}}
                                @break
                            @endswitch</span>
                    @else
                        @switch($additional_info->estate_plan_in_place)
                            @case(0)
                            {{'Yes'}}
                            @break @case(1)
                            {{'No'}}
                            @break @case(2)
                            {{'Unsure'}}
                            @break
                        @endswitch
                    @endif
                @else
                    @switch($additional_info->estate_plan_in_place)
                        @case(0)
                        {{'Yes'}}
                        @break @case(1)
                        {{'No'}}
                        @break @case(2)
                        {{'Unsure'}}
                        @break
                    @endswitch
                @endif
            </td>
        </tr>
    @endif
    <tr>
        <th colspan="2">Basic Information</th>
    </tr>
    @if(isset($basic_info))
        @php
            $var_name = 'basic_info'
        @endphp
        <tr>
            <td width="50%">

                State

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($basic_info->state !== $prev_data[$var_name]->state)
                        {{$prev_data[$var_name]->state}}
                        <span class="red">{{$basic_info->state}}</span>
                    @else
                        {{$basic_info->state}}
                    @endif
                @else
                    {{$basic_info->state}}
                @endif


            </td>
        </tr>

        <tr>
            <td width="50%">

                County

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($basic_info->county !== $prev_data[$var_name]->county)
                        {{$prev_data[$var_name]->county}}
                        <span class="red">{{$basic_info->county}}</span>
                    @else
                        {{$basic_info->county}}
                    @endif
                @else
                    {{$basic_info->county}}
                @endif
            </td>
        </tr>

        <tr>
            <td width="50%">

                City or Township

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($basic_info->city !== $prev_data[$var_name]->city)
                        {{$prev_data[$var_name]->city}}
                        <span class="red">{{$basic_info->city}}</span>
                    @else
                        {{$basic_info->city}}
                    @endif
                @else
                    {{$basic_info->city}}
                @endif


            </td>
        </tr>

        <tr>
            <td width="50%">

                School District

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($basic_info->school !== $prev_data[$var_name]->school)
                        {{$prev_data[$var_name]->school}}
                        <span class="red">{{$basic_info->school}}</span>
                    @else
                        {{$basic_info->school}}
                    @endif
                @else
                    {{$basic_info->school}}
                @endif
            </td>
        </tr>
    @endif
    <tr>
        <th colspan="2">Dependent Information</th>
    </tr>
    @if(isset($dependent_info))
        <tr>
            <td width="50%">

                Do you have any dependents to claim? If yes, please
                enter the information below.

            </td>
            <td width="50%">
                @if(isset($prev_data['dependent_info']))
                    @if($dependent_info->have_dependents !== $prev_data['dependent_info']->have_dependents )
                        <span
                            class="red">
                            {{$prev_data['dependent_info']->have_dependents?'Yes':'No'}}</span>
                    @endif
                @endif
                {{$dependent_info->have_dependents?'Yes':'No'}}

            </td>
        </tr>
        <tr>

            <td colspan="2">
                @if($dependent_info->have_dependents)
                    @if(isset($prev_data['dependent_info']))
                        @if(count($dependent_info->dependents)>count($prev_data['dependent_info']->dependents))
                            @php
                                $size = count($dependent_info->dependents);
                            @endphp
                        @else
                            @php
                                $size =count($prev_data['dependent_info']->dependents);
                            @endphp
                        @endif
                    @else
                        @php
                            $size = count($dependent_info->dependents);
                        @endphp
                    @endif

                    @for($i=0;$i<$size;$i++)
                        @php
                            $dependent = isset($dependent_info->dependents[$i])?$dependent_info->dependents[$i]:null;
                            $prev_dependent = isset($prev_data['dependent_info']->dependents[$i])?$prev_data['dependent_info']->dependents[$i]:null;
                        @endphp
                        <table id="child_table" style="width: 96%;margin-left: 2%;">
                            <tr>
                                <th>
                                    Full legal name
                                </th>
                                <th>
                                    DOB
                                </th>
                                <th>
                                    Relation
                                </th>
                                <th>
                                    Month lived
                                </th>
                                <th>
                                    Married as
                                </th>
                                <th>
                                    SSN
                                </th>
                                <th>
                                    Fulltime student
                                </th>
                                <th>
                                    Received income
                                </th>
                            </tr>
                            <tr>
                                <td width="12%">
                                    @if(isset($prev_dependent) && isset($dependent))
                                        @if($dependent->full_legal_name !== $prev_dependent->full_legal_name )
                                            {{$prev_dependent->full_legal_name}}
                                            <span
                                                class="red">
                                                {{$dependent->full_legal_name}}
                                            </span>
                                        @else
                                            {{$dependent->full_legal_name}}
                                        @endif
                                    @else
                                        <span class="{{isset($dependent->full_legal_name)?'':'removed'}}">
                                            {{isset($prev_dependent->full_legal_name)?$prev_dependent->full_legal_name:''}}
                                        </span>
                                        <span
                                            class="{{(!isset($prev_dependent->full_legal_name)&&!isset($prev_data[$var_name]))?'':'red'}}">
                                            {{isset($dependent->full_legal_name)?$dependent->full_legal_name:''}}
                                        </span>
                                    @endif
                                </td>
                                <td width="12%">
                                    @if(isset($prev_dependent) && isset($dependent))
                                        @if($dependent->date_of_birth !== $prev_dependent->date_of_birth )
                                            {{$prev_dependent->date_of_birth}}
                                            <span
                                                class="red">
                                                {{$dependent->date_of_birth}}
                                            </span>
                                        @else
                                            {{$dependent->date_of_birth}}
                                        @endif
                                    @else
                                        <span class="{{isset($dependent->date_of_birth)?'':'removed'}}">
                                            {{isset($prev_dependent->date_of_birth)?$prev_dependent->date_of_birth:''}}
                                        </span>
                                        <span
                                            class="{{(!isset($prev_dependent->date_of_birth)&&!isset($prev_data[$var_name]))?'':'red'}}">
                                            {{isset($dependent->date_of_birth)?$dependent->date_of_birth:''}}
                                        </span>
                                    @endif

                                </td>
                                <td width="12%">
                                    @if(isset($prev_dependent) && isset($dependent))
                                        @if($dependent->relation !== $prev_dependent->relation )
                                            {{$prev_dependent->relation}}
                                            <span
                                                class="red">
                                                {{$dependent->relation}}
                                            </span>
                                        @else
                                            {{$dependent->relation}}
                                        @endif
                                    @else
                                        <span class="{{isset($dependent->relation)?'':'removed'}}">
                                            {{isset($prev_dependent->relation)?$prev_dependent->relation:''}}
                                        </span>
                                        <span
                                            class="{{(!isset($prev_dependent->relation)&&!isset($prev_data[$var_name]))?'':'red'}}">
                                            {{isset($dependent->relation)?$dependent->relation:''}}
                                        </span>
                                    @endif


                                </td>
                                <td width="12%">
                                    @if(isset($prev_dependent) && isset($dependent))
                                        @if($dependent->month_lived !== $prev_dependent->month_lived )
                                            {{$prev_dependent->month_lived}}
                                            <span
                                                class="red">
                                                {{$dependent->month_lived}}
                                            </span>
                                        @else
                                            {{$dependent->month_lived}}
                                        @endif
                                    @else
                                        <span class="{{isset($dependent->month_lived)?'':'removed'}}">
                                            {{isset($prev_dependent->month_lived)?$prev_dependent->month_lived:''}}
                                        </span>
                                        <span class="{{(!isset($prev_dependent->month_lived)&&!isset($prev_data[$var_name]))?'':'red'}}">
                                            {{isset($dependent->month_lived)?$dependent->month_lived:''}}
                                        </span>
                                    @endif


                                </td>
                                <td width="12%">
                                    @if(isset($prev_dependent) && isset($dependent))
                                        @if($dependent->married_as !== $prev_dependent->married_as )
                                            {{$prev_dependent->married_as?'Yes':'No'}}
                                            <span
                                                class="red">
                                                {{$dependent->married_as?'Yes':'No'}}
                                            </span>
                                        @else
                                            {{$dependent->married_as?'Yes':'No'}}
                                        @endif
                                    @else
                                        <span class="{{isset($dependent->married_as)?'':'removed'}}">
                                            {{isset($prev_dependent->married_as)?($prev_dependent->married_as?'Yes':'No'):''}}
                                        </span>
                                        <span class="{{(!isset($prev_dependent->married_as)&&!isset($prev_data[$var_name]))?'':'red'}}">

                                            {{isset($dependent->married_as)?($dependent->married_as?'Yes':'No'):''}}
                                        </span>
                                    @endif


                                </td>
                                <td width="12%">
                                    @if(isset($prev_dependent) && isset($dependent))
                                        @if($dependent->ssn !== $prev_dependent->ssn )
                                            {{$prev_dependent->ssn}}
                                            <span
                                                class="red">
                                                {{$dependent->ssn}}
                                            </span>
                                        @else
                                            {{$dependent->ssn}}
                                        @endif
                                    @else
                                        <span class="{{isset($dependent->ssn)?'':'removed'}}">
                                            {{isset($prev_dependent->ssn)?$prev_dependent->ssn:''}}
                                        </span>
                                        <span class="{{(!isset($prev_dependent->ssn)&&!isset($prev_data[$var_name]))?'':'red'}}">
                                            {{isset($dependent->ssn)?$dependent->ssn:''}}
                                        </span>
                                    @endif
                                </td>
                                <td width="12%">
                                    @if(isset($prev_dependent) && isset($dependent))
                                        @if($dependent->fulltime_student !== $prev_dependent->fulltime_student )
                                            {{$prev_dependent->fulltime_student?'Yes':'No'}}
                                            <span
                                                class="red">
                                                {{$dependent->fulltime_student?'Yes':'No'}}
                                            </span>
                                        @else
                                            {{$dependent->fulltime_student?'Yes':'No'}}
                                        @endif
                                    @else
                                        <span class="{{isset($dependent->fulltime_student)?'':'removed'}}">
                                            {{isset($prev_dependent->fulltime_student)?($prev_dependent->fulltime_student?'Yes':'No'):''}}
                                        </span>
                                        <span class="{{(!isset($prev_dependent->fulltime_student)&&!isset($prev_data[$var_name]))?'':'red'}}">
                                            {{isset($dependent->fulltime_student)?($dependent->fulltime_student?'Yes':'No'):''}}
                                        </span>
                                    @endif
                                </td>
                                <td width="12%">
                                    @if(isset($prev_dependent) && isset($dependent))
                                        @if($dependent->received_income !== $prev_dependent->received_income )
                                            {{$prev_dependent->received_income?'Yes':'No'}}
                                            <span
                                                class="red">
                                                {{$dependent->received_income?'Yes':'No'}}
                                            </span>
                                        @else
                                            {{$dependent->received_income?'Yes':'No'}}
                                        @endif
                                    @else
                                        <span class="{{isset($dependent->received_income)?'':'removed'}}">
                                            {{isset($prev_dependent->received_income)?($prev_dependent->received_income?'Yes':'No'):''}}
                                        </span>
                                        <span class="{{(!isset($prev_dependent->received_income)&&!isset($prev_data[$var_name]))?'':'red'}}">
                                            {{isset($dependent->received_income)?($dependent->received_income?'Yes':'No'):''}}
                                        </span>
                                    @endif
                                </td>
                            </tr>
                        </table>
        @endfor
    @endif
    @endif
    <tr>

        <th colspan="2">Report of Foreign Bank and Financial Accounts (FBAR)</th>
    </tr>
    <tr>
        <td width="50%">

            Did you have more than $10000 in the aggregate combined
            in any foreign bank accounts outside of the United States?

        </td>
        @if(isset($fbar))
            <td width="50%">
                @if(isset($prev_data['fbar']))
                    @if($fbar->fbr_outside_money !== $prev_data['fbar']->fbr_outside_money)
                        {{$prev_data['fbar']->fbr_outside_money?'Yes':'No'}}
                        <span class="red">{{$fbar->fbr_outside_money?'Yes':'No'}}</span>
                    @else
                        {{$fbar->fbr_outside_money?'Yes':'No'}}
                    @endif
                @else
                    {{$fbar->fbr_outside_money?'Yes':'No'}}
                @endif
            </td>
        @endif
    </tr>
    <tr>
        <th colspan="2">Rental Property Information</th>
    </tr>
    <tr>
        <td width="50%">

            Do you own a second house that you rent out all the
            time? Do you own a vacation home that you rent out when you or your
            family isn't using it?

        </td>
        @if(isset($rpi))
            <td width="50%">
                @if(isset($prev_data['rpi']))
                    @if($rpi->rental_home !== $prev_data['rpi']->rental_home)
                        {{$prev_data['rpi']->rental_home?'Yes':'No'}}
                        <span class="red">{{$rpi->rental_home?'Yes':'No'}}</span>
                    @else
                        {{$rpi->rental_home?'Yes':'No'}}
                    @endif
                @else
                    {{$rpi->rental_home?'Yes':'No'}}
                @endif
            </td>
        @endif
    </tr>
    <tr>
        <th colspan="2">Side Income Generating Activities</th>
    </tr>
    @if(isset($siga))
        <tr>
            <td width="50%">

                Did you have any income or expenses from a business or
                hobby in 2019?

            </td>
            <td width="50%">
                @if(isset($prev_data['siga']))
                    @if($siga->income_expense !== $prev_data['siga']->income_expense)
                        {{$prev_data['siga']->income_expense?'Yes':'No'}}
                        <span class="red">{{$siga->income_expense?'Yes':'No'}}</span>
                    @else
                        {{$siga->income_expense?'Yes':'No'}}
                    @endif
                @else
                    {{$siga->income_expense?'Yes':'No'}}
                @endif

            </td>
        </tr>
    @endif
    <tr>
        <th colspan="2">Charitable Donations</th>
    </tr>
    @if(isset($cd))
        <tr>
            <td width="50%">

                Did you make any cash contributions in 2019?

            </td>
            <td width="50%">
                @if(isset($prev_data['cd']))
                    @if($cd->make_cache !== $prev_data['cd']->make_cache)
                        {{$prev_data['cd']->make_cache?'Yes':'No'}}
                        <span class="red">{{$cd->make_cache?'Yes':'No'}}</span>
                    @else
                        {{$cd->make_cache?'Yes':'No'}}
                    @endif
                @else
                    {{$cd->make_cache?'Yes':'No'}}
                @endif

            </td>
        </tr>
        @if($cd->make_cache)

            @php
                $var_name='cd';
                $prop_child = $cd->expenses;
                $totalCharibleDonations = 0;
                $totalCharibleDonationsPrev=0;
            @endphp
            @if(isset($prev_data[$var_name]))
                @php
                    $prev_prop_child =$prev_data[$var_name]->expenses;
                @endphp
                @if(count($prop_child)>count($prev_prop_child))
                    @php
                        $size = count($prop_child);
                    @endphp
                @else
                    @php
                        $size =count($prev_prop_child);
                    @endphp
                @endif
            @else
                @php
                    $size = count($prop_child);
                @endphp
            @endif
            @for($i=0;$i<$size;$i++)
                @php
                    $ex_value = isset($prop_child[$i])?$prop_child[$i]:null;
                    $prev_value = isset($prev_prop_child[$i])?$prev_prop_child[$i]:null;
                    $totalCharibleDonations += isset($ex_value)?$ex_value->frequency * $ex_value->amount:0;
                    $totalCharibleDonationsPrev += isset($prev_value)?$prev_value->frequency * $prev_value->amount:0;
                @endphp
                <tr>
                    <td width="50%">

                        Description

                    </td>
                    <td width="50%">
                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->description !== $prev_value->description )
                                {{$prev_value->description}}
                                <span
                                    class="red">
                                                {{$ex_value->description}}
                                            </span>
                            @else
                                {{$ex_value->description}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->description)?'':'removed'}}">
                                            {{isset($prev_value->description)?$prev_value->description:''}}
                            </span>
                            <span
                                class="{{(!isset($prev_value->description)&&!isset($prev_data[$var_name]))?'':'red'}}">
                                            {{isset($ex_value->description)?$ex_value->description:''}}
                            </span>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td width="50%">

                        Frequency

                    </td>
                    <td width="50%">
                        @if(isset($prev_value))
                            @php
                                $prev_frequency_label='';

                                  switch ($prev_value->frequency) {
                                    case '1':
                                        $prev_frequency_label = 'One time';
                                        break;
                                    case '52':
                                        $prev_frequency_label = 'Weekly recurring';
                                        break;
                                    case '12':
                                        $prev_frequency_label = 'Monthly recurring';
                                        break;
                                }
                            @endphp
                        @endif
                        @if(isset($ex_value))
                            @php
                                $frequency_label = '';
                                switch ($ex_value->frequency) {
                                    case '1':
                                        $frequency_label = 'One time';
                                        break;
                                    case '52':
                                        $frequency_label = 'Weekly recurring';
                                        break;
                                    case '12':
                                        $frequency_label = 'Monthly recurring';
                                        break;
                                }
                            @endphp
                        @endif
                        @if(isset($prev_value) && isset($ex_value) )
                            @if($ex_value->frequency !== $prev_value->frequency )
                                {{$prev_frequency_label}}
                                <span
                                    class="red">
                        {{$frequency_label}}
                    </span>
                            @else
                                {{$frequency_label}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->frequency)?'':'removed'}}">
                    {{isset($prev_value->frequency)?$prev_frequency_label:''}}
    </span>
                            <span class="{{(!isset($prev_value->frequency)&&!isset($prev_data[$var_name]))?'':'red'}}">
                    {{isset($ex_value->frequency)?$frequency_label:''}}
    </span>
                        @endif
                    </td>
                </tr>

                <tr>
                    <td width="50%">

                        Amount

                    </td>
                    <td width="50%">
                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->amount !== $prev_value->amount )
                                ${{$prev_value->amount}}
                                <span
                                    class="red">
                       ${{$ex_value->amount}}
                    </span>
                            @else
                                ${{$ex_value->amount}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->amount)?'':'removed'}}">
                   {{isset($prev_value->amount)?'$'.$prev_value->amount:''}}
    </span>
                            <span class="{{(!isset($prev_value->amount)&&!isset($prev_data[$var_name]))?'':'red'}}">
                    {{isset($ex_value->amount)?'$'.$ex_value->amount:''}}
    </span>
                        @endif
                    </td>
                </tr>
            @endfor

            <tr>
                <td width="50%">

                    Total Charitable Donations

                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))

                        @if($totalCharibleDonations !== $totalCharibleDonationsPrev)
                            {{$totalCharibleDonationsPrev}}
                            <span class="red"> ${{$totalCharibleDonations}}</span>
                        @else
                            ${{$totalCharibleDonations}}
                        @endif
                    @else
                        ${{$totalCharibleDonations}}
                    @endif

                </td>
            </tr>
        @endif
    @endif
    <tr>
        <th colspan="2">Property Donations</th>
    </tr>
    @if(isset($property_donations))
        <tr>
            <td width="50%">

                Did you make any donations of property in 2019?

            </td>
            <td width="50%">
                @if(isset($prev_data['property_donations']))
                    @if($property_donations->pd_checkbox !== $prev_data['property_donations']->pd_checkbox)
                        {{$prev_data['property_donations']->pd_checkbox?'Yes':'No'}}
                        <span class="red">{{$property_donations->pd_checkbox?'Yes':'No'}}</span>
                    @else
                        {{$property_donations->pd_checkbox?'Yes':'No'}}
                    @endif
                @else
                    {{$property_donations->pd_checkbox?'Yes':'No'}}
                @endif


            </td>
        </tr>

        <tr>
            <td colspan="2">
                @if($property_donations->pd_checkbox)
                    @if(isset($prev_data['property_donations']))
                        @if(count($property_donations->donations)>count($prev_data['property_donations']->donations))
                            @php
                                $size = count($property_donations->donations);
                            @endphp
                        @else
                            @php
                                $size =count($prev_data['property_donations']->donations);
                            @endphp
                        @endif
                    @else
                        @php
                            $size = count($property_donations->donations);
                        @endphp
                    @endif
                    @for($i=0;$i<$size;$i++)
                        @php
                            $ex_value = isset($property_donations->donations[$i])?$property_donations->donations[$i]:null;
                            $prev_value = isset($prev_data['property_donations']->donations[$i])?$prev_data['property_donations']->donations[$i]:null;
                        @endphp

                        <table id="child_table" style="width: 80%;margin-left: 10%;">
                            <tr>
                                <th>
                                    Description
                                </th>
                                <th>
                                    Value
                                </th>
                            </tr>
                            <tr>
                                <td width="50%">
                                    @if(isset($prev_value) && isset($ex_value))
                                        @if($ex_value->cost_description !== $prev_value->cost_description )
                                            {{$prev_value->cost_description}}
                                            <span
                                                class="red">
                        {{$ex_value->cost_description}}
                    </span>
                                        @else
                                            {{$ex_value->cost_description}}
                                        @endif
                                    @else
                                        <span class="{{isset($ex_value->cost_description)?'':'removed'}}">
                    {{isset($prev_value->cost_description)?$prev_value->cost_description:''}}
                </span>
                                        <span
                                            class="{{(!isset($prev_value->cost_description)&&!isset($prev_data[$var_name]))?'':'red'}}">
                    {{isset($ex_value->cost_description)?$ex_value->cost_description:''}}
                </span>
                                    @endif

                                </td>
                                <td width="50%">
                                    @if(isset($prev_value) && isset($ex_value))
                                        @if($ex_value->cost_value !== $prev_value->cost_value )
                                            ${{$prev_value->cost_value}}
                                            <span
                                                class="red">
                        ${{$ex_value->cost_value}}
                    </span>
                                        @else
                                            ${{$ex_value->cost_value}}
                                        @endif
                                    @else
                                        <span class="{{isset($ex_value->cost_value)?'':'removed'}}">
                    {{isset($prev_value->cost_value)?'$'.$prev_value->cost_value:''}}
                </span>
                                        <span
                                            class="{{(!isset($prev_value->cost_value)&&!isset($prev_data[$var_name]))?'':'red'}}">
                    {{isset($ex_value->cost_value)?'$'.$ex_value->cost_value:''}}
                </span>
                                    @endif
                                </td>
                            </tr>

                        </table>
                    @endfor

                @endif

            </td>
        </tr>
    @endif

    <tr>
        <th colspan="2">Volunteering Expenses</th>
    </tr>

    @if(isset($volonteering_exp))
        @php
            $var_name='volonteering_exp';
            $prop_child = $volonteering_exp->expenses;
        @endphp
        @if(isset($prev_data[$var_name]))
            @php
                $prev_prop_child =$prev_data[$var_name]->expenses;
            @endphp
            @if(count($prop_child)>count($prev_prop_child))
                @php
                    $size = count($prop_child);
                @endphp
            @else
                @php
                    $size =count($prev_prop_child);
                @endphp
            @endif
        @else
            @php
                $size = count($prop_child);
            @endphp
        @endif

        <tr>
            <td width="50%">

                Did you have any Volunteering Expenses in 2019?

            </td>
            <td width="50%">

                @if(isset($prev_data[$var_name]))
                    @if($volonteering_exp->ve_volunteering_checkbox !== $prev_data[$var_name]->ve_volunteering_checkbox)
                        {{$prev_data[$var_name]->ve_volunteering_checkbox?'Yes':'No'}}
                        <span class="red">{{$volonteering_exp->ve_volunteering_checkbox?'Yes':'No'}}</span>
                    @else
                        {{$volonteering_exp->ve_volunteering_checkbox?'Yes':'No'}}
                    @endif
                @else
                    {{$volonteering_exp->ve_volunteering_checkbox?'Yes':'No'}}
                @endif

            </td>
        </tr>

        <tr>
            <td colspan="2">
                @if($volonteering_exp->ve_volunteering_checkbox)
                    @for($i=0;$i<$size;$i++)
                        @php
                            $ex_value = isset($prop_child[$i])?$prop_child[$i]:null;
                            $prev_value = isset($prev_prop_child[$i])?$prev_prop_child[$i]:null;
                        @endphp
                        <table id="child_table" style="width: 80%;margin-left: 10%;">
                            <tr>
                                <th>
                                    Description
                                </th>
                                <th>
                                    Value
                                </th>
                            </tr>
                            <tr>

                                <td width="50%">
                                    @if(isset($prev_value) && isset($ex_value))
                                        @if($ex_value->cost_description !== $prev_value->cost_description )
                                            {{$prev_value->cost_description}}
                                            <span
                                                class="red">
                        {{$ex_value->cost_description}}
                    </span>
                                        @else
                                            {{$ex_value->cost_description}}
                                        @endif
                                    @else
                                        <span class="{{isset($ex_value->cost_description)?'':'removed'}}">
                    {{isset($prev_value->cost_description)?$prev_value->cost_description:''}}
                </span>
                                        <span
                                            class="{{(!isset($prev_value->cost_description)&&!isset($prev_data[$var_name]))?'':'red'}}">
                    {{isset($ex_value->cost_description)?$ex_value->cost_description:''}}
                </span>
                                    @endif

                                </td>
                                <td width="50%">

                                    @if(isset($prev_value) && isset($ex_value))
                                        @if($ex_value->cost_value !== $prev_value->cost_value )
                                            ${{$prev_value->cost_value}}
                                            <span
                                                class="red">
                        ${{$ex_value->cost_value}}
                    </span>
                                        @else
                                            ${{$ex_value->cost_value}}
                                        @endif
                                    @else
                                        <span class="{{isset($ex_value->cost_value)?'':'removed'}}">
                    {{isset($prev_value->cost_value)?'$'.$prev_value->cost_value:''}}
                </span>
                                        <span
                                            class="{{(!isset($prev_value->cost_value)&&!isset($prev_data[$var_name]))?'':'red'}}">
                    {{isset($ex_value->cost_value)?'$'.$ex_value->cost_value:''}}
                </span>
                                    @endif


                                </td>

                            </tr>

                        </table>

                    @endfor
                @endif

            </td>
        </tr>
    @endif
    <tr>
        <th colspan="2">Education and Tuition related expenses</th>
    </tr>
    @if(isset($tuition_loan_expenses))
        @php
            $var_name='tuition_loan_expenses';
            $prop_child = $tuition_loan_expenses->expenses;
        @endphp
        @if(isset($prev_data[$var_name]))
            @php
                $prev_prop_child =$prev_data[$var_name]->expenses;
            @endphp
            @if(count($prop_child)>count($prev_prop_child))
                @php
                    $size = count($prop_child);
                @endphp
            @else
                @php
                    $size =count($prev_prop_child);
                @endphp
            @endif
        @else
            @php
                $size = count($prop_child);
            @endphp
        @endif
        <tr>
            <td width="50%">

                In 2019 were you or anyone to be listed on your return
                enrolled in a college or university?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($tuition_loan_expenses->se_enrolled_in_college_checkbox !== $prev_data[$var_name]->se_enrolled_in_college_checkbox)
                        {{$prev_data[$var_name]->se_enrolled_in_college_checkbox?'Yes':'No'}}
                        <span class="red">{{$tuition_loan_expenses->se_enrolled_in_college_checkbox?'Yes':'No'}}</span>
                    @else
                        {{$tuition_loan_expenses->se_enrolled_in_college_checkbox?'Yes':'No'}}
                    @endif
                @else
                    {{$tuition_loan_expenses->se_enrolled_in_college_checkbox?'Yes':'No'}}
                @endif

            </td>
        </tr>
        @if($tuition_loan_expenses->se_enrolled_in_college_checkbox)
            @for($i=0;$i<$size;$i++)
                @php
                    $ex_value = isset($prop_child[$i])?$prop_child[$i]:null;
                    $prev_value = isset($prev_prop_child[$i])?$prev_prop_child[$i]:null;
                @endphp
                <tr>
                    <td width="50%">

                        Description

                    </td>
                    <td width="50%">
                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_description !== $prev_value->cost_description )
                                {{$prev_value->cost_description}}
                                <span
                                    class="red">
                        {{$ex_value->cost_description}}
                    </span>
                            @else
                                {{$ex_value->cost_description}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_description)?'':'removed'}}">
                    {{isset($prev_value->cost_description)?$prev_value->cost_description:''}}
                </span>
                            <span
                                class="{{(!isset($prev_value->cost_description)&&!isset($prev_data[$var_name]))?'':'red'}}">

                    {{isset($ex_value->cost_description)?$ex_value->cost_description:''}}
                </span>
                        @endif

                    </td>
                </tr>

                <tr>
                    <td width="50%">

                        Value

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_value !== $prev_value->cost_value )
                                ${{$prev_value->cost_value}}
                                <span
                                    class="red">
                        ${{$ex_value->cost_value}}
                    </span>
                            @else
                                ${{$ex_value->cost_value}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_value)?'':'removed'}}">
                    {{isset($prev_value->cost_value)?'$'.$prev_value->cost_value:''}}
                </span>
                            <span class="{{(!isset($prev_value->cost_value)&&!isset($prev_data[$var_name]))?'':'red'}}">

                    {{isset($ex_value->cost_value)?'$'.$ex_value->cost_value:''}}
                </span>
                        @endif

                    </td>
                </tr>
            @endfor
        @endif
    @endif
    @if(isset($education_tution))
        @php
            $var_name='education_tution';
            $prop_child = $education_tution->expenses;
        @endphp
        @if(isset($prev_data[$var_name]))
            @php
                $prev_prop_child =$prev_data[$var_name]->expenses;
            @endphp
            @if(count($prop_child)>count($prev_prop_child))
                @php
                    $size = count($prop_child);
                @endphp
            @else
                @php
                    $size =count($prev_prop_child);
                @endphp
            @endif
        @else
            @php
                $size = count($prop_child);
            @endphp
        @endif
        <tr>
            <td width="50%">

                Did you pay any student loan interest in 2019?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($education_tution->se_load_checkbox !== $prev_data[$var_name]->se_load_checkbox)
                        {{$prev_data[$var_name]->se_load_checkbox?'Yes':'No'}}
                        <span class="red">{{$education_tution->se_load_checkbox?'Yes':'No'}}</span>
                    @else
                        {{$education_tution->se_load_checkbox?'Yes':'No'}}
                    @endif
                @else
                    {{$education_tution->se_load_checkbox?'Yes':'No'}}
                @endif
            </td>
        </tr>
        @if($education_tution->se_load_checkbox)
            @for($i=0;$i<$size;$i++)
                @php
                    $ex_value = isset($prop_child[$i])?$prop_child[$i]:null;
                    $prev_value = isset($prev_prop_child[$i])?$prev_prop_child[$i]:null;
                @endphp
                <tr>
                    <td width="50%">

                        Description

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_description !== $prev_value->cost_description )
                                {{$prev_value->cost_description}}
                                <span
                                    class="red">
                        {{$ex_value->cost_description}}
                    </span>
                            @else
                                {{$ex_value->cost_description}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_description)?'':'removed'}}">
                    {{isset($prev_value->cost_description)?$prev_value->cost_description:''}}
                </span>
                            <span
                                class="{{(!isset($prev_value->cost_description)&&!isset($prev_data[$var_name]))?'':'red'}}">

                    {{isset($ex_value->cost_description)?$ex_value->cost_description:''}}
                </span>
                        @endif

                    </td>
                </tr>

                <tr>
                    <td width="50%">

                        Value

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_value !== $prev_value->cost_value )
                                ${{$prev_value->cost_value}}
                                <span
                                    class="red">
                        ${{$ex_value->cost_value}}
                    </span>
                            @else
                                ${{$ex_value->cost_value}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_value)?'':'removed'}}">
                    {{isset($prev_value->cost_value)?'$'.$prev_value->cost_value:''}}
                </span>
                            <span class="{{(!isset($prev_value->cost_value)&&!isset($prev_data[$var_name]))?'':'red'}}">

                    {{isset($ex_value->cost_value)?'$'.$ex_value->cost_value:''}}
                </span>
                        @endif


                    </td>
                </tr>
            @endfor
        @endif
    @endif
    @if(isset($add_options_exp))
        @php
            $var_name='add_options_exp';
            $prop_child = $add_options_exp->expenses;
        @endphp
        @if(isset($prev_data[$var_name]))
            @php
                $prev_prop_child =$prev_data[$var_name]->expenses;
            @endphp
            @if(count($prop_child)>count($prev_prop_child))
                @php
                    $size = count($prop_child);
                @endphp
            @else
                @php
                    $size =count($prev_prop_child);
                @endphp
            @endif
        @else
            @php
                $size = count($prop_child);
            @endphp
        @endif
        <tr>
            <th colspan="2">Childcare & Adoption Expenses</th>
        </tr>

        <tr>
            <td width="50%">

                Did you have any expenses associated with day care
                nannies or babysitters in 2019: these are generally only
                deductible if it was necessary to pursue employment.

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($add_options_exp->fm_add_description_checkbox !== $prev_data[$var_name]->fm_add_description_checkbox)
                        {{$prev_data[$var_name]->fm_add_description_checkbox?'Yes':'No'}}
                        <span class="red">{{$add_options_exp->fm_add_description_checkbox?'Yes':'No'}}</span>
                    @else
                        {{$add_options_exp->fm_add_description_checkbox?'Yes':'No'}}
                    @endif
                @else
                    {{$add_options_exp->fm_add_description_checkbox?'Yes':'No'}}
                @endif

            </td>
        </tr>

        @if($add_options_exp->fm_add_description_checkbox)
            @for($i=0;$i<$size;$i++)
                @php
                    $ex_value = isset($prop_child[$i])?$prop_child[$i]:null;
                    $prev_value = isset($prev_prop_child[$i])?$prev_prop_child[$i]:null;
                @endphp
                <tr>
                    <td width="50%">

                        Description

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_description !== $prev_value->cost_description )
                                {{$prev_value->cost_description}}
                                <span
                                    class="red">
                        {{$ex_value->cost_description}}
                    </span>
                            @else
                                {{$ex_value->cost_description}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_description)?'':'removed'}}">
                    {{isset($prev_value->cost_description)?$prev_value->cost_description:''}}
                </span>
                            <span
                                class="{{(!isset($prev_value->cost_description)&&!isset($prev_data[$var_name]))?'':'red'}}">

                    {{isset($ex_value->cost_description)?$ex_value->cost_description:''}}
                </span>
                        @endif

                    </td>
                </tr>

                <tr>
                    <td width="50%">

                        Value

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_value !== $prev_value->cost_value )
                                ${{$prev_value->cost_value}}
                                <span
                                    class="red">
                        ${{$ex_value->cost_value}}
                    </span>
                            @else
                                ${{$ex_value->cost_value}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_value)?'':'removed'}}">
                    {{isset($prev_value->cost_value)?'$'.$prev_value->cost_value:''}}
                </span>
                            <span class="{{(!isset($prev_value->cost_value)&&!isset($prev_data[$var_name]))?'':'red'}}">

                    {{isset($ex_value->cost_value)?'$'.$ex_value->cost_value:''}}
                </span>
                        @endif


                    </td>
                </tr>
            @endfor
        @endif

        <tr>
            <td width="50%">

                The next major topic has to do with your family. Did
                you have any new dependents in the household in 2019?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($add_options_exp->fm_add_adoptions_checkbox !== $prev_data[$var_name]->fm_add_adoptions_checkbox)
                        {{$prev_data[$var_name]->fm_add_adoptions_checkbox?'Yes':'No'}}
                        <span class="red">{{$add_options_exp->fm_add_adoptions_checkbox?'Yes':'No'}}</span>
                    @else
                        {{$add_options_exp->fm_add_adoptions_checkbox?'Yes':'No'}}
                    @endif
                @else
                    {{$add_options_exp->fm_add_adoptions_checkbox?'Yes':'No'}}
                @endif
            </td>
        </tr>

        @if($add_options_exp->fm_add_adoptions_checkbox)
            @for($i=0;$i<$size;$i++)
                @php
                    $ex_value = isset($add_options_exp->child[$i])?$add_options_exp->child[$i]:null;
                    $prev_value = isset($prev_data[$var_name]->child[$i])?$prev_data[$var_name]->child[$i]:null;
                @endphp
                <tr>
                    <td width="50%">

                        Description

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_description !== $prev_value->cost_description )
                                {{$prev_value->cost_description}}
                                <span
                                    class="red">
                        {{$ex_value->cost_description}}
                    </span>
                            @else
                                {{$ex_value->cost_description}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_description)?'':'removed'}}">
                    {{isset($prev_value->cost_description)?$prev_value->cost_description:''}}
                </span>
                            <span
                                class="{{(!isset($prev_value->cost_description)&&!isset($prev_data[$var_name]))?'':'red'}}">
                    {{isset($ex_value->cost_description)?$ex_value->cost_description:''}}
                </span>
                        @endif

                    </td>
                </tr>

                <tr>
                    <td width="50%">

                        Value

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_value !== $prev_value->cost_value )
                                ${{$prev_value->cost_value}}
                                <span
                                    class="red">
                        ${{$ex_value->cost_value}}
                    </span>
                            @else
                                ${{$ex_value->cost_value}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_value)?'':'removed'}}">
                    {{isset($prev_value->cost_value)?'$'.$prev_value->cost_value:''}}
                </span>
                            <span class="{{(!isset($prev_value->cost_value)&&!isset($prev_data[$var_name]))?'':'red'}}">

                    {{isset($ex_value->cost_value)?'$'.$ex_value->cost_value:''}}
                </span>
                        @endif


                    </td>
                </tr>
            @endfor
        @endif
    @endif
    @if(isset($home_expense))
        @php
            $var_name='home_expense';
            $prop_child = $home_expense->expenses;
        @endphp
        <tr>
            <th colspan="2">Home Expenses</th>
        </tr>
        <tr>
            <td width="50%">

                Do you Own or Rent?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($home_expense->hm_own_rent !== $prev_data[$var_name]->hm_own_rent)
                        {{$prev_data[$var_name]->hm_own_rent?'Yes':'No'}}
                        <span class="red">{{$home_expense->hm_own_rent?'Yes':'No'}}</span>
                    @else
                        {{$home_expense->hm_own_rent?'Yes':'No'}}
                    @endif
                @else
                    {{$home_expense->hm_own_rent?'Yes':'No'}}
                @endif

            </td>
        </tr>

        <tr>
            <td width="50%">

                Did you have any mortgage interest in 2019?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($home_expense->hm_mortage_interest !== $prev_data[$var_name]->hm_mortage_interest)
                        {{$prev_data[$var_name]->hm_mortage_interest?'Yes':'No'}}
                        <span class="red">{{$home_expense->hm_mortage_interest?'Yes':'No'}}</span>
                    @else
                        {{$home_expense->hm_mortage_interest?'Yes':'No'}}
                    @endif
                @else
                    {{$home_expense->hm_mortage_interest?'Yes':'No'}}
                @endif


            </td>
        </tr>

        <tr>
            <td width="50%">

                Did you pay any real estate taxes in 2019?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($home_expense->hm_real_estate_tax_checkbox !== $prev_data[$var_name]->hm_real_estate_tax_checkbox)
                        {{$prev_data[$var_name]->hm_real_estate_tax_checkbox?'Yes':'No'}}
                        <span class="red">{{$home_expense->hm_real_estate_tax_checkbox?'Yes':'No'}}</span>
                    @else
                        {{$home_expense->hm_real_estate_tax_checkbox?'Yes':'No'}}
                    @endif
                @else
                    {{$home_expense->hm_real_estate_tax_checkbox?'Yes':'No'}}
                @endif


            </td>
        </tr>
        @if($home_expense->hm_real_estate_tax_checkbox)
            <tr>
                <td width="50%">

                    Real Estate Taxes

                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))
                        @if($home_expense->hm_real_estate_tax !== $prev_data[$var_name]->hm_real_estate_tax)
                            ${{$prev_data[$var_name]->hm_real_estate_tax}}
                            <span class="red">${{$home_expense->hm_real_estate_tax}}</span>
                        @else
                            ${{$home_expense->hm_real_estate_tax}}
                        @endif
                    @else
                        ${{$home_expense->hm_real_estate_tax}}
                    @endif
                </td>
            </tr>

            <tr>
                <td width="50%">

                    Mortgage Insurance

                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))
                        @if($home_expense->hm_mortgage_insurance !== $prev_data[$var_name]->hm_mortgage_insurance)
                            ${{$prev_data[$var_name]->hm_mortgage_insurance}}
                            <span class="red">${{$home_expense->hm_mortgage_insurance}}</span>
                        @else
                            ${{$home_expense->hm_mortgage_insurance}}
                        @endif
                    @else
                        ${{$home_expense->hm_mortgage_insurance}}
                    @endif

                </td>
            </tr>

            <tr>
                <td width="50%">

                    Loan-Mandated Homeowner's Ins.

                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))
                        @if($home_expense->hm_loan_mandated_homeowner_ins !== $prev_data[$var_name]->hm_loan_mandated_homeowner_ins)
                            ${{$prev_data[$var_name]->hm_loan_mandated_homeowner_ins}}
                            <span class="red">${{$home_expense->hm_loan_mandated_homeowner_ins}}</span>
                        @else
                            ${{$home_expense->hm_loan_mandated_homeowner_ins}}
                        @endif
                    @else
                        ${{$home_expense->hm_loan_mandated_homeowner_ins}}
                    @endif

                </td>
            </tr>
        @endif
        <tr>
            <td width="50%">

                Did You Have Any Energy Efficient Upgrades?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($home_expense->hm_energy_efficient_upgrades_radio !== $prev_data[$var_name]->hm_energy_efficient_upgrades_radio)
                        {{$prev_data[$var_name]->hm_energy_efficient_upgrades_radio?'Yes':'No'}}
                        <span class="red">{{$home_expense->hm_energy_efficient_upgrades_radio?'Yes':'No'}}</span>
                    @else
                        {{$home_expense->hm_energy_efficient_upgrades_radio?'Yes':'No'}}
                    @endif
                @else
                    {{$home_expense->hm_energy_efficient_upgrades_radio?'Yes':'No'}}
                @endif

            </td>
        </tr>
        @if($home_expense->hm_energy_efficient_upgrades_radio)
            @if(isset($prev_data[$var_name]))
                @php
                    $prev_prop_child =$prev_data[$var_name]->expenses;
  $totalEfficientPrev= 0;
                @endphp
                @if(count($prop_child)>count($prev_prop_child))
                    @php
                        $size = count($prop_child);
                    @endphp
                @else
                    @php
                        $size =count($prev_prop_child);
                    @endphp
                @endif
            @else
                @php
                    $size = count($prop_child);

                @endphp
            @endif
            <?php $totalEfficient = 0;
            ?>
            @for($i=0;$i<$size;$i++)
                @php
                    $ex_value = isset($prop_child[$i])?$prop_child[$i]:null;
                    $prev_value = isset($prev_prop_child[$i])?$prev_prop_child[$i]:null;
                    $totalEfficient += isset($ex_value)?$ex_value->cost_value:0;
                    $totalEfficientPrev += isset($prev_value)?$prev_value->cost_value:0;
                @endphp
                <tr>
                    <td width="50%">

                        Description

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_description !== $prev_value->cost_description )
                                {{$prev_value->cost_description}}
                                <span
                                    class="red">
                        {{$ex_value->cost_description}}
                    </span>
                            @else
                                {{$ex_value->cost_description}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_description)?'':'removed'}}">
                    {{isset($prev_value->cost_description)?$prev_value->cost_description:''}}
                </span>
                            <span
                                class="{{(!isset($prev_value->cost_description)&&!isset($prev_data[$var_name]))?'':'red'}}">
                    {{isset($ex_value->cost_description)?$ex_value->cost_description:''}}
                </span>
                        @endif

                    </td>
                </tr>

                <tr>
                    <td width="50%">

                        Value

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_value !== $prev_value->cost_value )
                                ${{$prev_value->cost_value}}
                                <span
                                    class="red">
                        ${{$ex_value->cost_value}}
                    </span>
                            @else
                                ${{$ex_value->cost_value}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_value)?'':'removed'}}">
                    {{isset($prev_value->cost_value)?'$'.$prev_value->cost_value:''}}
                </span>
                            <span class="{{(!isset($prev_value->cost_value)&&!isset($prev_data[$var_name]))?'':'red'}}">
                    {{isset($ex_value->cost_value)?'$'.$ex_value->cost_value:''}}
                </span>
                        @endif


                    </td>
                </tr>
            @endfor
            <tr>
                <td width="50%">

                    Total Energy Efficient upgrades

                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))
                        @if($totalEfficient !== $totalEfficientPrev)
                            ${{$totalEfficientPrev}}
                            <span class="red"> ${{$totalEfficient}}</span>
                        @else
                            ${{$totalEfficient}}
                        @endif
                    @else
                        ${{$totalEfficient}}
                    @endif

                </td>
            </tr>
        @endif

    @endif
    @if(isset($small_items))
        @php
            $var_name='small_items';
        @endphp
        <tr>
            <th colspan="2">Small But Important Items</th>
        </tr>
        <tr>
            <td width="50%">

                Did you make any contribution to a Traditional or Roth
                IRA in 2019?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($small_items->sii_traditional_IRA_contributions_radio !== $prev_data[$var_name]->sii_traditional_IRA_contributions_radio)
                        {{$prev_data[$var_name]->sii_traditional_IRA_contributions_radio?'Yes':'No'}}
                        <span class="red">{{$small_items->sii_traditional_IRA_contributions_radio?'Yes':'No'}}</span>
                    @else
                        {{$small_items->sii_traditional_IRA_contributions_radio?'Yes':'No'}}
                    @endif
                @else
                    {{$small_items->sii_traditional_IRA_contributions_radio?'Yes':'No'}}
                @endif

            </td>
        </tr>

        @if($small_items->sii_traditional_IRA_contributions_radio)
            <tr>
                <td width="50%">

                    Traditional IRA Contributions

                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))
                        @if($small_items->sii_traditional_IRA_contributions !== $prev_data[$var_name]->sii_traditional_IRA_contributions)
                            $ {{$prev_data[$var_name]->sii_traditional_IRA_contributions}}
                            <span class="red">${{$small_items->sii_traditional_IRA_contributions}}</span>
                        @else
                            ${{$small_items->sii_traditional_IRA_contributions}}
                        @endif
                    @else
                        ${{$small_items->sii_traditional_IRA_contributions}}
                    @endif
                </td>
            </tr>

            <tr>
                <td width="50%">

                    Roth IRA Contributions

                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))
                        @if($small_items->sii_roth_IRA_contributions !== $prev_data[$var_name]->sii_roth_IRA_contributions)
                            $ {{$prev_data[$var_name]->sii_roth_IRA_contributions}}
                            <span class="red">${{$small_items->sii_roth_IRA_contributions}}</span>
                        @else
                            ${{$small_items->sii_roth_IRA_contributions}}
                        @endif
                    @else
                        ${{$small_items->sii_roth_IRA_contributions}}
                    @endif

                </td>
            </tr>
        @endif
        <tr>
            <td width="50%">

                ARE YOU A TEACHER?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($small_items->sii_traditional_IRA_contributions_radio !== $prev_data[$var_name]->sii_traditional_IRA_contributions_radio)
                        {{$prev_data[$var_name]->sii_traditional_IRA_contributions_radio?'Yes':'No'}}
                        <span class="red">{{$small_items->sii_traditional_IRA_contributions_radio?'Yes':'No'}}</span>
                    @else
                        {{$small_items->sii_traditional_IRA_contributions_radio?'Yes':'No'}}
                    @endif
                @else
                    {{$small_items->sii_traditional_IRA_contributions_radio?'Yes':'No'}}
                @endif


            </td>
        </tr>

        <tr>
            <td width="50%">

                Did you have any educator expenses as a teacher or
                professor in 2019?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($small_items->sii_educator_expenses_radio !== $prev_data[$var_name]->sii_educator_expenses_radio)
                        {{$prev_data[$var_name]->sii_educator_expenses_radio?'Yes':'No'}}
                        <span class="red">{{$small_items->sii_educator_expenses_radio?'Yes':'No'}}</span>
                    @else
                        {{$small_items->sii_educator_expenses_radio?'Yes':'No'}}
                    @endif
                @else
                    {{$small_items->sii_educator_expenses_radio?'Yes':'No'}}
                @endif


            </td>
        </tr>

        <tr>
            <td width="50%">

                Educator Expenses

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($small_items->sii_educator_expenses !== $prev_data[$var_name]->sii_educator_expenses)
                        $ {{$prev_data[$var_name]->sii_educator_expenses}}
                        <span class="red">${{$small_items->sii_educator_expenses}}</span>
                    @else
                        ${{$small_items->sii_educator_expenses}}
                    @endif
                @else
                    ${{$small_items->sii_educator_expenses}}
                @endif
            </td>
        </tr>

        <tr>
            <td width="50%">

                Are you self employed with a home-office?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($small_items->sii_self_employed_radio !== $prev_data[$var_name]->sii_self_employed_radio)
                        {{$prev_data[$var_name]->sii_self_employed_radio?'Yes':'No'}}
                        <span class="red">{{$small_items->sii_self_employed_radio?'Yes':'No'}}</span>
                    @else
                        {{$small_items->sii_self_employed_radio?'Yes':'No'}}
                    @endif
                @else
                    {{$small_items->sii_self_employed_radio?'Yes':'No'}}
                @endif
            </td>
        </tr>

        <tr>
            <td width="50%">

                Total Mortgage / Rent Payments for the Year

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($small_items->sii_total_mortgage_payments !== $prev_data[$var_name]->sii_total_mortgage_payments)
                        $ {{$prev_data[$var_name]->sii_total_mortgage_payments}}
                        <span class="red">${{$small_items->sii_total_mortgage_payments}}</span>
                    @else
                        ${{$small_items->sii_total_mortgage_payments}}
                    @endif
                @else
                    ${{$small_items->sii_total_mortgage_payments}}
                @endif
            </td>
        </tr>

        <tr>
            <td width="50%">

                Total HOA

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($small_items->sii_total_HOA !== $prev_data[$var_name]->sii_total_HOA)
                        $ {{$prev_data[$var_name]->sii_total_HOA}}
                        <span class="red">${{$small_items->sii_total_HOA}}</span>
                    @else
                        ${{$small_items->sii_total_HOA}}
                    @endif
                @else
                    ${{$small_items->sii_total_HOA}}
                @endif
            </td>
        </tr>

        <tr>
            <td width="50%">

                Total Utilities

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($small_items->sii_total_utilities !== $prev_data[$var_name]->sii_total_utilities)
                        $ {{$prev_data[$var_name]->sii_total_utilities}}
                        <span class="red">${{$small_items->sii_total_utilities}}</span>
                    @else
                        ${{$small_items->sii_total_utilities}}
                    @endif
                @else
                    ${{$small_items->sii_total_utilities}}
                @endif
            </td>
        </tr>
    @endif
    @if(isset($business_expense))
        @php
            $var_name='business_expense';
        @endphp
        <tr>
            <th colspan="2">Employee Business Expenses</th>
        </tr>
        <tr>
            <td width="50%">

                Did you have any work-related parking fees or parking
                garage costs in 2019?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($business_expense->ebe_total_parking_fees_radio !== $prev_data[$var_name]->ebe_total_parking_fees_radio)
                        {{$prev_data[$var_name]->ebe_total_parking_fees_radio?'Yes':'No'}}
                        <span class="red">{{$business_expense->ebe_total_parking_fees_radio?'Yes':'No'}}</span>
                    @else
                        {{$business_expense->ebe_total_parking_fees_radio?'Yes':'No'}}
                    @endif
                @else
                    {{$business_expense->ebe_total_parking_fees_radio?'Yes':'No'}}
                @endif

            </td>
        </tr>
        @if($business_expense->ebe_total_parking_fees_radio)
            <tr>
                <td width="50%">

                    Total Parking Fees

                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))
                        @if($business_expense->ebe_total_parking_fees !== $prev_data[$var_name]->ebe_total_parking_fees)
                            $ {{$prev_data[$var_name]->ebe_total_parking_fees}}
                            <span class="red">${{$business_expense->ebe_total_parking_fees}}</span>
                        @else
                            ${{$business_expense->ebe_total_parking_fees}}
                        @endif
                    @else
                        ${{$business_expense->ebe_total_parking_fees}}
                    @endif

                </td>
            </tr>
        @endif

        <tr>
            <td width="50%">

                Did you have any work-related Tollway charges or Toll
                Tag costs in 2019?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($business_expense->ebe_tollway_costs_radio !== $prev_data[$var_name]->ebe_tollway_costs_radio)
                        {{$prev_data[$var_name]->ebe_tollway_costs_radio?'Yes':'No'}}
                        <span class="red">{{$business_expense->ebe_tollway_costs_radio?'Yes':'No'}}</span>
                    @else
                        {{$business_expense->ebe_tollway_costs_radio?'Yes':'No'}}
                    @endif
                @else
                    {{$business_expense->ebe_tollway_costs_radio?'Yes':'No'}}
                @endif


            </td>
        </tr>
        @if($business_expense->ebe_tollway_costs_radio)
            <tr>
                <td width="50%">

                    Tollway or Toll Tag Costs

                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))
                        @if($business_expense->ebe_tollway_costs !== $prev_data[$var_name]->ebe_tollway_costs)
                            $ {{$prev_data[$var_name]->ebe_tollway_costs}}
                            <span class="red">${{$business_expense->ebe_tollway_costs}}</span>
                        @else
                            ${{$business_expense->ebe_tollway_costs}}
                        @endif
                    @else
                        ${{$business_expense->ebe_tollway_costs}}
                    @endif

                </td>
            </tr>
        @endif
        <tr>
            <td width="50%">

                Did you have any local transportation expenses, such as
                Uber, Lyft, Metro Pass, Taxis, Subways in 2019?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($business_expense->ebe_total_local_transportation_radio !== $prev_data[$var_name]->ebe_total_local_transportation_radio)
                        {{$prev_data[$var_name]->ebe_total_local_transportation_radio?'Yes':'No'}}
                        <span class="red">{{$business_expense->ebe_total_local_transportation_radio?'Yes':'No'}}</span>
                    @else
                        {{$business_expense->ebe_total_local_transportation_radio?'Yes':'No'}}
                    @endif
                @else
                    {{$business_expense->ebe_total_local_transportation_radio?'Yes':'No'}}
                @endif


            </td>
        </tr>
        @if($business_expense->ebe_total_local_transportation_radio)
            @if(isset($prev_data[$var_name]))
                @php
                    $prop_child = $business_expense->transportation;
                    $prev_prop_child =$prev_data[$var_name]->travelCost;
                @endphp
                @if(count($prop_child)>count($prev_prop_child))
                    @php
                        $size = count($prop_child);
                    @endphp
                @else
                    @php
                        $size =count($prev_prop_child);
                    @endphp
                @endif
            @else
                @php
                    $size = count($prop_child);
                @endphp
            @endif
            @for($i=0;$i<$size;$i++)
                @php
                    $ex_value = isset($prop_child[$i])?$prop_child[$i]:null;
                    $prev_value = isset($prev_prop_child[$i])?$prev_prop_child[$i]:null;
                @endphp
                <tr>
                    <td width="50%">

                        Description

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_description !== $prev_value->cost_description )
                                {{$prev_value->cost_description}}
                                <span
                                    class="red">
                        {{$ex_value->cost_description}}
                    </span>
                            @else
                                {{$ex_value->cost_description}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_description)?'':'removed'}}">
                    {{isset($prev_value->cost_description)?$prev_value->cost_description:''}}
                </span>
                            <span
                                class="{{(!isset($prev_value->cost_description)&&!isset($prev_data[$var_name]))?'':'red'}}">
                    {{isset($ex_value->cost_description)?$ex_value->cost_description:''}}
                </span>
                        @endif

                    </td>
                </tr>

                <tr>
                    <td width="50%">

                        Value

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_value !== $prev_value->cost_value )
                                ${{$prev_value->cost_value}}
                                <span
                                    class="red">
                        ${{$ex_value->cost_value}}
                    </span>
                            @else
                                ${{$ex_value->cost_value}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_value)?'':'removed'}}">
                    {{isset($prev_value->cost_value)?'$'.$prev_value->cost_value:''}}
                </span>
                            <span class="{{(!isset($prev_value->cost_value)&&!isset($prev_data[$var_name]))?'':'red'}}">
                    {{isset($ex_value->cost_value)?'$'.$ex_value->cost_value:''}}
                </span>
                        @endif


                    </td>
                </tr>
            @endfor
        @endif
        <tr>
            <td width="50%">

                This may have been related to a conference that you
                attended or an out-of-town business meeting or temporary job site.

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($business_expense->ebe_total_travel_costs_radio !== $prev_data[$var_name]->ebe_total_travel_costs_radio)
                        {{$prev_data[$var_name]->ebe_total_travel_costs_radio?'Yes':'No'}}
                        <span class="red">{{$business_expense->ebe_total_travel_costs_radio?'Yes':'No'}}</span>
                    @else
                        {{$business_expense->ebe_total_travel_costs_radio?'Yes':'No'}}
                    @endif
                @else
                    {{$business_expense->ebe_total_travel_costs_radio?'Yes':'No'}}
                @endif
            </td>
        </tr>
        @if($business_expense->ebe_total_travel_costs_radio)
            @if(isset($prev_data[$var_name]))
                @php
                    $prop_child = $business_expense->travelCost;
                    $prev_prop_child =$prev_data[$var_name]->travelCost;
                @endphp
                @if(count($prop_child)>count($prev_prop_child))
                    @php
                        $size = count($prop_child);
                    @endphp
                @else
                    @php
                        $size =count($prev_prop_child);
                    @endphp
                @endif
            @else
                @php
                    $size = count($prop_child);
                @endphp
            @endif
            @for($i=0;$i<$size;$i++)
                @php
                    $ex_value = isset($prop_child[$i])?$prop_child[$i]:null;
                    $prev_value = isset($prev_prop_child[$i])?$prev_prop_child[$i]:null;
                @endphp
                <tr>
                    <td width="50%">

                        Description

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_description !== $prev_value->cost_description )
                                {{$prev_value->cost_description}}
                                <span
                                    class="red">
                        {{$ex_value->cost_description}}
                    </span>
                            @else
                                {{$ex_value->cost_description}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_description)?'':'removed'}}">
                    {{isset($prev_value->cost_description)?$prev_value->cost_description:''}}
                </span>
                            <span
                                class="{{(!isset($prev_value->cost_description)&&!isset($prev_data[$var_name]))?'':'red'}}">
                    {{isset($ex_value->cost_description)?$ex_value->cost_description:''}}
                </span>
                        @endif

                    </td>
                </tr>

                <tr>
                    <td width="50%">

                        Value

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_value !== $prev_value->cost_value )
                                ${{$prev_value->cost_value}}
                                <span
                                    class="red">
                        ${{$ex_value->cost_value}}
                    </span>
                            @else
                                ${{$ex_value->cost_value}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_value)?'':'removed'}}">
                    {{isset($prev_value->cost_value)?'$'.$prev_value->cost_value:''}}
                </span>
                            <span class="{{(!isset($prev_value->cost_value)&&!isset($prev_data[$var_name]))?'':'red'}}">
                    {{isset($ex_value->cost_value)?'$'.$ex_value->cost_value:''}}
                </span>
                        @endif


                    </td>
                </tr>
            @endfor
        @endif
    @endif
    @if(isset($vehicle_expense))
        @php
            $var_name='vehicle_expense';
        @endphp
        <tr>
            <th colspan="2">Vehicle Expenses</th>
        </tr>
        <tr>
            <td width="50%">

                Make

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($vehicle_expense->ve_make !== $prev_data[$var_name]->ve_make)
                        {{$prev_data[$var_name]->ve_make}}
                        <span class="red">{{$vehicle_expense->ve_make}}</span>
                    @else
                        {{$vehicle_expense->ve_make}}
                    @endif
                @else
                    {{$vehicle_expense->ve_make}}
                @endif


            </td>
        </tr>

        <tr>
            <td width="50%">

                Model

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($vehicle_expense->ve_model !== $prev_data[$var_name]->ve_model)
                        {{$prev_data[$var_name]->ve_model}}
                        <span class="red">{{$vehicle_expense->ve_model}}</span>
                    @else
                        {{$vehicle_expense->ve_model}}
                    @endif
                @else
                    {{$vehicle_expense->ve_model}}
                @endif

            </td>
        </tr>

        <tr>
            <td width="50%">

                Price or Value-at-Acquisition

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($vehicle_expense->ve_price !== $prev_data[$var_name]->ve_price)
                        ${{$prev_data[$var_name]->ve_price}}
                        <span class="red"> ${{$vehicle_expense->ve_price}}</span>
                    @else
                        ${{$vehicle_expense->ve_price}}
                    @endif
                @else
                    ${{$vehicle_expense->ve_price}}
                @endif


            </td>
        </tr>
        <tr>
            <td width="50%">

                Do you own or lease?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($vehicle_expense->own_lease !== $prev_data[$var_name]->own_lease)
                        {{$prev_data[$var_name]->own_lease?'Own':'Lease'}}
                        <span class="red">{{$vehicle_expense->own_lease?'Own':'Lease'}}</span>
                    @else
                        {{$vehicle_expense->own_lease?'Own':'Lease'}}
                    @endif
                @else
                    {{$vehicle_expense->own_lease?'Own':'Lease'}}
                @endif

            </td>
        </tr>
        @if($personal_info->marital_status=='1')
            <tr>
                <td width="50%">

                    Make

                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))
                        @if($vehicle_expense->ve_spouse_make !== $prev_data[$var_name]->ve_spouse_make)
                            {{$prev_data[$var_name]->ve_spouse_make}}
                            <span class="red">{{$vehicle_expense->ve_spouse_make}}</span>
                        @else
                            {{$vehicle_expense->ve_spouse_make}}
                        @endif
                    @else
                        {{$vehicle_expense->ve_spouse_make}}
                    @endif

                </td>
            </tr>

            <tr>
                <td width="50%">

                    Model

                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))
                        @if($vehicle_expense->ve_spouse_model !== $prev_data[$var_name]->ve_spouse_model)
                            {{$prev_data[$var_name]->ve_spouse_model}}
                            <span class="red">{{$vehicle_expense->ve_spouse_model}}</span>
                        @else
                            {{$vehicle_expense->ve_spouse_model}}
                        @endif
                    @else
                        {{$vehicle_expense->ve_spouse_model}}
                    @endif
                </td>
            </tr>

            <tr>
                <td width="50%">

                    Price or Value-at-Acquisition

                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))
                        @if($vehicle_expense->ve_spouse_price !== $prev_data[$var_name]->ve_spouse_price)
                            ${{$prev_data[$var_name]->ve_spouse_price}}
                            <span class="red"> ${{$vehicle_expense->ve_spouse_price}}</span>
                        @else
                            ${{$vehicle_expense->ve_spouse_price}}
                        @endif
                    @else
                        ${{$vehicle_expense->ve_spouse_price}}
                    @endif

                </td>
            </tr>
            <tr>
                <td width="50%">

                    Does your spouse own or lease?

                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))
                        @if($vehicle_expense->spouse_own_lease !== $prev_data[$var_name]->spouse_own_lease)
                            {{$prev_data[$var_name]->spouse_own_lease?'Own':'Lease'}}
                            <span class="red">{{$vehicle_expense->spouse_own_lease?'Own':'Lease'}}</span>
                        @else
                            {{$vehicle_expense->spouse_own_lease?'Own':'Lease'}}
                        @endif
                    @else
                        {{$vehicle_expense->spouse_own_lease?'Own':'Lease'}}
                    @endif

                </td>
            </tr>
        @endif
    @endif
    @if(isset($vehicle_maintenance))
        @php
            $var_name='vehicle_maintenance';
        @endphp
        <tr>
            <td width="50%">

                Weekly Estimated Total

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($vehicle_maintenance->eve_weekly_estimated_total !== $prev_data[$var_name]->eve_weekly_estimated_total)
                        ${{$prev_data[$var_name]->eve_weekly_estimated_total}}
                        <span class="red"> ${{$vehicle_maintenance->eve_weekly_estimated_total}}</span>
                    @else
                        ${{$vehicle_maintenance->eve_weekly_estimated_total}}
                    @endif
                @else
                    ${{$vehicle_maintenance->eve_weekly_estimated_total}}
                @endif

            </td>
        </tr>
        @if($personal_info->marital_status=='1')
            <tr>
                <td width="50%">

                    Spouse's Weekly Estimated Total

                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))
                        @if($vehicle_maintenance->eve_spouse_weekly_estimated_total !== $prev_data[$var_name]->eve_spouse_weekly_estimated_total)
                            ${{$prev_data[$var_name]->eve_spouse_weekly_estimated_total}}
                            <span class="red"> ${{$vehicle_maintenance->eve_spouse_weekly_estimated_total}}</span>
                        @else
                            ${{$vehicle_maintenance->eve_spouse_weekly_estimated_total}}
                        @endif
                    @else
                        ${{$vehicle_maintenance->eve_spouse_weekly_estimated_total}}
                    @endif


                </td>
            </tr>
        @endif

        <tr>
            <td width="50%">

                Primary Number Of Oil Changes

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($vehicle_maintenance->eve_no_Oil_Changes !== $prev_data[$var_name]->eve_no_Oil_Changes)
                        {{$prev_data[$var_name]->eve_no_Oil_Changes}}
                        <span class="red">{{$vehicle_maintenance->eve_no_Oil_Changes}}</span>
                    @else
                        {{$vehicle_maintenance->eve_no_Oil_Changes}}
                    @endif
                @else
                    {{$vehicle_maintenance->eve_no_Oil_Changes}}
                @endif


            </td>
        </tr>

        <tr>
            <td width="50%">

                Primary Cost Per Change

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($vehicle_maintenance->eve_Cost_Per_Change !== $prev_data[$var_name]->eve_Cost_Per_Change)
                        ${{$prev_data[$var_name]->eve_Cost_Per_Change}}
                        <span class="red"> ${{$vehicle_maintenance->eve_Cost_Per_Change}}</span>
                    @else
                        ${{$vehicle_maintenance->eve_Cost_Per_Change}}
                    @endif
                @else
                    ${{$vehicle_maintenance->eve_Cost_Per_Change}}
                @endif


            </td>
        </tr>
        @if($personal_info->marital_status=='1')
            <tr>
                <td width="50%">

                    Spouse Number Of Oil Changes

                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))
                        @if($vehicle_maintenance->eve_spouse_no_Oil_Changes !== $prev_data[$var_name]->eve_spouse_no_Oil_Changes)
                            {{$prev_data[$var_name]->eve_spouse_no_Oil_Changes}}
                            <span class="red">{{$vehicle_maintenance->eve_spouse_no_Oil_Changes}}</span>
                        @else
                            {{$vehicle_maintenance->eve_spouse_no_Oil_Changes}}
                        @endif
                    @else
                        {{$vehicle_maintenance->eve_spouse_no_Oil_Changes}}
                    @endif
                </td>
            </tr>

            <tr>
                <td width="50%">

                    Spouse Cost Per Change

                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))
                        @if($vehicle_maintenance->eve_spouse_Cost_Per_Change !== $prev_data[$var_name]->eve_spouse_Cost_Per_Change)
                            ${{$prev_data[$var_name]->eve_spouse_Cost_Per_Change}}
                            <span class="red"> ${{$vehicle_maintenance->eve_spouse_Cost_Per_Change}}</span>
                        @else
                            ${{$vehicle_maintenance->eve_spouse_Cost_Per_Change}}
                        @endif
                    @else
                        ${{$vehicle_maintenance->eve_spouse_Cost_Per_Change}}
                    @endif


                </td>
            </tr>
        @endif
        <tr>
            <td width="50%">

                Did you have any tire changes or brakes replaced in
                2019? If so, what was the total cost?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($vehicle_maintenance->eve_Total_Tire_Costs_radio !== $prev_data[$var_name]->eve_Total_Tire_Costs_radio)
                        {{$prev_data[$var_name]->eve_Total_Tire_Costs_radio?'Yes':'No'}}
                        <span class="red">{{$vehicle_maintenance->eve_Total_Tire_Costs_radio?'Yes':'No'}}</span>
                    @else
                        {{$vehicle_maintenance->eve_Total_Tire_Costs_radio?'Yes':'No'}}
                    @endif
                @else
                    {{$vehicle_maintenance->eve_Total_Tire_Costs_radio?'Yes':'No'}}
                @endif

            </td>
        </tr>
        @if($vehicle_maintenance->eve_Total_Tire_Costs_radio)
            <tr>
                <td width="50%">

                    Total Tire or Brake Costs

                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))
                        @if($vehicle_maintenance->eve_Total_Tire_Costs !== $prev_data[$var_name]->eve_Total_Tire_Costs)
                            ${{$prev_data[$var_name]->eve_Total_Tire_Costs}}
                            <span class="red"> ${{$vehicle_maintenance->eve_Total_Tire_Costs}}</span>
                        @else
                            ${{$vehicle_maintenance->eve_Total_Tire_Costs}}
                        @endif
                    @else
                        ${{$vehicle_maintenance->eve_Total_Tire_Costs}}
                    @endif


                </td>
            </tr>
            @if($personal_info->marital_status=='1')
                <tr>
                    <td width="50%">

                        Total Tire or Brake Costs (Spouse)

                    </td>
                    <td width="50%">
                        @if(isset($prev_data[$var_name]))
                            @if($vehicle_maintenance->eve_spouse_Total_Tire_Costs !== $prev_data[$var_name]->eve_spouse_Total_Tire_Costs)
                                ${{$prev_data[$var_name]->eve_spouse_Total_Tire_Costs}}
                                <span class="red"> ${{$vehicle_maintenance->eve_spouse_Total_Tire_Costs}}</span>
                            @else
                                ${{$vehicle_maintenance->eve_spouse_Total_Tire_Costs}}
                            @endif
                        @else
                            ${{$vehicle_maintenance->eve_spouse_Total_Tire_Costs}}
                        @endif

                    </td>
                </tr>
            @endif
        @endif

        <tr>
            <td width="50%">

                Did you have any other repairs or maintenance costs
                associated with your work vehicle in 2019?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($vehicle_maintenance->eve_Total_Repairs_Cost_radio !== $prev_data[$var_name]->eve_Total_Repairs_Cost_radio)
                        {{$prev_data[$var_name]->eve_Total_Repairs_Cost_radio?'Yes':'No'}}
                        <span class="red">{{$vehicle_maintenance->eve_Total_Repairs_Cost_radio?'Yes':'No'}}</span>
                    @else
                        {{$vehicle_maintenance->eve_Total_Repairs_Cost_radio?'Yes':'No'}}
                    @endif
                @else
                    {{$vehicle_maintenance->eve_Total_Repairs_Cost_radio?'Yes':'No'}}
                @endif

            </td>
        </tr>
        @if($vehicle_maintenance->eve_Total_Repairs_Cost_radio)
            <tr>
                <td width="50%">

                    Total Repairs or Maintenance

                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))
                        @if($vehicle_maintenance->eve_Total_Repairs_Cost !== $prev_data[$var_name]->eve_Total_Repairs_Cost)
                            ${{$prev_data[$var_name]->eve_Total_Repairs_Cost}}
                            <span class="red"> ${{$vehicle_maintenance->eve_Total_Repairs_Cost}}</span>
                        @else
                            ${{$vehicle_maintenance->eve_Total_Repairs_Cost}}
                        @endif
                    @else
                        ${{$vehicle_maintenance->eve_Total_Repairs_Cost}}
                    @endif

                </td>
            </tr>
            @if($personal_info->marital_status=='1')
                <tr>
                    <td width="50%">

                        Total Repairs or Maintenance (Spouse)

                    </td>
                    <td width="50%">
                        @if(isset($prev_data[$var_name]))
                            @if($vehicle_maintenance->eve_spouse_Total_Repairs_Cost !== $prev_data[$var_name]->eve_spouse_Total_Repairs_Cost)
                                ${{$prev_data[$var_name]->eve_spouse_Total_Repairs_Cost}}
                                <span class="red"> ${{$vehicle_maintenance->eve_spouse_Total_Repairs_Cost}}</span>
                            @else
                                ${{$vehicle_maintenance->eve_spouse_Total_Repairs_Cost}}
                            @endif
                        @else
                            ${{$vehicle_maintenance->eve_spouse_Total_Repairs_Cost}}
                        @endif
                    </td>
                </tr>
            @endif
        @endif
    @endif
    @if(isset($vehicle_insurance))
        @php
            $var_name='vehicle_insurance';
        @endphp
        <tr>
            <td width="50%">

                Total Auto Insurance

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($vehicle_insurance->eves_Total_Auto_Insurance !== $prev_data[$var_name]->eves_Total_Auto_Insurance)
                        ${{$prev_data[$var_name]->eves_Total_Auto_Insurance}}
                        <span class="red"> ${{$vehicle_insurance->eves_Total_Auto_Insurance}}</span>
                    @else
                        ${{$vehicle_insurance->eves_Total_Auto_Insurance}}
                    @endif
                @else
                    ${{$vehicle_insurance->eves_Total_Auto_Insurance}}
                @endif
            </td>
        </tr>

        <tr>
            <td width="50%">

                Did you have any annual registration, safety
                inspection, or vehicle taxes in 2019?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($vehicle_insurance->eves_inspection_Total_Costs_radio !== $prev_data[$var_name]->eves_inspection_Total_Costs_radio)
                        {{$prev_data[$var_name]->eves_inspection_Total_Costs_radio?'Yes':'No'}}
                        <span class="red">{{$vehicle_insurance->eves_inspection_Total_Costs_radio?'Yes':'No'}}</span>
                    @else
                        {{$vehicle_insurance->eves_inspection_Total_Costs_radio?'Yes':'No'}}
                    @endif
                @else
                    {{$vehicle_insurance->eves_inspection_Total_Costs_radio?'Yes':'No'}}
                @endif

            </td>
        </tr>

        @if($vehicle_insurance->eves_inspection_Total_Costs_radio)
            <tr>
                <td width="50%">

                    Total Costs

                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))
                        @if($vehicle_insurance->eves_inspection_Total_Costs !== $prev_data[$var_name]->eves_inspection_Total_Costs)
                            ${{$prev_data[$var_name]->eves_inspection_Total_Costs}}
                            <span class="red"> ${{$vehicle_insurance->eves_inspection_Total_Costs}}</span>
                        @else
                            ${{$vehicle_insurance->eves_inspection_Total_Costs}}
                        @endif
                    @else
                        ${{$vehicle_insurance->eves_inspection_Total_Costs}}
                    @endif

                </td>
            </tr>
            @if($personal_info->marital_status=='1')
                <tr>
                    <td width="50%">

                        Total Costs (Spouse)

                    </td>
                    <td width="50%">
                        @if(isset($prev_data[$var_name]))
                            @if($vehicle_insurance->eves_spouse_inspection_Total_Costs !== $prev_data[$var_name]->eves_spouse_inspection_Total_Costs)
                                ${{$prev_data[$var_name]->eves_spouse_inspection_Total_Costs}}
                                <span class="red"> ${{$vehicle_insurance->eves_spouse_inspection_Total_Costs}}</span>
                            @else
                                ${{$vehicle_insurance->eves_spouse_inspection_Total_Costs}}
                            @endif
                        @else
                            ${{$vehicle_insurance->eves_spouse_inspection_Total_Costs}}
                        @endif


                    </td>
                </tr>
            @endif
        @endif
    @endif
    @if(isset($additional_transport))
        @php
            $var_name='additional_transport';
            $prop_child = $additional_transport->expenses;
        @endphp
        <tr>
            <th colspan="2">Additional Transportation Questions</th>
        </tr>
        <tr>
            <td width="50%">

                What’s the distance between your home and place of work?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($additional_transport->distance !== $prev_data[$var_name]->distance)
                        {{$prev_data[$var_name]->distance}}
                        <span class="red"> {{$additional_transport->distance}}</span>
                    @else
                        {{$additional_transport->distance}}
                    @endif
                @else
                    {{$additional_transport->distance}}
                @endif

            </td>
        </tr>
        <tr>
            <td width="50%">

                How many days a week do you commute to work?


            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($additional_transport->days !== $prev_data[$var_name]->days)
                        {{$prev_data[$var_name]->days}}
                        <span class="red"> {{$additional_transport->days}}</span>
                    @else
                        {{$additional_transport->days}}
                    @endif
                @else
                    {{$additional_transport->days}}
                @endif

            </td>
        </tr>
        <tr>
            <td width="50%">

                Total miles travelled


            </td>
            <td width="50%">
                <?php
                $totalMiles = isset($additional_transport) ? $additional_transport->days * $additional_transport->distance : null;
                $totalMilesPrev = isset($prev_data[$var_name]) ? $prev_data[$var_name]->days * $prev_data[$var_name]->distance : null;
                ?>
                @if(isset($totalMilesPrev))
                    @if($totalMiles !== $totalMilesPrev)
                        {{$totalMilesPrev}}
                        <span class="red"> {{$totalMiles}}</span>
                    @else
                        {{$totalMiles}}
                    @endif
                @else
                    {{$totalMiles}}
                @endif
            </td>
        </tr>
        <tr>
            <td width="50%">

                did you have any other work mileage, such as traveling between offices, business locations, client
                visits, or out-of-town conferences and seminars?


            </td>
            <td width="50%">

                @if(isset($prev_data[$var_name]))
                    @if($additional_transport->other_dist_checkbox !== $prev_data[$var_name]->other_dist_checkbox)
                        {{$prev_data[$var_name]->other_dist_checkbox?'Yes':'No'}}
                        <span class="red">{{$additional_transport->other_dist_checkbox?'Yes':'No'}}</span>
                    @else
                        {{$additional_transport->other_dist_checkbox?'Yes':'No'}}
                    @endif
                @else
                    {{$additional_transport->other_dist_checkbox?'Yes':'No'}}
                @endif

            </td>
        </tr>
        @if($additional_transport->other_dist_checkbox)
            @if(isset($prev_data[$var_name]))
                @php
                    $prev_prop_child =$prev_data[$var_name]->expenses;
                @endphp
                @if(count($prop_child)>count($prev_prop_child))
                    @php
                        $size = count($prop_child);
                    @endphp
                @else
                    @php
                        $size =count($prev_prop_child);
                    @endphp
                @endif
            @else
                @php
                    $size = count($prop_child);
                @endphp
            @endif
            @for($i=0;$i<$size;$i++)
                @php
                    $ex_value = isset($prop_child[$i])?$prop_child[$i]:null;
                    $prev_value = isset($prev_prop_child[$i])?$prev_prop_child[$i]:null;
                @endphp
                <tr>
                    <td width="50%">

                        Description

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->distance_description !== $prev_value->distance_description )
                                {{$prev_value->distance_description}}
                                <span
                                    class="red">
                        {{$ex_value->distance_description}}
                    </span>
                            @else
                                {{$ex_value->distance_description}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->distance_description)?'':'removed'}}">
                    {{isset($prev_value->distance_description)?$prev_value->distance_description:''}}
                </span>
                            <span class="{{isset($prev_value->distance_description)?'red':''}}">
                    {{isset($ex_value->distance_description)?$ex_value->distance_description:''}}
                </span>
                        @endif

                    </td>
                </tr>

                <tr>
                    <td width="50%">

                        Value

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->distance_value !== $prev_value->distance_value )
                                ${{$prev_value->distance_value}}
                                <span
                                    class="red">
                        ${{$ex_value->distance_value}}
                    </span>
                            @else
                                ${{$ex_value->distance_value}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->distance_value)?'':'removed'}}">
                    {{isset($prev_value->distance_value)?'$'.$prev_value->distance_value:''}}
                </span>
                            <span class="{{isset($prev_value->distance_description)?'red':''}}">
                    {{isset($ex_value->distance_value)?'$'.$ex_value->distance_value:''}}
                </span>
                        @endif
                    </td>
                </tr>

            @endfor
        @endif
    @endif
    @if(isset($entertainment))
        @php
            $var_name='entertainment';
            $prop_child = $entertainment->expenses;
        @endphp
        <tr>
            <th colspan="2">Meals and Entertainment</th>
        </tr>
        <tr>
            <td width="50%">

                Did you have any work-related meal expenses in 2019?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($entertainment->eme_time_per_week_radio !== $prev_data[$var_name]->eme_time_per_week_radio)
                        {{$prev_data[$var_name]->eme_time_per_week_radio?'Yes':'No'}}
                        <span class="red"> {{$entertainment->eme_time_per_week_radio?'Yes':'No'}}</span>
                    @else
                        {{$entertainment->eme_time_per_week_radio?'Yes':'No'}}
                    @endif
                @else
                    {{$entertainment->eme_time_per_week_radio?'Yes':'No'}}
                @endif
            </td>
        </tr>
        @if($entertainment->eme_time_per_week_radio)
            <tr>
                <td width="50%">

                    Primary Times Per/Week

                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))
                        @if($entertainment->eme_time_per_week !== $prev_data[$var_name]->eme_time_per_week)
                            {{$prev_data[$var_name]->eme_time_per_week}}
                            <span class="red"> {{$entertainment->eme_time_per_week}}</span>
                        @else
                            {{$entertainment->eme_time_per_week}}
                        @endif
                    @else
                        {{$entertainment->eme_time_per_week}}
                    @endif

                </td>
            </tr>

            <tr>
                <td width="50%">

                    Primary Cost Per/Meal

                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))
                        @if($entertainment->eme_meal_cost !== $prev_data[$var_name]->eme_meal_cost)
                            ${{$prev_data[$var_name]->eme_meal_cost}}
                            <span class="red"> ${{$entertainment->eme_meal_cost}}</span>
                        @else
                            ${{$entertainment->eme_meal_cost}}
                        @endif
                    @else
                        ${{$entertainment->eme_meal_cost}}
                    @endif
                </td>
            </tr>
            @if($personal_info->marital_status=='1')
                <tr>
                    <td width="50%">

                        Spouse Times Per/Week

                    </td>
                    <td width="50%">
                        @if(isset($prev_data[$var_name]))
                            @if($entertainment->eme_spouse_time_per_week !== $prev_data[$var_name]->eme_spouse_time_per_week)
                                {{$prev_data[$var_name]->eme_spouse_time_per_week}}
                                <span class="red"> {{$entertainment->eme_spouse_time_per_week}}</span>
                            @else
                                {{$entertainment->eme_spouse_time_per_week}}
                            @endif
                        @else
                            {{$entertainment->eme_spouse_time_per_week}}
                        @endif

                    </td>
                </tr>

                <tr>
                    <td width="50%">

                        Spouse Cost Per/Meal

                    </td>
                    <td width="50%">
                        @if(isset($prev_data[$var_name]))
                            @if($entertainment->eme_spouse_meal_cost !== $prev_data[$var_name]->eme_spouse_meal_cost)
                                ${{$prev_data[$var_name]->eme_spouse_meal_cost}}
                                <span class="red"> ${{$entertainment->eme_spouse_meal_cost}}</span>
                            @else
                                ${{$entertainment->eme_spouse_meal_cost}}
                            @endif
                        @else
                            ${{$entertainment->eme_spouse_meal_cost}}
                        @endif

                    </td>
                </tr>
            @endif
        @endif
        <tr>
            <td width="50%">

                Did you have any expenses associated with gifts or
                entertainment related to any coworkers or clients in 2019?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($entertainment->eme_gift_Description_radio !== $prev_data[$var_name]->eme_gift_Description_radio)
                        {{$prev_data[$var_name]->eme_gift_Description_radio?'Yes':'No'}}
                        <span class="red"> {{$entertainment->eme_gift_Description_radio?'Yes':'No'}}</span>
                    @else
                        {{$entertainment->eme_gift_Description_radio?'Yes':'No'}}
                    @endif
                @else
                    {{$entertainment->eme_gift_Description_radio?'Yes':'No'}}
                @endif

            </td>
        </tr>
        @if($entertainment->eme_gift_Description_radio)
            @if(isset($prev_data[$var_name]))
                @php
                    $prev_prop_child =$prev_data[$var_name]->expenses;
                @endphp
                @if(count($prop_child)>count($prev_prop_child))
                    @php
                        $size = count($prop_child);
                    @endphp
                @else
                    @php
                        $size =count($prev_prop_child);
                    @endphp
                @endif
            @else
                @php
                    $size = count($prop_child);
                @endphp
            @endif
            @for($i=0;$i<$size;$i++)
                @php
                    $ex_value = isset($prop_child[$i])?$prop_child[$i]:null;
                    $prev_value = isset($prev_prop_child[$i])?$prev_prop_child[$i]:null;
                @endphp
                <tr>
                    <td width="50%">

                        Description

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_description !== $prev_value->cost_description )
                                {{$prev_value->cost_description}}
                                <span
                                    class="red">
                        {{$ex_value->cost_description}}
                    </span>
                            @else
                                {{$ex_value->cost_description}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_description)?'':'removed'}}">
                    {{isset($prev_value->cost_description)?$prev_value->cost_description:''}}
                </span>
                            <span class="red">
                    {{isset($ex_value->cost_description)?$ex_value->cost_description:''}}
                </span>
                        @endif

                    </td>
                </tr>

                <tr>
                    <td width="50%">

                        Value

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_value !== $prev_value->cost_value )
                                ${{$prev_value->cost_value}}
                                <span
                                    class="red">
                        ${{$ex_value->cost_value}}
                    </span>
                            @else
                                ${{$ex_value->cost_value}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_value)?'':'removed'}}">
                    {{isset($prev_value->cost_value)?'$'.$prev_value->cost_value:''}}
                </span>
                            <span class="red">
                    {{isset($ex_value->cost_value)?'$'.$ex_value->cost_value:''}}
                </span>
                        @endif


                    </td>
                </tr>
            @endfor
        @endif
    @endif
    @if(isset($work_tool))
        @php
            $var_name='work_tool';
            $prop_child = $work_tool->expenses;
        @endphp
        <tr>
            <th colspan="2">Work Tools and Misc.</th>
        </tr>
        <tr>
            <td width="50%">

                Did you purchase a new office desk, chair, or other
                office furniture in 2019?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($work_tool->et_office_furniture_description_radio !== $prev_data[$var_name]->et_office_furniture_description_radio)
                        {{$prev_data[$var_name]->et_office_furniture_description_radio?'Yes':'No'}}
                        <span class="red">{{$work_tool->et_office_furniture_description_radio?'Yes':'No'}}</span>
                    @else
                        {{$work_tool->et_office_furniture_description_radio?'Yes':'No'}}
                    @endif
                @else
                    {{$work_tool->et_office_furniture_description_radio?'Yes':'No'}}
                @endif

            </td>
        </tr>
        @if($work_tool->et_office_furniture_description_radio)
            @if(isset($prev_data[$var_name]))
                @php
                    $prev_prop_child =$prev_data[$var_name]->expenses;
                @endphp
                @if(count($prop_child)>count($prev_prop_child))
                    @php
                        $size = count($prop_child);
                    @endphp
                @else
                    @php
                        $size =count($prev_prop_child);
                    @endphp
                @endif
            @else
                @php
                    $size = count($prop_child);
                @endphp
            @endif
            @for($i=0;$i<$size;$i++)
                @php
                    $ex_value = isset($prop_child[$i])?$prop_child[$i]:null;
                    $prev_value = isset($prev_prop_child[$i])?$prev_prop_child[$i]:null;
                @endphp
                <tr>
                    <td width="50%">

                        Description

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_description !== $prev_value->cost_description )
                                {{$prev_value->cost_description}}
                                <span
                                    class="red">
                        {{$ex_value->distance_description}}
                    </span>
                            @else
                                {{$ex_value->cost_description}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_description)?'':'removed'}}">
                    {{isset($prev_value->cost_description)?$prev_value->cost_description:''}}
                </span>
                            <span class="{{isset($prev_value->cost_description)?'red':''}}">
                    {{isset($ex_value->cost_description)?$ex_value->cost_description:''}}
                </span>
                        @endif

                    </td>
                </tr>

                <tr>
                    <td width="50%">

                        Value

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_value !== $prev_value->cost_value )
                                ${{$prev_value->cost_value}}
                                <span
                                    class="red">
                        ${{$ex_value->cost_value}}
                    </span>
                            @else
                                ${{$ex_value->cost_value}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->distance_value)?'':'removed'}}">
                    {{isset($prev_value->cost_value)?'$'.$prev_value->cost_value:''}}
                </span>
                            <span class="{{isset($prev_value->cost_value)?'red':''}}">
                    {{isset($ex_value->cost_value)?'$'.$ex_value->cost_value:''}}
                </span>
                        @endif
                    </td>
                </tr>
            @endfor
        @endif
        <tr>
            <td width="50%">

                Did you purchase any new computer, laptop, phone,
                tablet, printer, ink, other office equipment or software in 2019?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($work_tool->et_office_equipment_description_radio !== $prev_data[$var_name]->et_office_equipment_description_radio)
                        {{$prev_data[$var_name]->et_office_equipment_description_radio?'Yes':'No'}}
                        <span class="red">{{$work_tool->et_office_equipment_description_radio?'Yes':'No'}}</span>
                    @else
                        {{$work_tool->et_office_equipment_description_radio?'Yes':'No'}}
                    @endif
                @else
                    {{$work_tool->et_office_equipment_description_radio?'Yes':'No'}}
                @endif


            </td>
        </tr>
        @if($work_tool->et_office_equipment_description_radio)
            @php
                $prop_child = $work_tool->equipment;
            @endphp
            @if(isset($prev_data[$var_name]))
                @php
                    $prev_prop_child =$prev_data[$var_name]->equipment;
                @endphp
                @if(count($prop_child)>count($prev_prop_child))
                    @php
                        $size = count($prop_child);
                    @endphp
                @else
                    @php
                        $size =count($prev_prop_child);
                    @endphp
                @endif
            @else
                @php
                    $size = count($prop_child);
                @endphp
            @endif
            @for($i=0;$i<$size;$i++)
                @php
                    $ex_value = isset($prop_child[$i])?$prop_child[$i]:null;
                    $prev_value = isset($prev_prop_child[$i])?$prev_prop_child[$i]:null;
                @endphp
                <tr>
                    <td width="50%">

                        Description

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_description !== $prev_value->cost_description )
                                {{$prev_value->cost_description}}
                                <span
                                    class="red">
                        {{$ex_value->cost_description}}
                    </span>
                            @else
                                {{$ex_value->cost_description}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_description)?'':'removed'}}">
                    {{isset($prev_value->cost_description)?$prev_value->cost_description:''}}
                </span>
                            <span class="{{isset($prev_value->cost_description)?'red':''}}">
                    {{isset($ex_value->cost_description)?$ex_value->cost_description:''}}
                </span>
                        @endif

                    </td>
                </tr>

                <tr>
                    <td width="50%">

                        Value

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_value !== $prev_value->cost_value )
                                ${{$prev_value->cost_value}}
                                <span
                                    class="red">
                        ${{$ex_value->cost_value}}
                    </span>
                            @else
                                ${{$ex_value->cost_value}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_value)?'':'removed'}}">
                    {{isset($prev_value->cost_value)?'$'.$prev_value->cost_value:''}}
                </span>
                            <span class="{{isset($prev_value->cost_value)?'red':''}}">
                    {{isset($ex_value->cost_value)?'$'.$ex_value->cost_value:''}}
                </span>
                        @endif
                    </td>
                </tr>
            @endfor
        @endif

        <tr>
            <td width="50%">

                Did you have any job-seeking expenses in 2019?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($work_tool->et_seeking_expense_description_radio !== $prev_data[$var_name]->et_seeking_expense_description_radio)
                        {{$prev_data[$var_name]->et_seeking_expense_description_radio?'Yes':'No'}}
                        <span class="red">{{$work_tool->et_seeking_expense_description_radio?'Yes':'No'}}</span>
                    @else
                        {{$work_tool->et_seeking_expense_description_radio?'Yes':'No'}}
                    @endif
                @else
                    {{$work_tool->et_seeking_expense_description_radio?'Yes':'No'}}
                @endif

            </td>
        </tr>

        @if($work_tool->et_seeking_expense_description_radio)
            @php
                $prop_child = $work_tool->seeking;
            @endphp
            @if(isset($prev_data[$var_name]))
                @php
                    $prev_prop_child =$prev_data[$var_name]->seeking;
                @endphp
                @if(count($prop_child)>count($prev_prop_child))
                    @php
                        $size = count($prop_child);
                    @endphp
                @else
                    @php
                        $size =count($prev_prop_child);
                    @endphp
                @endif
            @else
                @php
                    $size = count($prop_child);
                @endphp
            @endif
            @for($i=0;$i<$size;$i++)
                @php
                    $ex_value = isset($prop_child[$i])?$prop_child[$i]:null;
                    $prev_value = isset($prev_prop_child[$i])?$prev_prop_child[$i]:null;
                @endphp
                <tr>
                    <td width="50%">

                        Description

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_description !== $prev_value->cost_description )
                                {{$prev_value->cost_description}}
                                <span
                                    class="red">
                        {{$ex_value->cost_description}}
                    </span>
                            @else
                                {{$ex_value->cost_description}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_description)?'':'removed'}}">
                    {{isset($prev_value->cost_description)?$prev_value->cost_description:''}}
                </span>
                            <span class="{{isset($prev_value->cost_description)?'red':''}}">
                    {{isset($ex_value->cost_description)?$ex_value->cost_description:''}}
                </span>
                        @endif

                    </td>
                </tr>

                <tr>
                    <td width="50%">

                        Value

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_value !== $prev_value->cost_value )
                                ${{$prev_value->cost_value}}
                                <span
                                    class="red">
                        ${{$ex_value->cost_value}}
                    </span>
                            @else
                                ${{$ex_value->cost_value}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_value)?'':'removed'}}">
                    {{isset($prev_value->cost_value)?'$'.$prev_value->cost_value:''}}
                </span>
                            <span class="{{isset($prev_value->cost_value)?'red':''}}">
                    {{isset($ex_value->cost_value)?'$'.$ex_value->cost_value:''}}
                </span>
                        @endif
                    </td>
                </tr>
            @endfor
        @endif
    @endif
    @if(isset($work_tool_cell))
        @php
            $var_name='work_tool_cell';
        @endphp
        <tr>
            <td width="50%">

                Monthly Cell Phone Bill

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($work_tool_cell->ets_Monthly_Cell_Phone_Bill !== $prev_data[$var_name]->ets_Monthly_Cell_Phone_Bill)
                        ${{$prev_data[$var_name]->ets_Monthly_Cell_Phone_Bill}}
                        <span class="red"> ${{$work_tool_cell->ets_Monthly_Cell_Phone_Bill}}</span>
                    @else
                        ${{$work_tool_cell->ets_Monthly_Cell_Phone_Bill}}
                    @endif
                @else
                    ${{$work_tool_cell->ets_Monthly_Cell_Phone_Bill}}
                @endif

            </td>
        </tr>
        @if($personal_info->marital_status=='1')

            <tr>
                <td width="50%">

                    Monthly Cell Phone Bill (Spouse)

                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))
                        @if($work_tool_cell->ets_Spouse_Monthly_Cell_Phone_Bill !== $prev_data[$var_name]->ets_Spouse_Monthly_Cell_Phone_Bill)
                            ${{$prev_data[$var_name]->ets_Spouse_Monthly_Cell_Phone_Bill}}
                            <span class="red"> ${{$work_tool_cell->ets_Spouse_Monthly_Cell_Phone_Bill}}</span>
                        @else
                            ${{$work_tool_cell->ets_Spouse_Monthly_Cell_Phone_Bill}}
                        @endif
                    @else
                        ${{$work_tool_cell->ets_Spouse_Monthly_Cell_Phone_Bill}}
                    @endif
                </td>
            </tr>
        @endif
        <tr>
            <td width="50%">

                Did you have any expenses associated with any
                networking events or conferences in 2019?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($work_tool_cell->ets_Networking_Expenses_radio !== $prev_data[$var_name]->ets_Networking_Expenses_radio)
                        {{$prev_data[$var_name]->ets_Networking_Expenses_radio?'Yes':'No'}}
                        <span class="red">{{$work_tool_cell->ets_Networking_Expenses_radio?'Yes':'No'}}</span>
                    @else
                        {{$work_tool_cell->ets_Networking_Expenses_radio?'Yes':'No'}}
                    @endif
                @else
                    {{$work_tool_cell->ets_Networking_Expenses_radio?'Yes':'No'}}
                @endif

            </td>
        </tr>

        @if($work_tool_cell->ets_Networking_Expenses_radio)
            <tr>
                <td width="50%">

                    Networking Expenses

                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))
                        @if($work_tool_cell->ets_Networking_Expenses !== $prev_data[$var_name]->ets_Networking_Expenses)
                            ${{$prev_data[$var_name]->ets_Networking_Expenses}}
                            <span class="red"> ${{$work_tool_cell->ets_Networking_Expenses}}</span>
                        @else
                            ${{$work_tool_cell->ets_Networking_Expenses}}
                        @endif
                    @else
                        ${{$work_tool_cell->ets_Networking_Expenses}}
                    @endif

                </td>
            </tr>
            @if($personal_info->marital_status=='1')
                <tr>
                    <td width="50%">

                        Networking Expenses (Spouse)

                    </td>
                    <td width="50%">
                        @if(isset($prev_data[$var_name]))
                            @if($work_tool_cell->ets_spouse_Networking_Expenses !== $prev_data[$var_name]->ets_spouse_Networking_Expenses)
                                ${{$prev_data[$var_name]->ets_spouse_Networking_Expenses}}
                                <span class="red"> ${{$work_tool_cell->ets_spouse_Networking_Expenses}}</span>
                            @else
                                ${{$work_tool_cell->ets_spouse_Networking_Expenses}}
                            @endif
                        @else
                            ${{$work_tool_cell->ets_spouse_Networking_Expenses}}
                        @endif
                    </td>
                </tr>
            @endif
        @endif
        <tr>
            <td width="50%">

                Did you have any continuing education, training, or
                certification expenses in 2019?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($work_tool_cell->ets_education_expenses_description_checkbox !== $prev_data[$var_name]->ets_education_expenses_description_checkbox)
                        {{$prev_data[$var_name]->ets_education_expenses_description_checkbox?'Yes':'No'}}
                        <span
                            class="red">{{$work_tool_cell->ets_education_expenses_description_checkbox?'Yes':'No'}}</span>
                    @else
                        {{$work_tool_cell->ets_education_expenses_description_checkbox?'Yes':'No'}}
                    @endif
                @else
                    {{$work_tool_cell->ets_education_expenses_description_checkbox?'Yes':'No'}}
                @endif

            </td>
        </tr>
        @if($work_tool_cell->ets_education_expenses_description_checkbox)
            @php
                $prop_child = $work_tool_cell->expenses;
            @endphp
            @if(isset($prev_data[$var_name]))
                @php
                    $prev_prop_child =$prev_data[$var_name]->expenses;
                @endphp
                @if(count($prop_child)>count($prev_prop_child))
                    @php
                        $size = count($prop_child);
                    @endphp
                @else
                    @php
                        $size =count($prev_prop_child);
                    @endphp
                @endif
            @else
                @php
                    $size = count($prop_child);
                @endphp
            @endif
            @for($i=0;$i<$size;$i++)
                @php
                    $ex_value = isset($prop_child[$i])?$prop_child[$i]:null;
                    $prev_value = isset($prev_prop_child[$i])?$prev_prop_child[$i]:null;
                @endphp
                <tr>
                    <td width="50%">

                        Description

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_description !== $prev_value->cost_description )
                                {{$prev_value->cost_description}}
                                <span
                                    class="red">
                        {{$ex_value->cost_description}}
                    </span>
                            @else
                                {{$ex_value->cost_description}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_description)?'':'removed'}}">
                    {{isset($prev_value->cost_description)?$prev_value->cost_description:''}}
                </span>
                            <span class="{{isset($prev_value->cost_description)?'red':''}}">
                    {{isset($ex_value->cost_description)?$ex_value->cost_description:''}}
                </span>
                        @endif

                    </td>
                </tr>

                <tr>
                    <td width="50%">

                        Value

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_value !== $prev_value->cost_value )
                                ${{$prev_value->cost_value}}
                                <span
                                    class="red">
                        ${{$ex_value->cost_value}}
                    </span>
                            @else
                                ${{$ex_value->cost_value}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_value)?'':'removed'}}">
                    {{isset($prev_value->cost_value)?'$'.$prev_value->cost_value:''}}
                </span>
                            <span class="{{isset($prev_value->cost_value)?'red':''}}">
                    {{isset($ex_value->cost_value)?'$'.$ex_value->cost_value:''}}
                </span>
                        @endif
                    </td>
                </tr>
            @endfor
        @endif
    @endif
    @if(isset($work_tool_exam))
        @php
            $var_name='work_tool_exam';
        @endphp
        <tr>
            <td width="50%">

                Did you have any professional exam or license renewal
                fees in 2019?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($work_tool_exam->professional_fees_radio !== $prev_data[$var_name]->professional_fees_radio)
                        {{$prev_data[$var_name]->professional_fees_radio?'Yes':'No'}}
                        <span class="red">{{$work_tool_exam->professional_fees_radio?'Yes':'No'}}</span>
                    @else
                        {{$work_tool_exam->professional_fees_radio?'Yes':'No'}}
                    @endif
                @else
                    {{$work_tool_exam->professional_fees_radio?'Yes':'No'}}
                @endif

            </td>
        </tr>
        @if($work_tool_exam->professional_fees_radio)
            @php
                $prop_child = $work_tool_exam->professional;
            @endphp
            @if(isset($prev_data[$var_name]))
                @php
                    $prev_prop_child =$prev_data[$var_name]->professional;
                @endphp
                @if(count($prop_child)>count($prev_prop_child))
                    @php
                        $size = count($prop_child);
                    @endphp
                @else
                    @php
                        $size =count($prev_prop_child);
                    @endphp
                @endif
            @else
                @php
                    $size = count($prop_child);
                @endphp
            @endif
            @for($i=0;$i<$size;$i++)
                @php
                    $ex_value = isset($prop_child[$i])?$prop_child[$i]:null;
                    $prev_value = isset($prev_prop_child[$i])?$prev_prop_child[$i]:null;
                @endphp
                <tr>
                    <td width="50%">

                        Description

                    </td>
                    <td width="50%">
                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_description !== $prev_value->cost_description )
                                {{$prev_value->cost_description}}
                                <span
                                    class="red">
                        {{$ex_value->cost_description}}
                    </span>
                            @else
                                {{$ex_value->cost_description}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_description)?'':'removed'}}">
                    {{isset($prev_value->cost_description)?$prev_value->cost_description:''}}
                </span>
                            {{--                        Correct condition--}}
                            <span
                                class="{{(!isset($prev_value->cost_description)&&!isset($prev_data[$var_name]))?'':'red'}}">
                    {{isset($ex_value->cost_description)?$ex_value->cost_description:''}}
                </span>
                        @endif

                    </td>
                </tr>

                <tr>
                    <td width="50%">

                        Value

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_value !== $prev_value->cost_value )
                                ${{$prev_value->cost_value}}
                                <span
                                    class="red">
                        ${{$ex_value->cost_value}}
                    </span>
                            @else
                                ${{$ex_value->cost_value}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_value)?'':'removed'}}">
                    {{isset($prev_value->cost_value)?'$'.$prev_value->cost_value:''}}
                </span>
                            <span class="{{(!isset($prev_value->cost_value)&&!isset($prev_data[$var_name]))?'':'red'}}">
                    {{isset($ex_value->cost_value)?'$'.$ex_value->cost_value:''}}
                </span>
                        @endif
                    </td>
                </tr>
            @endfor
        @endif

        <tr>
            <td width="50%">

                Did you have any personal professional development
                expenses in 2019?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($work_tool_exam->professional_dev_radio !== $prev_data[$var_name]->professional_dev_radio)
                        {{$prev_data[$var_name]->professional_dev_radio?'Yes':'No'}}
                        <span class="red">{{$work_tool_exam->professional_dev_radio?'Yes':'No'}}</span>
                    @else
                        {{$work_tool_exam->professional_dev_radio?'Yes':'No'}}
                    @endif
                @else
                    {{$work_tool_exam->professional_dev_radio?'Yes':'No'}}
                @endif


            </td>
        </tr>
        @if($work_tool_exam->professional_dev_radio)
            <tr>
                <td width="50%">

                    Professional Development

                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))
                        @if($work_tool_exam->professional_dev !== $prev_data[$var_name]->professional_dev)
                            ${{$prev_data[$var_name]->professional_dev}}
                            <span class="red"> ${{$work_tool_exam->professional_dev}}</span>
                        @else
                            ${{$work_tool_exam->professional_dev}}
                        @endif
                    @else
                        ${{$work_tool_exam->professional_dev}}
                    @endif

                </td>
            </tr>
            @if($personal_info->marital_status=='1')

                <tr>
                    <td width="50%">

                        Professional Development (Spouse)

                    </td>
                    <td width="50%">
                        @if(isset($prev_data[$var_name]))
                            @if($work_tool_exam->professional_dev_spouse !== $prev_data[$var_name]->professional_dev_spouse)
                                ${{$prev_data[$var_name]->professional_dev_spouse}}
                                <span class="red"> ${{$work_tool_exam->professional_dev_spouse}}</span>
                            @else
                                ${{$work_tool_exam->professional_dev_spouse}}
                            @endif
                        @else
                            ${{$work_tool_exam->professional_dev_spouse}}
                        @endif
                    </td>
                </tr>
            @endif
        @endif

        <tr>
            <td width="50%">

                <a name="page12"></a>
                Did you have any membership fees for any professional
                organizations in 2019? This includes any union dues in
                2019?
            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($work_tool_exam->membership_fees_radio !== $prev_data[$var_name]->membership_fees_radio)
                        {{$prev_data[$var_name]->membership_fees_radio?'Yes':'No'}}
                        <span class="red">{{$work_tool_exam->membership_fees_radio?'Yes':'No'}}</span>
                    @else
                        {{$work_tool_exam->membership_fees_radio?'Yes':'No'}}
                    @endif
                @else
                    {{$work_tool_exam->membership_fees_radio?'Yes':'No'}}
                @endif


            </td>
        </tr>
        @if($work_tool_exam->membership_fees_radio)
            @php
                $prop_child = $work_tool_exam->membership;
            @endphp
            @if(isset($prev_data[$var_name]))
                @php
                    $prev_prop_child =$prev_data[$var_name]->membership;
                @endphp
                @if(count($prop_child)>count($prev_prop_child))
                    @php
                        $size = count($prop_child);
                    @endphp
                @else
                    @php
                        $size =count($prev_prop_child);
                    @endphp
                @endif
            @else
                @php
                    $size = count($prop_child);
                @endphp
            @endif
            @for($i=0;$i<$size;$i++)
                @php
                    $ex_value = isset($prop_child[$i])?$prop_child[$i]:null;
                    $prev_value = isset($prev_prop_child[$i])?$prev_prop_child[$i]:null;
                @endphp
                <tr>
                    <td width="50%">

                        Description

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_description !== $prev_value->cost_description )
                                {{$prev_value->cost_description}}
                                <span
                                    class="red">
                        {{$ex_value->cost_description}}
                    </span>
                            @else
                                {{$ex_value->cost_description}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_description)?'':'removed'}}">
                    {{isset($prev_value->cost_description)?$prev_value->cost_description:''}}
                </span>
                            <span class="{{isset($prev_value->cost_description)?'red':''}}">
                    {{isset($ex_value->cost_description)?$ex_value->cost_description:''}}
                </span>
                        @endif

                    </td>
                </tr>

                <tr>
                    <td width="50%">

                        Value

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_value !== $prev_value->cost_value )
                                ${{$prev_value->cost_value}}
                                <span
                                    class="red">
                        ${{$ex_value->cost_value}}
                    </span>
                            @else
                                ${{$ex_value->cost_value}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_value)?'':'removed'}}">
                    {{isset($prev_value->cost_value)?'$'.$prev_value->cost_value:''}}
                </span>
                            <span class="{{isset($prev_value->cost_value)?'red':''}}">
                    {{isset($ex_value->cost_value)?'$'.$ex_value->cost_value:''}}
                </span>
                        @endif
                    </td>
                </tr>
            @endfor
        @endif
    @endif
    @if(isset($work_tool_research))
        @php
            $var_name='work_tool_research';
        @endphp
        <tr>
            <td width="50%">

                Did you have any expenses associated with any
                subscription or research expenses associated with your job?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($work_tool_research->research_radio !== $prev_data[$var_name]->research_radio)
                        {{$prev_data[$var_name]->research_radio?'Yes':'No'}}
                        <span class="red">{{$work_tool_research->research_radio?'Yes':'No'}}</span>
                    @else
                        {{$work_tool_research->research_radio?'Yes':'No'}}
                    @endif
                @else
                    {{$work_tool_research->research_radio?'Yes':'No'}}
                @endif

            </td>
        </tr>
        @if($work_tool_research->research_radio)
            @php
                $prop_child = $work_tool_research->expenses;
            @endphp
            @if(isset($prev_data[$var_name]))
                @php
                    $prev_prop_child =$prev_data[$var_name]->expenses;
                @endphp
                @if(count($prop_child)>count($prev_prop_child))
                    @php
                        $size = count($prop_child);
                    @endphp
                @else
                    @php
                        $size =count($prev_prop_child);
                    @endphp
                @endif
            @else
                @php
                    $size = count($prop_child);
                @endphp
            @endif
            @for($i=0;$i<$size;$i++)
                @php
                    $ex_value = isset($prop_child[$i])?$prop_child[$i]:null;
                    $prev_value = isset($prev_prop_child[$i])?$prev_prop_child[$i]:null;
                @endphp
                <tr>
                    <td width="50%">

                        Description

                    </td>
                    <td width="50%">
                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_description !== $prev_value->cost_description )
                                {{$prev_value->cost_description}}
                                <span
                                    class="red">
                        {{$ex_value->cost_description}}
                    </span>
                            @else
                                {{$ex_value->cost_description}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_description)?'':'removed'}}">
                    {{isset($prev_value->cost_description)?$prev_value->cost_description:''}}
                </span>
                            {{--                        Correct condition--}}
                            <span
                                class="{{(!isset($prev_value->cost_description)&&!isset($prev_data[$var_name]))?'':'red'}}">
                    {{isset($ex_value->cost_description)?$ex_value->cost_description:''}}
                </span>
                        @endif

                    </td>
                </tr>

                <tr>
                    <td width="50%">

                        Value

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_value !== $prev_value->cost_value )
                                ${{$prev_value->cost_value}}
                                <span
                                    class="red">
                        ${{$ex_value->cost_value}}
                    </span>
                            @else
                                ${{$ex_value->cost_value}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_value)?'':'removed'}}">
                    {{isset($prev_value->cost_value)?'$'.$prev_value->cost_value:''}}
                </span>
                            <span class="{{(!isset($prev_value->cost_value)&&!isset($prev_data[$var_name]))?'':'red'}}">
                    {{isset($ex_value->cost_value)?'$'.$ex_value->cost_value:''}}
                </span>
                        @endif
                    </td>
                </tr>
            @endfor
        @endif
        <tr>
            <td width="50%">

                How much did you pay each month for your home Internet
                service in 2019?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($work_tool_research->internet_pay !== $prev_data[$var_name]->internet_pay)
                        ${{$prev_data[$var_name]->internet_pay}}
                        <span class="red"> ${{$work_tool_research->internet_pay}}</span>
                    @else
                        ${{$work_tool_research->internet_pay}}
                    @endif
                @else
                    ${{$work_tool_research->internet_pay}}
                @endif

            </td>
        </tr>

    @endif
    @if(isset($blue_collar_expense))
        @php
            $var_name='blue_collar_expense';
        @endphp
        <tr>
            <th colspan="2">Blue Collar Expenses</th>
        </tr>
        <tr>
            <td width="50%">

                Did you have any expenses for any equipment,
                instruments, or tools in 2019?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($blue_collar_expense->expenses_equipment_radio !== $prev_data[$var_name]->expenses_equipment_radio)
                        {{$prev_data[$var_name]->expenses_equipment_radio?'Yes':'No'}}
                        <span class="red">{{$blue_collar_expense->expenses_equipment_radio?'Yes':'No'}}</span>
                    @else
                        {{$blue_collar_expense->expenses_equipment_radio?'Yes':'No'}}
                    @endif
                @else
                    {{$blue_collar_expense->expenses_equipment_radio?'Yes':'No'}}
                @endif
            </td>
        </tr>
        @if($blue_collar_expense->expenses_equipment_radio)
            @php
                $prop_child = $blue_collar_expense->equipment;
            @endphp
            @if(isset($prev_data[$var_name]))
                @php
                    $prev_prop_child =$prev_data[$var_name]->equipment;
                @endphp
                @if(count($prop_child)>count($prev_prop_child))
                    @php
                        $size = count($prop_child);
                    @endphp
                @else
                    @php
                        $size =count($prev_prop_child);
                    @endphp
                @endif
            @else
                @php
                    $size = count($prop_child);
                @endphp
            @endif
            @for($i=0;$i<$size;$i++)
                @php
                    $ex_value = isset($prop_child[$i])?$prop_child[$i]:null;
                    $prev_value = isset($prev_prop_child[$i])?$prev_prop_child[$i]:null;
                @endphp
                <tr>
                    <td width="50%">

                        Description

                    </td>
                    <td width="50%">
                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_description !== $prev_value->cost_description )
                                {{$prev_value->cost_description}}
                                <span
                                    class="red">
                        {{$ex_value->cost_description}}
                    </span>
                            @else
                                {{$ex_value->cost_description}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_description)?'':'removed'}}">
                    {{isset($prev_value->cost_description)?$prev_value->cost_description:''}}
                </span>
                            {{--                        Correct condition--}}
                            <span
                                class="{{(!isset($prev_value->cost_description)&&!isset($prev_data[$var_name]))?'':'red'}}">
                    {{isset($ex_value->cost_description)?$ex_value->cost_description:''}}
                </span>
                        @endif

                    </td>
                </tr>

                <tr>
                    <td width="50%">

                        Value

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_value !== $prev_value->cost_value )
                                ${{$prev_value->cost_value}}
                                <span
                                    class="red">
                        ${{$ex_value->cost_value}}
                    </span>
                            @else
                                ${{$ex_value->cost_value}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_value)?'':'removed'}}">
                    {{isset($prev_value->cost_value)?'$'.$prev_value->cost_value:''}}
                </span>
                            <span class="{{(!isset($prev_value->cost_value)&&!isset($prev_data[$var_name]))?'':'red'}}">
                    {{isset($ex_value->cost_value)?'$'.$ex_value->cost_value:''}}
                </span>
                        @endif
                    </td>
                </tr>
            @endfor
        @endif
        <tr>
            <td width="50%">

                Did you have any expenses for purchasing any machinery?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($blue_collar_expense->purchasing_machinery_radio !== $prev_data[$var_name]->purchasing_machinery_radio)
                        {{$prev_data[$var_name]->purchasing_machinery_radio?'Yes':'No'}}
                        <span class="red">{{$blue_collar_expense->purchasing_machinery_radio?'Yes':'No'}}</span>
                    @else
                        {{$blue_collar_expense->purchasing_machinery_radio?'Yes':'No'}}
                    @endif
                @else
                    {{$blue_collar_expense->purchasing_machinery_radio?'Yes':'No'}}
                @endif

            </td>
        </tr>
        @if($blue_collar_expense->purchasing_machinery_radio)
            @php
                $prop_child = $blue_collar_expense->machinery;
            @endphp
            @if(isset($prev_data[$var_name]))
                @php
                    $prev_prop_child =$prev_data[$var_name]->machinery;
                @endphp
                @if(count($prop_child)>count($prev_prop_child))
                    @php
                        $size = count($prop_child);
                    @endphp
                @else
                    @php
                        $size =count($prev_prop_child);
                    @endphp
                @endif
            @else
                @php
                    $size = count($prop_child);
                @endphp
            @endif
            @for($i=0;$i<$size;$i++)
                @php
                    $ex_value = isset($prop_child[$i])?$prop_child[$i]:null;
                    $prev_value = isset($prev_prop_child[$i])?$prev_prop_child[$i]:null;
                @endphp
                <tr>
                    <td width="50%">

                        Description

                    </td>
                    <td width="50%">
                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_description !== $prev_value->cost_description )
                                {{$prev_value->cost_description}}
                                <span
                                    class="red">
                        {{$ex_value->cost_description}}
                    </span>
                            @else
                                {{$ex_value->cost_description}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_description)?'':'removed'}}">
                    {{isset($prev_value->cost_description)?$prev_value->cost_description:''}}
                </span>
                            {{--                        Correct condition--}}
                            <span
                                class="{{(!isset($prev_value->cost_description)&&!isset($prev_data[$var_name]))?'':'red'}}">
                    {{isset($ex_value->cost_description)?$ex_value->cost_description:''}}
                </span>
                        @endif

                    </td>
                </tr>

                <tr>
                    <td width="50%">

                        Value

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_value !== $prev_value->cost_value )
                                ${{$prev_value->cost_value}}
                                <span
                                    class="red">
                        ${{$ex_value->cost_value}}
                    </span>
                            @else
                                ${{$ex_value->cost_value}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_value)?'':'removed'}}">
                    {{isset($prev_value->cost_value)?'$'.$prev_value->cost_value:''}}
                </span>
                            <span class="{{(!isset($prev_value->cost_value)&&!isset($prev_data[$var_name]))?'':'red'}}">
                    {{isset($ex_value->cost_value)?'$'.$ex_value->cost_value:''}}
                </span>
                        @endif
                    </td>
                </tr>
            @endfor
        @endif
    @endif
    @if(isset($blue_collar_expense_two))
        @php
            $var_name='blue_collar_expense_two';
        @endphp
        <tr>
            <td width="50%">

                Did you have any expenses for uniforms?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($blue_collar_expense_two->expenses_uniforms_radio !== $prev_data[$var_name]->expenses_uniforms_radio)
                        {{$prev_data[$var_name]->expenses_uniforms_radio?'Yes':'No'}}
                        <span class="red">{{$blue_collar_expense_two->expenses_uniforms_radio?'Yes':'No'}}</span>
                    @else
                        {{$blue_collar_expense_two->expenses_uniforms_radio?'Yes':'No'}}
                    @endif
                @else
                    {{$blue_collar_expense_two->expenses_uniforms_radio?'Yes':'No'}}
                @endif

            </td>
        </tr>
        @if($blue_collar_expense_two->expenses_uniforms_radio)
            @php
                $prop_child = $blue_collar_expense_two->uniform;
            @endphp
            @if(isset($prev_data[$var_name]))
                @php
                    $prev_prop_child =$prev_data[$var_name]->uniform;
                @endphp
                @if(count($prop_child)>count($prev_prop_child))
                    @php
                        $size = count($prop_child);
                    @endphp
                @else
                    @php
                        $size =count($prev_prop_child);
                    @endphp
                @endif
            @else
                @php
                    $size = count($prop_child);
                @endphp
            @endif
            @for($i=0;$i<$size;$i++)
                @php
                    $ex_value = isset($prop_child[$i])?$prop_child[$i]:null;
                    $prev_value = isset($prev_prop_child[$i])?$prev_prop_child[$i]:null;
                @endphp
                <tr>
                    <td width="50%">

                        Description

                    </td>
                    <td width="50%">
                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_description !== $prev_value->cost_description )
                                {{$prev_value->cost_description}}
                                <span
                                    class="red">
                        {{$ex_value->cost_description}}
                    </span>
                            @else
                                {{$ex_value->cost_description}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_description)?'':'removed'}}">
                    {{isset($prev_value->cost_description)?$prev_value->cost_description:''}}
                </span>
                            {{--                        Correct condition--}}
                            <span
                                class="{{(!isset($prev_value->cost_description)&&!isset($prev_data[$var_name]))?'':'red'}}">
                    {{isset($ex_value->cost_description)?$ex_value->cost_description:''}}
                </span>
                        @endif

                    </td>
                </tr>

                <tr>
                    <td width="50%">

                        Value

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_value !== $prev_value->cost_value )
                                ${{$prev_value->cost_value}}
                                <span
                                    class="red">
                        ${{$ex_value->cost_value}}
                    </span>
                            @else
                                ${{$ex_value->cost_value}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_value)?'':'removed'}}">
                    {{isset($prev_value->cost_value)?'$'.$prev_value->cost_value:''}}
                </span>
                            <span class="{{(!isset($prev_value->cost_value)&&!isset($prev_data[$var_name]))?'':'red'}}">
                    {{isset($ex_value->cost_value)?'$'.$ex_value->cost_value:''}}
                </span>
                        @endif
                    </td>
                </tr>
            @endfor
        @endif
        <tr>
            <td width="50%">

                Did you have any other work clothing expenses, like
                work shoes or work boots?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($blue_collar_expense_two->work_clothing_radio !== $prev_data[$var_name]->work_clothing_radio)
                        {{$prev_data[$var_name]->work_clothing_radio?'Yes':'No'}}
                        <span class="red">{{$blue_collar_expense_two->work_clothing_radio?'Yes':'No'}}</span>
                    @else
                        {{$blue_collar_expense_two->work_clothing_radio?'Yes':'No'}}
                    @endif
                @else
                    {{$blue_collar_expense_two->work_clothing_radio?'Yes':'No'}}
                @endif


            </td>
        </tr>

        @if($blue_collar_expense_two->work_clothing_radio)
            @php
                $prop_child = $blue_collar_expense_two->clothing;
            @endphp
            @if(isset($prev_data[$var_name]))
                @php
                    $prev_prop_child =$prev_data[$var_name]->clothing;
                @endphp
                @if(count($prop_child)>count($prev_prop_child))
                    @php
                        $size = count($prop_child);
                    @endphp
                @else
                    @php
                        $size =count($prev_prop_child);
                    @endphp
                @endif
            @else
                @php
                    $size = count($prop_child);
                @endphp
            @endif
            @for($i=0;$i<$size;$i++)
                @php
                    $ex_value = isset($prop_child[$i])?$prop_child[$i]:null;
                    $prev_value = isset($prev_prop_child[$i])?$prev_prop_child[$i]:null;
                @endphp
                <tr>
                    <td width="50%">

                        Description

                    </td>
                    <td width="50%">
                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_description !== $prev_value->cost_description )
                                {{$prev_value->cost_description}}
                                <span
                                    class="red">
                        {{$ex_value->cost_description}}
                    </span>
                            @else
                                {{$ex_value->cost_description}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_description)?'':'removed'}}">
                    {{isset($prev_value->cost_description)?$prev_value->cost_description:''}}
                </span>
                            {{--                        Correct condition--}}
                            <span
                                class="{{(!isset($prev_value->cost_description)&&!isset($prev_data[$var_name]))?'':'red'}}">
                    {{isset($ex_value->cost_description)?$ex_value->cost_description:''}}
                </span>
                        @endif

                    </td>
                </tr>

                <tr>
                    <td width="50%">

                        Value

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_value !== $prev_value->cost_value )
                                ${{$prev_value->cost_value}}
                                <span
                                    class="red">
                        ${{$ex_value->cost_value}}
                    </span>
                            @else
                                ${{$ex_value->cost_value}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_value)?'':'removed'}}">
                    {{isset($prev_value->cost_value)?'$'.$prev_value->cost_value:''}}
                </span>
                            <span class="{{(!isset($prev_value->cost_value)&&!isset($prev_data[$var_name]))?'':'red'}}">
                    {{isset($ex_value->cost_value)?'$'.$ex_value->cost_value:''}}
                </span>
                        @endif
                    </td>
                </tr>
            @endfor
        @endif

        <tr>
            <td width="50%">

                Did you have any dry cleaning, tailoring, or altering
                expenses for any of the uniforms or work clothing?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($blue_collar_expense_two->dry_cleaning_radio !== $prev_data[$var_name]->dry_cleaning_radio)
                        {{$prev_data[$var_name]->dry_cleaning_radio?'Yes':'No'}}
                        <span class="red">{{$blue_collar_expense_two->dry_cleaning_radio?'Yes':'No'}}</span>
                    @else
                        {{$blue_collar_expense_two->dry_cleaning_radio?'Yes':'No'}}
                    @endif
                @else
                    {{$blue_collar_expense_two->dry_cleaning_radio?'Yes':'No'}}
                @endif

            </td>
        </tr>

        @if($blue_collar_expense_two->dry_cleaning_radio)
            @php
                $prop_child = $blue_collar_expense_two->cleaning;
            @endphp
            @if(isset($prev_data[$var_name]))
                @php
                    $prev_prop_child =$prev_data[$var_name]->cleaning;
                @endphp
                @if(count($prop_child)>count($prev_prop_child))
                    @php
                        $size = count($prop_child);
                    @endphp
                @else
                    @php
                        $size =count($prev_prop_child);
                    @endphp
                @endif
            @else
                @php
                    $size = count($prop_child);
                @endphp
            @endif
            @for($i=0;$i<$size;$i++)
                @php
                    $ex_value = isset($prop_child[$i])?$prop_child[$i]:null;
                    $prev_value = isset($prev_prop_child[$i])?$prev_prop_child[$i]:null;
                @endphp
                <tr>
                    <td width="50%">

                        Description

                    </td>
                    <td width="50%">
                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_description !== $prev_value->cost_description )
                                {{$prev_value->cost_description}}
                                <span
                                    class="red">
                        {{$ex_value->cost_description}}
                    </span>
                            @else
                                {{$ex_value->cost_description}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_description)?'':'removed'}}">
                    {{isset($prev_value->cost_description)?$prev_value->cost_description:''}}
                </span>
                            {{--                        Correct condition--}}
                            <span
                                class="{{(!isset($prev_value->cost_description)&&!isset($prev_data[$var_name]))?'':'red'}}">
                    {{isset($ex_value->cost_description)?$ex_value->cost_description:''}}
                </span>
                        @endif

                    </td>
                </tr>

                <tr>
                    <td width="50%">

                        Value

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_value !== $prev_value->cost_value )
                                ${{$prev_value->cost_value}}
                                <span
                                    class="red">
                        ${{$ex_value->cost_value}}
                    </span>
                            @else
                                ${{$ex_value->cost_value}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_value)?'':'removed'}}">
                    {{isset($prev_value->cost_value)?'$'.$prev_value->cost_value:''}}
                </span>
                            <span class="{{(!isset($prev_value->cost_value)&&!isset($prev_data[$var_name]))?'':'red'}}">
                    {{isset($ex_value->cost_value)?'$'.$ex_value->cost_value:''}}
                </span>
                        @endif
                    </td>
                </tr>
            @endfor
        @endif
    @endif
    @if(isset($emergency_personal))
        @php
            $var_name='emergency_personal';
        @endphp
        <tr>
            <th colspan="2">Emergency Personnel</th>
        </tr>
        <tr>
            <td width="50%">

                Are You Emergency Personnel? (Military, Police, Fire,
                EMS)

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($emergency_personal->personal_status !== $prev_data[$var_name]->personal_status)
                        {{$prev_data[$var_name]->personal_status?'Yes':'No'}}
                        <span class="red">{{$emergency_personal->personal_status?'Yes':'No'}}</span>
                    @else
                        {{$emergency_personal->personal_status?'Yes':'No'}}
                    @endif
                @else
                    {{$emergency_personal->personal_status?'Yes':'No'}}
                @endif

            </td>
        </tr>
    @endif
    @if(isset($emergency_personal_exp))
        @php
            $var_name='emergency_personal_exp';
            $isEmergency =  count($emergency_personal_exp->emergency)>0;
        @endphp
        <tr>
            <th colspan="2">Emergency Personnel Expenses</th>
        </tr>
        <tr>
            <td width="50%">

                Did you purchase any new weapons in 2019?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($emergency_personal_exp->weapons_radio !== $prev_data[$var_name]->weapons_radio)
                        {{$prev_data[$var_name]->weapons_radio?'Yes':'No'}}
                        <span class="red">{{$emergency_personal_exp->weapons_radio?'Yes':'No'}}</span>
                    @else
                        {{$emergency_personal_exp->weapons_radio?'Yes':'No'}}
                    @endif
                @else
                    {{$emergency_personal_exp->weapons_radio?'Yes':'No'}}
                @endif

            </td>
        </tr>
        @if($emergency_personal_exp->weapons_radio)
            @php
                $prop_child = $emergency_personal_exp->weapons;
            @endphp
            @if(isset($prev_data[$var_name]))
                @php
                    $prev_prop_child =$prev_data[$var_name]->weapons;
                @endphp
                @if(count($prop_child)>count($prev_prop_child))
                    @php
                        $size = count($prop_child);
                    @endphp
                @else
                    @php
                        $size =count($prev_prop_child);
                    @endphp
                @endif
            @else
                @php
                    $size = count($prop_child);
                @endphp
            @endif
            @for($i=0;$i<$size;$i++)
                @php
                    $ex_value = isset($prop_child[$i])?$prop_child[$i]:null;
                    $prev_value = isset($prev_prop_child[$i])?$prev_prop_child[$i]:null;
                @endphp
                <tr>
                    <td width="50%">

                        Description

                    </td>
                    <td width="50%">
                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_description !== $prev_value->cost_description )
                                {{$prev_value->cost_description}}
                                <span
                                    class="red">
                        {{$ex_value->cost_description}}
                    </span>
                            @else
                                {{$ex_value->cost_description}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_description)?'':'removed'}}">
                    {{isset($prev_value->cost_description)?$prev_value->cost_description:''}}
                </span>
                            {{--                        Correct condition--}}
                            <span
                                class="{{(!isset($prev_value->cost_description)&&!isset($prev_data[$var_name]))?'':'red'}}">
                    {{isset($ex_value->cost_description)?$ex_value->cost_description:''}}
                </span>
                        @endif

                    </td>
                </tr>

                <tr>
                    <td width="50%">

                        Value

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_value !== $prev_value->cost_value )
                                ${{$prev_value->cost_value}}
                                <span
                                    class="red">
                        ${{$ex_value->cost_value}}
                    </span>
                            @else
                                ${{$ex_value->cost_value}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_value)?'':'removed'}}">
                    {{isset($prev_value->cost_value)?'$'.$prev_value->cost_value:''}}
                </span>
                            <span class="{{(!isset($prev_value->cost_value)&&!isset($prev_data[$var_name]))?'':'red'}}">
                    {{isset($ex_value->cost_value)?'$'.$ex_value->cost_value:''}}
                </span>
                        @endif
                    </td>
                </tr>
            @endfor
        @endif
        <tr>
            <td width="50%">

                Any Emergency Personnel equipment; this can include
                special lightweight body armor camel packs for hot environments
                magazines tactical flashlights tasers tear gas med kits etc

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @php

                        $isEmergencyPrew =  count($prev_data[$var_name]->emergency)>0;
                    @endphp
                    @if($isEmergency !== $isEmergencyPrew)
                        {{$isEmergencyPrew?'Yes':'No'}}
                        <span class="red">{{$isEmergency?'Yes':'No'}}</span>
                    @else
                        {{$isEmergency?'Yes':'No'}}
                    @endif
                @else
                    {{$isEmergency?'Yes':'No'}}
                @endif


            </td>
        </tr>
        @if(count($emergency_personal_exp->emergency)>0)
            @php
                $prop_child = $emergency_personal_exp->emergency;
            @endphp
            @if(isset($prev_data[$var_name]))
                @php
                    $prev_prop_child =$prev_data[$var_name]->emergency;
                @endphp
                @if(count($prop_child)>count($prev_prop_child))
                    @php
                        $size = count($prop_child);
                    @endphp
                @else
                    @php
                        $size =count($prev_prop_child);
                    @endphp
                @endif
            @else
                @php
                    $size = count($prop_child);
                @endphp
            @endif
            @for($i=0;$i<$size;$i++)
                @php
                    $ex_value = isset($prop_child[$i])?$prop_child[$i]:null;
                    $prev_value = isset($prev_prop_child[$i])?$prev_prop_child[$i]:null;
                @endphp
                <tr>
                    <td width="50%">

                        Description

                    </td>
                    <td width="50%">
                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_description !== $prev_value->cost_description )
                                {{$prev_value->cost_description}}
                                <span
                                    class="red">
                        {{$ex_value->cost_description}}
                    </span>
                            @else
                                {{$ex_value->cost_description}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_description)?'':'removed'}}">
                    {{isset($prev_value->cost_description)?$prev_value->cost_description:''}}
                </span>
                            {{--                        Correct condition--}}
                            <span
                                class="{{(!isset($prev_value->cost_description)&&!isset($prev_data[$var_name]))?'':'red'}}">
                    {{isset($ex_value->cost_description)?$ex_value->cost_description:''}}
                </span>
                        @endif

                    </td>
                </tr>

                <tr>
                    <td width="50%">

                        Value

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_value !== $prev_value->cost_value )
                                ${{$prev_value->cost_value}}
                                <span
                                    class="red">
                        ${{$ex_value->cost_value}}
                    </span>
                            @else
                                ${{$ex_value->cost_value}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_value)?'':'removed'}}">
                    {{isset($prev_value->cost_value)?'$'.$prev_value->cost_value:''}}
                </span>
                            <span class="{{(!isset($prev_value->cost_value)&&!isset($prev_data[$var_name]))?'':'red'}}">
                    {{isset($ex_value->cost_value)?'$'.$ex_value->cost_value:''}}
                </span>
                        @endif
                    </td>
                </tr>
            @endfor
        @endif


        <tr>
            <td width="50%">

                Any off-duty training expenses for ammo, shooting range fees, maybe even setting up your own shooting
                range?


            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @php
                        $isEmergency =  count($emergency_personal_exp->travel)>0;
                        $isEmergencyPrew =  count($prev_data[$var_name]->travel)>0;
                    @endphp
                    @if($isEmergency !== $isEmergencyPrew)
                        {{$isEmergencyPrew?'Yes':'No'}}
                        <span class="red">{{$isEmergency?'Yes':'No'}}</span>
                    @else
                        {{$isEmergency?'Yes':'No'}}
                    @endif
                @else
                    {{$isEmergency?'Yes':'No'}}
                @endif


            </td>
        </tr>
        @if(count($emergency_personal_exp->travel)>0)
            @php
                $prop_child = $emergency_personal_exp->travel;
            @endphp
            @if(isset($prev_data[$var_name]))
                @php
                    $prev_prop_child =$prev_data[$var_name]->travel;
                @endphp
                @if(count($prop_child)>count($prev_prop_child))
                    @php
                        $size = count($prop_child);
                    @endphp
                @else
                    @php
                        $size =count($prev_prop_child);
                    @endphp
                @endif
            @else
                @php
                    $size = count($prop_child);
                @endphp
            @endif
            @for($i=0;$i<$size;$i++)
                @php
                    $ex_value = isset($prop_child[$i])?$prop_child[$i]:null;
                    $prev_value = isset($prev_prop_child[$i])?$prev_prop_child[$i]:null;
                @endphp
                <tr>
                    <td width="50%">

                        Description

                    </td>
                    <td width="50%">
                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_description !== $prev_value->cost_description )
                                {{$prev_value->cost_description}}
                                <span
                                    class="red">
                        {{$ex_value->cost_description}}
                    </span>
                            @else
                                {{$ex_value->cost_description}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_description)?'':'removed'}}">
                    {{isset($prev_value->cost_description)?$prev_value->cost_description:''}}
                </span>
                            {{--                        Correct condition--}}
                            <span
                                class="{{(!isset($prev_value->cost_description)&&!isset($prev_data[$var_name]))?'':'red'}}">
                    {{isset($ex_value->cost_description)?$ex_value->cost_description:''}}
                </span>
                        @endif

                    </td>
                </tr>

                <tr>
                    <td width="50%">

                        Value

                    </td>
                    <td width="50%">

                        @if(isset($prev_value) && isset($ex_value))
                            @if($ex_value->cost_value !== $prev_value->cost_value )
                                ${{$prev_value->cost_value}}
                                <span
                                    class="red">
                        ${{$ex_value->cost_value}}
                    </span>
                            @else
                                ${{$ex_value->cost_value}}
                            @endif
                        @else
                            <span class="{{isset($ex_value->cost_value)?'':'removed'}}">
                    {{isset($prev_value->cost_value)?'$'.$prev_value->cost_value:''}}
                </span>
                            <span class="{{(!isset($prev_value->cost_value)&&!isset($prev_data[$var_name]))?'':'red'}}">
                    {{isset($ex_value->cost_value)?'$'.$ex_value->cost_value:''}}
                </span>
                        @endif
                    </td>
                </tr>
            @endfor
        @endif
    @endif
    @if(isset($pocket_medical))
        @php
            $var_name='pocket_medical';
        @endphp
        <tr>
            <th colspan="2">Out of Pocket Medical Expenses</th>
        </tr>
        <tr>
            <td width="50%">

                Insurance Premiums

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($pocket_medical->insurance !== $prev_data[$var_name]->insurance)
                        ${{$prev_data[$var_name]->insurance}}
                        <span class="red"> ${{$pocket_medical->insurance}}</span>
                    @else
                        ${{$pocket_medical->insurance}}
                    @endif
                @else
                    ${{$pocket_medical->insurance}}
                @endif


            </td>
        </tr>

        <tr>
            <td width="50%">

                Insurance Premiums (Non-Working Members)

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($pocket_medical->insurance_non !== $prev_data[$var_name]->insurance_non)
                        ${{$prev_data[$var_name]->insurance_non}}
                        <span class="red"> ${{$pocket_medical->insurance_non}}</span>
                    @else
                        ${{$pocket_medical->insurance_non}}
                    @endif
                @else
                    ${{$pocket_medical->insurance_non}}
                @endif


            </td>
        </tr>

        <tr>
            <td width="50%">

                Did you have any out-of-pocket expenses for glasses,
                contacts, or eye exams in 2019?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($pocket_medical->out_of_pocket_radio !== $prev_data[$var_name]->out_of_pocket_radio)
                        {{$prev_data[$var_name]->out_of_pocket_radio?'Yes':'No'}}
                        <span class="red">{{$pocket_medical->out_of_pocket_radio?'Yes':'No'}}</span>
                    @else
                        {{$pocket_medical->out_of_pocket_radio?'Yes':'No'}}
                    @endif
                @else
                    {{$pocket_medical->out_of_pocket_radio?'Yes':'No'}}
                @endif


            </td>
        </tr>
        @if($pocket_medical->out_of_pocket_radio)
            <tr>
                <td width="50%">

                    Out-of-pocket Vision

                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))
                        @if($pocket_medical->out_of_pocket !== $prev_data[$var_name]->out_of_pocket)
                            ${{$prev_data[$var_name]->out_of_pocket}}
                            <span class="red"> ${{$pocket_medical->out_of_pocket}}</span>
                        @else
                            ${{$pocket_medical->out_of_pocket}}
                        @endif
                    @else
                        ${{$pocket_medical->out_of_pocket}}
                    @endif


                </td>
            </tr>

            <tr>
                <td width="50%">

                    Out of Pocket Vision (Non-Working Members)

                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))
                        @if($pocket_medical->out_of_pocket_non !== $prev_data[$var_name]->out_of_pocket_non)
                            ${{$prev_data[$var_name]->out_of_pocket_non}}
                            <span class="red"> ${{$pocket_medical->out_of_pocket_non}}</span>
                        @else
                            ${{$pocket_medical->out_of_pocket_non}}
                        @endif
                    @else
                        ${{$pocket_medical->out_of_pocket_non}}
                    @endif


                </td>
            </tr>
        @endif
    @endif
    @if(isset($pocket_medical_two))
        @php
            $var_name='pocket_medical_two';
        @endphp
        <tr>
            <td width="50%">

                Did you have any out-of-pocket expenses for
                prescriptions?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($pocket_medical_two->out_of_pocket_medications_radio !== $prev_data[$var_name]->out_of_pocket_medications_radio)
                        {{$prev_data[$var_name]->out_of_pocket_medications_radio?'Yes':'No'}}
                        <span class="red">{{$pocket_medical_two->out_of_pocket_medications_radio?'Yes':'No'}}</span>
                    @else
                        {{$pocket_medical_two->out_of_pocket_medications_radio?'Yes':'No'}}
                    @endif
                @else
                    {{$pocket_medical_two->out_of_pocket_medications_radio?'Yes':'No'}}
                @endif

            </td>
        </tr>
        @if($pocket_medical_two->out_of_pocket_medications_radio)
            <tr>
                <td width="50%">

                    Out-of-pocket Medications

                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))
                        @if($pocket_medical_two->out_of_pocket_medications !== $prev_data[$var_name]->out_of_pocket_medications)
                            ${{$prev_data[$var_name]->out_of_pocket_medications}}
                            <span class="red"> ${{$pocket_medical_two->out_of_pocket_medications}}</span>
                        @else
                            ${{$pocket_medical_two->out_of_pocket_medications}}
                        @endif
                    @else
                        ${{$pocket_medical_two->out_of_pocket_medications}}
                    @endif

                </td>
            </tr>
        @endif
        <tr>
            <td width="50%">

                Did you have any out-of-pocket expenses for dental
                check-ups or any visits at all to the dentist in 2019?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($pocket_medical_two->out_of_pocket_dental_radio !== $prev_data[$var_name]->out_of_pocket_dental_radio)
                        {{$prev_data[$var_name]->out_of_pocket_dental_radio?'Yes':'No'}}
                        <span class="red">{{$pocket_medical_two->out_of_pocket_dental_radio?'Yes':'No'}}</span>
                    @else
                        {{$pocket_medical_two->out_of_pocket_dental_radio?'Yes':'No'}}
                    @endif
                @else
                    {{$pocket_medical_two->out_of_pocket_dental_radio?'Yes':'No'}}
                @endif


            </td>
        </tr>
        @if($pocket_medical_two->out_of_pocket_dental_radio)

            <tr>
                <td width="50%">

                    Out-of-pocket Dental

                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))
                        @if($pocket_medical_two->out_of_pocket_dental !== $prev_data[$var_name]->out_of_pocket_dental)
                            ${{$prev_data[$var_name]->out_of_pocket_dental}}
                            <span class="red"> ${{$pocket_medical_two->out_of_pocket_dental}}</span>
                        @else
                            ${{$pocket_medical_two->out_of_pocket_dental}}
                        @endif
                    @else
                        ${{$pocket_medical_two->out_of_pocket_dental}}
                    @endif


                </td>
            </tr>

        @endif
        <tr>
            <td width="50%">

                Did you have any out-of-pocket expenses for any routine
                checkups, injuries, or surgeries?

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($pocket_medical_two->out_of_pocket_other_radio !== $prev_data[$var_name]->out_of_pocket_other_radio)
                        {{$prev_data[$var_name]->out_of_pocket_other_radio?'Yes':'No'}}
                        <span class="red">{{$pocket_medical_two->out_of_pocket_other_radio?'Yes':'No'}}</span>
                    @else
                        {{$pocket_medical_two->out_of_pocket_other_radio?'Yes':'No'}}
                    @endif
                @else
                    {{$pocket_medical_two->out_of_pocket_other_radio?'Yes':'No'}}
                @endif


            </td>
        </tr>
        @if($pocket_medical_two->out_of_pocket_other_radio)

            <tr>
                <td width="50%">
                    Out-of-pocket Other Medical
                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))
                        @if($pocket_medical_two->out_of_pocket_other !== $prev_data[$var_name]->out_of_pocket_other)
                            ${{$prev_data[$var_name]->out_of_pocket_other}}
                            <span class="red"> ${{$pocket_medical_two->out_of_pocket_other}}</span>
                        @else
                            ${{$pocket_medical_two->out_of_pocket_other}}
                        @endif
                    @else
                        ${{$pocket_medical_two->out_of_pocket_other}}
                    @endif
                </td>
            </tr>
        @endif
    @endif
    @if(isset($deposit_infromation))
        @php
            $var_name='deposit_infromation';
        @endphp
        <tr>
            <th colspan="2">Deposit Information</th>
        </tr>
        <tr>
            <td width="50%">
                If you are due a refund, would you like a direct deposit? (If your bank information is not provided
                below, your refund will be mailed by check.)
            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($deposit_infromation->refund_deposit !== $prev_data[$var_name]->refund_deposit)
                        {{$prev_data[$var_name]->refund_deposit?'Yes':'No'}}
                        <span class="red">{{$deposit_infromation->refund_deposit?'Yes':'No'}}</span>
                    @else
                        {{$deposit_infromation->refund_deposit?'Yes':'No'}}
                    @endif
                @else
                    {{$deposit_infromation->refund_deposit?'Yes':'No'}}
                @endif
            </td>
        </tr>
        <tr>
            <td width="50%">
                If you are due a refund, would you like information on how to split your refund between accounts?
            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($deposit_infromation->refund_between_acc !== $prev_data[$var_name]->refund_between_acc)
                        {{$prev_data[$var_name]->refund_between_acc?'Yes':'No'}}
                        <span class="red">{{$deposit_infromation->refund_between_acc?'Yes':'No'}}</span>
                    @else
                        {{$deposit_infromation->refund_between_acc?'Yes':'No'}}
                    @endif
                @else
                    {{$deposit_infromation->refund_between_acc?'Yes':'No'}}
                @endif
            </td>
        </tr>

        <tr>
            <td width="50%">
                Bank Routing / ABA Number (U.S. Only)
            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($deposit_infromation->bank_routing !== $prev_data[$var_name]->bank_routing)
                        {{$prev_data[$var_name]->bank_routing}}
                        <span class="red">{{$deposit_infromation->bank_routing}}</span>
                    @else
                        {{$deposit_infromation->bank_routing}}
                    @endif
                @else
                    {{$deposit_infromation->bank_routing}}
                @endif

            </td>
        </tr>
        <tr>
            <td width="50%">
                Bank Name
            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($deposit_infromation->bank_name !== $prev_data[$var_name]->bank_name)
                        {{$prev_data[$var_name]->bank_name}}
                        <span class="red">{{$deposit_infromation->bank_name}}</span>
                    @else
                        {{$deposit_infromation->bank_name}}
                    @endif
                @else
                    {{$deposit_infromation->bank_name}}
                @endif
            </td>
        </tr>

        <tr>
            <td width="50%">
                Account Number or, if applicable, IBAN
            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($deposit_infromation->account_number !== $prev_data[$var_name]->account_number)
                        {{$prev_data[$var_name]->account_number}}
                        <span class="red">{{$deposit_infromation->account_number}}</span>
                    @else
                        {{$deposit_infromation->account_number}}
                    @endif
                @else
                    {{$deposit_infromation->account_number}}
                @endif
            </td>
        </tr>

        <tr>
            <td width="50%">
                Account Owner Name
            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($deposit_infromation->account_owner !== $prev_data[$var_name]->account_owner)
                        {{$prev_data[$var_name]->account_owner}}
                        <span class="red">{{$deposit_infromation->account_owner}}</span>
                    @else
                        {{$deposit_infromation->account_owner}}
                    @endif
                @else
                    {{$deposit_infromation->account_owner}}
                @endif
            </td>
        </tr>
        <tr>
            <td width="50%">
                Secondary Bank Account (Split Refund)
            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($deposit_infromation->secondary_bank !== $prev_data[$var_name]->secondary_bank)
                        {{$prev_data[$var_name]->secondary_bank}}
                        <span class="red">{{$deposit_infromation->secondary_bank}}</span>
                    @else
                        {{$deposit_infromation->secondary_bank}}
                    @endif
                @else
                    {{$deposit_infromation->secondary_bank}}
                @endif
            </td>
        </tr>
        <tr>
            <td width="50%">
                Secondary Account Owner Name
            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($deposit_infromation->secondary_account_owner !== $prev_data[$var_name]->secondary_account_owner)
                        {{$prev_data[$var_name]->secondary_account_owner}}
                        <span class="red">{{$deposit_infromation->secondary_account_owner}}</span>
                    @else
                        {{$deposit_infromation->secondary_account_owner}}
                    @endif
                @else
                    {{$deposit_infromation->secondary_account_owner}}
                @endif
            </td>
        </tr>
        <tr>
            <td width="50%">
                Mailing Address of Financial Institution
            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($deposit_infromation->mailing_address !== $prev_data[$var_name]->mailing_address)
                        {{$prev_data[$var_name]->mailing_address}}
                        <span class="red">{{$deposit_infromation->mailing_address}}</span>
                    @else
                        {{$deposit_infromation->mailing_address}}
                    @endif
                @else
                    {{$deposit_infromation->mailing_address}}
                @endif
            </td>
        </tr>
        <tr>
            <td width="50%">
                SWIFT / BIC Code
            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($deposit_infromation->swift !== $prev_data[$var_name]->swift)
                        {{$prev_data[$var_name]->swift}}
                        <span class="red">{{$deposit_infromation->swift}}</span>
                    @else
                        {{$deposit_infromation->swift}}
                    @endif
                @else
                    {{$deposit_infromation->swift}}
                @endif
            </td>
        </tr>
        <tr>
            <td width="50%">
                Bank State Branch (BSB) Number
            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($deposit_infromation->bsb_number !== $prev_data[$var_name]->bsb_number)
                        {{$prev_data[$var_name]->bsb_number}}
                        <span class="red">{{$deposit_infromation->bsb_number}}</span>
                    @else
                        {{$deposit_infromation->bsb_number}}
                    @endif
                @else
                    {{$deposit_infromation->bsb_number}}
                @endif
            </td>
        </tr>
        <tr>
            <td width="50%">
                Check mailed to the address of record

            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($deposit_infromation->address_record !== $prev_data[$var_name]->address_record)
                        {{$prev_data[$var_name]->address_record?'Yes':'No'}}
                        <span class="red">{{$deposit_infromation->address_record?'Yes':'No'}}</span>
                    @else
                        {{$deposit_infromation->address_record?'Yes':'No'}}
                    @endif
                @else
                    {{$deposit_infromation->address_record?'Yes':'No'}}
                @endif
            </td>
        </tr>
        <tr>
            <td width="50%">
                Check mailed to you at an address other than your record address


            </td>
            <td width="50%">
                @if(isset($prev_data[$var_name]))
                    @if($deposit_infromation->address_record_than !== $prev_data[$var_name]->address_record_than)
                        {{$prev_data[$var_name]->address_record_than?'Yes':'No'}}
                        <span class="red">{{$deposit_infromation->address_record_than?'Yes':'No'}}</span>
                    @else
                        {{$deposit_infromation->address_record_than?'Yes':'No'}}
                    @endif
                @else
                    {{$deposit_infromation->address_record_than?'Yes':'No'}}
                @endif
            </td>
        </tr>
        @if(isset($deposit_infromation->address_record_than))
            <tr>
                <td width="50%">
                    Address

                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))
                        @if($deposit_infromation->address !== $prev_data[$var_name]->address)
                            {{$prev_data[$var_name]->address}}
                            <span class="red">{{$deposit_infromation->address}}</span>
                        @else
                            {{$deposit_infromation->address}}
                        @endif
                    @else
                        {{$deposit_infromation->address}}
                    @endif
                </td>
            </tr>
            <tr>
                <td width="50%">
                    City

                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))
                        @if($deposit_infromation->city !== $prev_data[$var_name]->city)
                            {{$prev_data[$var_name]->city}}
                            <span class="red">{{$deposit_infromation->city}}</span>
                        @else
                            {{$deposit_infromation->city}}
                        @endif
                    @else
                        {{$deposit_infromation->city}}
                    @endif
                </td>
            </tr>
            <tr>
                <td width="50%">
                    State or Province

                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))
                        @if($deposit_infromation->state !== $prev_data[$var_name]->state)
                            {{$prev_data[$var_name]->state}}
                            <span class="red">{{$deposit_infromation->state}}</span>
                        @else
                            {{$deposit_infromation->state}}
                        @endif
                    @else
                        {{$deposit_infromation->state}}
                    @endif
                </td>
            </tr>
            <tr>
                <td width="50%">
                    Postal Code
                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))
                        @if($deposit_infromation->postal_code !== $prev_data[$var_name]->postal_code)
                            {{$prev_data[$var_name]->postal_code}}
                            <span class="red">{{$deposit_infromation->postal_code}}</span>
                        @else
                            {{$deposit_infromation->postal_code}}
                        @endif
                    @else
                        {{$deposit_infromation->postal_code}}
                    @endif
                </td>
            </tr>
            <tr>
                <td width="50%">
                    Country
                </td>
                <td width="50%">
                    @if(isset($prev_data[$var_name]))
                        @if($deposit_infromation->country !== $prev_data[$var_name]->country)
                            {{$prev_data[$var_name]->country}}
                            <span class="red">{{$deposit_infromation->country}}</span>
                        @else
                            {{$deposit_infromation->country}}
                        @endif
                    @else
                        {{$deposit_infromation->country}}
                    @endif
                </td>
            </tr>
        @endif
    @endif
    </tbody>
</table>

</body>

</html>
