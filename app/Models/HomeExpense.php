<?php

namespace App\Models;

class HomeExpense extends ChildModel
{
    protected $fillable = ['hm_own_rent', 'hm_mortage_interest',
        'hm_real_estate_tax_checkbox', 'hm_energy_efficient_upgrades_radio',
        'hm_real_estate_tax', 'hm_mortgage_insurance', 'hm_loan_mandated_homeowner_ins', 'hm_energy_efficient_total_amount',
        'created_by', 'session_id'];
    protected $childModel = HomeExpenseChild::class;

    protected $parentKey = 'home_expense_id';
    protected $appends = ['expenses'];

    public function expenses()
    {
        return $this->hasMany($this->childModel, $this->parentKey);
    }

    public function getExpensesAttribute()
    {
        return $this->expenses()->get();
    }
}
