<?php

namespace App\Models;

class BlueCollarExpTwo extends MultiChildModel
{
    protected $fillable = ['expenses_uniforms_radio', 'work_clothing_radio', 'dry_cleaning_radio', 'created_by',
        'session_id'];
    protected $childModel = BlueCollarExpTwoChild::class;
    protected $parentKey = 'blue_collar_two_id';

    protected $appends = ['uniform', 'clothing','cleaning'];

    public function expenses()
    {
        return $this->hasMany($this->childModel, $this->parentKey);
    }

    public function getUniformAttribute()
    {
        return $this->expenses()->where(['type' => 'expenses_uniforms'])->get();
    }

    public function getClothingAttribute()
    {
        return $this->expenses()->where(['type' => 'work_clothing'])->get();
    }

    public function getCleaningAttribute()
    {
        return $this->expenses()->where(['type' => 'dry_cleaning'])->get();
    }
}
