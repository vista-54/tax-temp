<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MultiChildModel extends Model
{
    protected $childModel = Model::class;
    protected $parentKey;

    public function addExpenseChild($dependants, $type = null)
    {
        $result = [];
        $paramsArr =
            [
                $this->parentKey => $this->id,
                'session_id' => $this->session_id,
                'created_by' => $this->created_by
            ];
        if ($type) {
            $paramsArr['type'] = $type;
        }
        foreach ($dependants as $item) {
            $res = $this->childModel::insert(array_merge($paramsArr, $item));
            $result[] = $res;
        }
        return $result;
    }

    public function removeAllChild()
    {
        $this->childModel::where([$this->parentKey => $this->id])->delete();
    }
}
