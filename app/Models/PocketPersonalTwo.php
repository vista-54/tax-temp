<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PocketPersonalTwo extends Model
{
    protected $fillable = ['out_of_pocket_medications_radio',
        'out_of_pocket_medications','out_of_pocket_dental_radio','out_of_pocket_dental',
        'out_of_pocket_other_radio','out_of_pocket_other','created_by','session_id'];
}
