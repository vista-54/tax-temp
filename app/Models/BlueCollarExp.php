<?php

namespace App\Models;

class BlueCollarExp extends MultiChildModel
{
    protected $fillable = ['expenses_equipment_radio', 'purchasing_machinery_radio', 'created_by', 'session_id'];
    protected $childModel = BlueCollarExpChild::class;
    protected $parentKey = 'blue_collar_id';

    protected $appends = ['equipment', 'machinery'];

    public function expenses()
    {
        return $this->hasMany($this->childModel, $this->parentKey);
    }

    public function getEquipmentAttribute()
    {
        return $this->expenses()->where(['type' => 'expenses_equipment'])->get();
    }

    public function getMachineryAttribute()
    {
        return $this->expenses()->where(['type' => 'purchasing_machinery'])->get();
    }
}
