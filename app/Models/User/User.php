<?php

namespace App\Models\User;

use App\Models\UserSession;
use App\Models\Address\Address;
// use App\Models\UserSession;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable implements JWTSubject
{
    use HasRoles;
    use Notifiable;
    use SoftDeletes;

    protected $guard_name = 'api';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'phone', 'email', 'country_code', 'password', 'status', 'gender', 'timezone', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = ['report'];

    public function report()
    {
        return UserSession::where(['user_id' => $this->id, 'status' => UserSession::STATUS_INACTIVE])->orderBy('id', 'DESC')->first();
    }

    public function getReportAttribute()
    {
        return $this->report();
    }


    public function addresses()
    {
        return $this->hasMany(Address::calss);
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public static function saveUser($data_arr = [])
    {
        $user = User::create($data_arr);

        $addresses = $data_arr['addresses'];
        Address::where('user_id', $user->id)->delete();
        if ($addresses) {

            $savedAddresses = [];
            foreach ($addresses as $idx) {
                $saved = Address::create($idx);
                $savedAddresses[] = $saved;
            }

            $user->addresses = $savedAddresses;
        }
        return $user;
    }

    public static function searchUsers($request)
    {


        $name = $request->input('name');
        $email = $request->input('email');
        $sort = $request->input('sort') ? $request->input('sort') : 'desc';
        $sort_field = $request->input('sort_field') ? $request->input('sort_field') : 'id';


        $users = User::query();
        $users = $users->when($name, function ($query, $name) {
            return $query->where('first_name', 'like', '%' . $name . '%');
        });
        $users = $users->when($name, function ($query, $name) {
            return $query->where('last_name', 'like', '%' . $name . '%');
        });
        $users = $users->when($name, function ($query, $name) {
            return $query->orwhereRAW("Concat(first_name,' ',last_name) LIKE '%{$name}%'");
        });
        $users = $users->when($email, function ($query, $email) {
            return $query->where('email', 'like', '%' . $email . '%');
        });
        if ($sort_field === 'report') {
            $sort_field = 'updated_at';
        }
        return $users->orderby('users.' . $sort_field, $sort);
    }

    public static function changeStatus($request)
    {

        $id = $request->input('id');
        $status = $request->input('status');
        $user = User::find($id);
        if ($status == 0) {
            $user->status = 1;
        } elseif ($status == 1) {
            $user->status = 0;
        } else {
            $user->status = $user->status;
        }

        $user->save();
        return $user;
    }
}
