<?php

namespace App\Models;

class EmployeeBusinessExpOne extends MultiChildModel

{
    protected $fillable = ['et_office_furniture_description_radio',
        'et_office_equipment_description_radio', 'et_seeking_expense_description_radio', 'created_by', 'session_id'];

    protected $childModel = OfficePurchase::class;
    protected $parentKey = 'emp_bus_one_id';

    protected $appends = ['furniture', 'equipment', 'seeking'];

    public function expenses()
    {
        return $this->hasMany($this->childModel, $this->parentKey);
    }

    public function getFurnitureAttribute()
    {
        return $this->expenses()->where(['type' => 'office_furniture'])->get();
    }

    public function getEquipmentAttribute()
    {
        return $this->expenses()->where(['type' => 'office_equipment'])->get();
    }

    public function getSeekingAttribute()
    {
        return $this->expenses()->where(['type' => 'seeking_expense'])->get();
    }

}
