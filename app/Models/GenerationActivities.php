<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenerationActivities extends Model
{
    protected $fillable=['income_expense', 'created_by', 'session_id'];

}
