<?php

namespace App\Models\Document;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;

class Document extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','path', 'type', 'created_by', 'updated_by','status'
    ];

    public static function updateDocument($path = Null, $name = Null, $type = Null){

        $document = Document::where(['type' => $type,'status' => 0 ,'finished' => 0, 'created_by' => Auth::id()])->first();
        if($document){
            $oldFile = $document->name;
            Storage::delete($oldFile);
            $document->update([
                'updated_by' => Auth::id(),
                'updated_at' => now(),
                'path' => $path,
                'name' => $name
            ]);
        }else{
            $document = Document::create([
                'name' => $name,
                'type' => $type,
                'path' => $path,
                'created_at' => now(),
                'created_by' => Auth::id()
            ]);
        }

        return $document;
    }
    public static function changeStatus($request){

        $id = $request->input('id');
        $status = $request->input('status');
        $document = Document::find($id);
        if($status == 0) {
            $document->status = 1;
        }
        elseif ($status == 1)
        {
            $document->status = 0;
        }else{
            $document->status = $document->status;
        }
        
        $document->save();
        return $document;
    }
}
