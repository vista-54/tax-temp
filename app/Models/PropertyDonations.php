<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyDonations extends Model
{
    protected $fillable = ['pd_checkbox', 'created_by', 'session_id'];
    protected $appends = ['donations'];

    public function donations()
    {
        return $this->hasMany(PropertyDonationsChild::class,'property_donation_id');
    }

    public function getDonationsAttribute()
    {
        return $this->donations()->get();
    }

    public function addDonationsChild($dependants)
    {
        $result = [];
        $this->removeAllChild();
        foreach ($dependants as $item) {
            $res = PropertyDonationsChild::insert(array_merge(['property_donation_id' => $this->id,
                'session_id' => $this->session_id,
                'created_by' => $this->created_by], $item));
            $result[] = $res;
        }
        return $result;
    }

    public function removeAllChild()
    {
        PropertyDonationsChild::where(['property_donation_id' => $this->id])->delete();
    }
}
