<?php
/**
 * Created by PhpStorm.
 * User: vista
 * Date: 24.02.2020
 * Time: 14:12
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ChildModel extends Model
{

    protected $childModel = Model::class;
    protected $parentKey;

    public function addExpenseChild($dependants)
    {
        $result = [];
        $this->removeAllChild();
        foreach ($dependants as $item) {
            $res = $this->childModel::insert(array_merge([$this->parentKey => $this->id,
                'session_id' => $this->session_id,
                'created_by' => $this->created_by], $item));
            $result[] = $res;
        }
        return $result;
    }

    public function removeAllChild()
    {
        $this->childModel::where([$this->parentKey => $this->id])->delete();
    }
}