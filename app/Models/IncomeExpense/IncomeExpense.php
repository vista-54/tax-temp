<?php

namespace App\Models\IncomeExpense;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IncomeExpense extends Model 
{

    protected $table = 'income_expenses';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = [
        'user_id', 'fbar_outside_money', 'rpi_rental_home', 'siga_icome_expense', 'cd_cash_contribution_checkbox', 'cd_frequency_type_contribution', 'cd_frequency_cost_contribution', 'pd_checkbox', 've_volunteering_checkbox', 'se_enrolled_in_college_checkbox', 'se_loan_checkbox', 'fm_add_description_checkbox', 'fm_adoption_cost_checkbox', 'hm_own_rent', 'hm_mortage_interest', 'hm_real_estate_tax_checkbox', 'hm_real_estate_tax', 'hm_mortgage_insurance', 'hm_loan_mandated_homeowner_ins', 'hm_energy_efficient_upgrades_radio', 'hm_energy_efficient_total_amount', 'sii_traditional_ira_contributions_radio', 'sii_traditional_ira_contributions', 'sii_roth_ira_contributions', 'are_you_teacher_radio', 'sii_educator_expenses_radio', 'sii_educator_expenses', 'sii_self_employed_radio', 'sii_total_mortgage_payments', 'sii_total_hoa', 'sii_total_utilities'
    ];

}