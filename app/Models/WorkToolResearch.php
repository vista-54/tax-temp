<?php

namespace App\Models;

use App\Models\ChildModel;

class WorkToolResearch extends ChildModel
{
    protected $fillable = ['research_radio', 'internet_pay', 'created_by', 'session_id'];
    protected $parentKey = 'work_tool_researches_id';
    protected $childModel = WorkToolResearchChild::class;

    protected $appends = ['expenses'];

    public function expenses()
    {
        return $this->hasMany($this->childModel, $this->parentKey);
    }

    public function getExpensesAttribute()
    {
        return $this->expenses()->get();
    }
}
