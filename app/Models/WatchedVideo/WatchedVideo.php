<?php

namespace App\Models\WatchedVideo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WatchedVideo extends Model 
{

    protected $table = 'watched_videos';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = [
        'annual_tax_video', 'cohan_rule_video'
    ];

}