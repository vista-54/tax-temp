<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class PersonalInformation extends Model
{
    protected $fillable = ['first_name', 'middle_name', 'last_name', 'email', 'phone', 'date_of_birth',
        'job_title', 'address', 'citizenship_status', 'visa_type','country_code',
        'citizenship_other', 'marital_status', 'active_military', 'active_military_state', 'created_by', 'updated_by', 'session_id','zip','social_security'];
//
//    public static function boot()
//    {
//        parent::boot();
//        $userId = Auth::user()->id;
//
//        self::creating(function ($model) use ($userId) {
//            $session = UserSession::where(['user_id' => $userId, 'status' => UserSession::STATUS_ACTIVE])->first();
//            $model->session_id = $session->id;
//            $model->created_by = $userId;
//        });
//        self::updating(function ($model) use ($userId) {
//            $session = UserSession::where(['user_id' => $userId, 'status' => UserSession::STATUS_ACTIVE])->first();
//            $model->session_id = $session->id;
//            $model->updated_by = $userId;
//        });
//    }
}
