<?php

namespace App\Models\EmployeeBusinessExpense;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeBusinessExpense extends Model 
{

    protected $table = 'employee_business_expenses';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = [
        'ebe_total_parking_fees_radio', 'ebe_tollway_costs_radio', 'ebe_total_local_transportation_radio', 'ebe_total_travel_costs_radio', 'ebe_total_parking_fees', 'ebe_tollway_costs', 'ebe_total_local_transportation', 'ebe_total_travel_cost', 've_make', 've_model', 've_price', 've_spouse_make', 've_spouse_model', 've_spouse_price', 'eve_weekly_estimated_total', 'eve_spouse_weekly_estimated_total', 'eve_no_oil_change', 'eve_cost_per_change', 'eve_spouse_no_oil_change', 'eve_spouse_cost_per_change', 'eve_total_tire_costs_radio', 'eve_total_tire_cost', 'eve_spouse_total_tire_cost', 'eve_total_repairs_cost_radio', 'eve_total_repairs_cost', 'eve_spouse_total_repairs_cost', 'eves_total_auto_insurance', 'eves_inspection_total_costs_radio', 'eves_inspection_total_cost', 'eves_spouse_inspection_total_cost', 'eme_time_per_week_radio', 'eme_time_per_week', 'eme_meal_cost', 'eme_spouse_time_per_week', 'eme_spouse_meal_cost', 'eme_gift_description_radio', 'eme_gift_description', 'eme_gift_cost', 'et_office_furniture_description_radio', 'et_office_equipment_description_radio', 'et_seeking_expense_description_radio', 'ets_monthly_cell_phone_bill', 'ets_spouse_monthly_cell_phone_bill', 'ets_networking_expenses_radio', 'ett_exam_fees_description_radio', 'ett_professional_development_radio', 'ett_professional_development', 'ett_spouse_professional_development', 'ett_membership_description_radio', 'etf_subscription_description_radio', 'etf_monthly_Internet_bill', 'ebc_equipment_expenses_description_radio', 'ebc_machinery_description_radio', 'ebcs_uniform_expense_description_radio', 'ebcs_cloting_expense_description_radio', 'ebcs_dry_cleaning_expens_description_radio', 'eps_military', 'ep_purchasing_description_radio', 'me_insurance_premiums', 'me_insurance_premiums_non_working', 'me_out_of_pocket_vision_expense_radio', 'me_out_of_pocket_vision_expense', 'me_out_of_pocket_vision_expense_non_working', 'mes_out_of_pocket_medications_radio', 'mes_out_of_pocket_medications', 'mes_out_of_pocket_dental_radio', 'mes_out_of_pocket_dental', 'mes_out_of_pocket_other_Medical_radio', 'mes_out_of_pocket_other_medical'
    ];

}