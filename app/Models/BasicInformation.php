<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BasicInformation extends Model
{
    protected $fillable = ['county', 'city', 'state', 'school', 'created_by', 'session_id'];
}
