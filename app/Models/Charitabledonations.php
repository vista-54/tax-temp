<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Charitabledonations extends ChildModel
{
    protected $fillable=['make_cache','created_by','session_id'];
    protected $childModel = CharitableDonationsChild::class;
    protected $parentKey = 'charitabledonations_id';


    protected $appends = ['expenses'];

    public function expenses()
    {
        return $this->hasMany($this->childModel, $this->parentKey);
    }

    public function getExpensesAttribute()
    {
        return $this->expenses()->get();
    }
}
