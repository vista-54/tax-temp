<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VehicleMaintenance extends Model
{
    protected $fillable = ['eve_weekly_estimated_total', 'eve_spouse_weekly_estimated_total',
        'eve_no_Oil_Changes', 'eve_Cost_Per_Change', 'eve_spouse_no_Oil_Changes',
        'eve_spouse_Cost_Per_Change', 'eve_Total_Tire_Costs_radio', 'eve_Total_Tire_Costs', 'eve_spouse_Total_Tire_Costs',
        'eve_Total_Repairs_Cost_radio', 'eve_Total_Repairs_Cost', 'eve_spouse_Total_Repairs_Cost','created_by','session_id'];
}
