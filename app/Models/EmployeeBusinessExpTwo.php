<?php

namespace App\Models;

class EmployeeBusinessExpTwo extends ChildModel
{
    protected $fillable = ['ets_Networking_Expenses_radio',
        'ets_education_expenses_description_checkbox', 'ets_Monthly_Cell_Phone_Bill',
        'ets_Spouse_Monthly_Cell_Phone_Bill', 'ets_Networking_Expenses', 'ets_spouse_Networking_Expenses', 'created_by',
        'session_id'];
    protected $childModel = CertificationExpense::class;
    protected $parentKey = 'emp_bus_two_id';

    protected $appends = ['expenses'];

    public function expenses()
    {
        return $this->hasMany($this->childModel, $this->parentKey);
    }

    public function getExpensesAttribute()
    {
        return $this->expenses()->get();
    }
}
