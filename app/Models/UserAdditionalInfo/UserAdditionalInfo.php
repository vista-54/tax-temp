<?php

namespace App\Models\UserAdditionalInfo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserAdditionalInfo extends Model 
{

    protected $table = 'user_additional_info';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = [
        'us_tax_residency','us_tax_residency_spouse', 'plan_to_change_immigration', 'plan_to_depart', 'estate_plan_in_place','session_id','created_by',
    ];

}