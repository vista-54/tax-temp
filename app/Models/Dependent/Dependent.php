<?php

namespace App\Models\Dependent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Dependent extends Model 
{

    protected $table = 'dependents';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = [
        'di_name', 'di_dob', 'di_relation', 'di_days_lived', 'di_itin', 'di_married_as', 'di_fulltime_student', 'di_received_income'
    ];

}