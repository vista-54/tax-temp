<?php

namespace App\Models;

class WorkToolExam extends MultiChildModel
{
    protected $fillable = [
        'professional_fees_radio',
        'professional_dev_radio',
        'professional_dev',
        'professional_dev_spouse',
        'membership_fees_radio',
        'session_id', 'created_by'];
    protected $childModel = WorkToolExamChild::class;
    protected $parentKey = 'work_tool_exams_id';

    protected $appends = ['professional', 'membership'];

    public function expenses()
    {
        return $this->hasMany($this->childModel, $this->parentKey);
    }

    public function getProfessionalAttribute()
    {
        return $this->expenses()->where(['type' => 'professional_fees'])->get();
    }

    public function getMembershipAttribute()
    {
        return $this->expenses()->where(['type' => 'membership_fees'])->get();
    }

}
