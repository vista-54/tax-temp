<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DepositInfromation extends Model
{
    //
    protected $fillable = ['refund_deposit','refund_between_acc','bank_routing','bank_name','account_number','account_owner','secondary_bank','secondary_account_owner',
    'mailing_address','swift','bsb_number','address_record','address_record_than','address','city','state','postal_code','country','created_by', 'created_by', 'session_id'];

}
