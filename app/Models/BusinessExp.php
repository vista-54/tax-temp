<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BusinessExp extends MultiChildModel
{
    protected $fillable = ['ebe_total_parking_fees_radio', 'ebe_total_parking_fees',
        'ebe_tollway_costs_radio', 'ebe_tollway_costs', 'ebe_total_local_transportation_radio', 'ebe_total_travel_costs_radio',
        'created_by', 'session_id'];

    protected $childModel = BusinessExpChild::class;
    protected $parentKey = 'business_exp_id';

    protected $appends = ['transportation', 'travelCost'];

    public function expenses()
    {
        return $this->hasMany($this->childModel, $this->parentKey);
    }

    public function getTransportationAttribute()
    {
        return $this->expenses()->where(['type' => 'ebe_total_local_transportation'])->get();
    }

    public function getTravelCostAttribute()
    {
        return $this->expenses()->where(['type' => 'ebe_total_travel_costs'])->get();
    }


}
