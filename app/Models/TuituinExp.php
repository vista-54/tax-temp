<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TuituinExp extends ChildModel
{
    protected $fillable = ['se_load_checkbox', 'created_by', 'session_id'];
    protected $childModel = TuituinExpChild::class;
    protected $parentKey = 'tuition_exp_id';
    protected $appends = ['expenses'];

    public function expenses()
    {
        return $this->hasMany($this->childModel, $this->parentKey);
    }

    public function getExpensesAttribute()
    {
        return $this->expenses()->get();
    }

}
