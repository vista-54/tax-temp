<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DependantInformationChild extends Model
{
    protected $fillable = ['dependent_id', 'full_legal_name', 'date_of_birth', 'relation', 'month_lived', 'married_as', 'ssn',
        'fulltime_student', 'received_income', 'created_by', 'session_id'];
}
