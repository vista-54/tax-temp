<?php

namespace App\Models;

class AdditionalTransportation extends ChildModel
{
    protected $fillable = ['distance', 'days', 'other_dist_checkbox', 'created_by', 'session_id'];
    protected $childModel = AdditionalTransportationOther::class;
    protected $parentKey = 'add_trans_id';

    protected $appends = ['expenses'];

    public function expenses()
    {
        return $this->hasMany($this->childModel, $this->parentKey);
    }

    public function getExpensesAttribute()
    {
        return $this->expenses()->get();
    }
}
