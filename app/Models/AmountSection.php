<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AmountSection extends Model
{
    const AMOUNT_ACTIVE = 1;
    const AMOUNT_INACTIVE = 0;
    protected $fillable = ['section_name', 'status', 'created_by', 'session_id'];
}
