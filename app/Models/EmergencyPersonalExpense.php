<?php

namespace App\Models;

class EmergencyPersonalExpense extends MultiChildModel
{
    protected $fillable = [
        'weapons_radio',
        'created_by', 'session_id'

    ];

    protected $childModel = EmergencyPersonalExpChild::class;
    protected $parentKey = 'em_per_exp_id';

    protected $appends = ['emergency', 'travel', 'weapons'];

    public function expenses()
    {
        return $this->hasMany($this->childModel, $this->parentKey);
    }

    public function getEmergencyAttribute()
    {
        return $this->expenses()->where(['type' => 'emergency_cost'])->get();
    }

    public function getTravelAttribute()
    {
        return $this->expenses()->where(['type' => 'training_cost'])->get();
    }
    public function getWeaponsAttribute()
    {
        return $this->expenses()->where(['type' => 'weapons'])->get();
    }
}
