<?php

namespace App\Models;

class Entertainment extends ChildModel
{
    protected $fillable = ['eme_time_per_week_radio', 'eme_gift_Description_radio', 'eme_time_per_week', 'eme_meal_cost',
        'eme_spouse_time_per_week', 'eme_spouse_meal_cost', 'created_by', 'session_id'];
    protected $childModel = EntertainmentChild::class;
    protected $parentKey = 'entertainment_id';

    protected $appends = ['expenses'];

    public function expenses()
    {
        return $this->hasMany($this->childModel, $this->parentKey);
    }

    public function getExpensesAttribute()
    {
        return $this->expenses()->get();
    }
}
