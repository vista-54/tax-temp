<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmergencyPersonal extends Model
{
    protected $fillable = ['personal_status', 'created_by', 'session_id'];
}
