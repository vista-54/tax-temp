<?php

namespace App\Models;

class AddOptionExp extends ChildModel
{
    protected $fillable = ['fm_add_description_checkbox', 'fm_add_adoptions_checkbox', 'created_by', 'session_id'];
    protected $childModel = AddOptionExpChild::class;
    protected $parentKey = 'add_option_exp_id';

    protected $appends = ['expenses', 'child'];

    public function expenses()
    {
        return $this->hasMany($this->childModel, $this->parentKey);
    }

    public function getExpensesAttribute()
    {
        return $this->expenses()->get();
    }

    public function child()
    {
        return $this->hasMany(AddOptionExpDayCare::class, $this->parentKey);
    }

    public function getChildAttribute()
    {
        return $this->child()->get();
    }

    public function addAdditionalChild($dependants)
    {
        $result = [];
        $this->removeAdditionalChild();
        foreach ($dependants as $item) {
            $res = AddOptionExpDayCare::insert(array_merge([$this->parentKey => $this->id,
                'session_id' => $this->session_id,
                'created_by' => $this->created_by], $item));
            $result[] = $res;
        }
        return $result;
    }

    public function removeAdditionalChild()
    {
        AddOptionExpDayCare::where([$this->parentKey => $this->id])->delete();
    }
}
