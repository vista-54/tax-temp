<?php

namespace App\Models\Spouse;

use Illuminate\Database\Eloquent\Model;

class Spouse extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','middle_name', 'date_of_birth','job_title','last_name','country_code', 'email', 'phone', 'citizenship_status','visa_type',
        'citizenship_other','session_id','user_id','created_by','updated_by','social_security'
    ];


}
