<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class W2Form extends Model
{
    use SoftDeletes;
    protected $table = 'w2form_uploads';
    protected $fillable = ['is_submit', 'image_path', 'created_by'];
}