<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PocketMedicalExp extends Model
{
    protected $fillable = ['insurance','insurance_non','out_of_pocket_radio',
        'out_of_pocket','out_of_pocket_non','created_by','session_id'];
}
