<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SmallItems extends Model
{
    protected $fillable = ['sii_traditional_IRA_contributions_radio', 'sii_roth_IRA_contributions',
        'are_you_teacher_radio', 'sii_self_employed_radio', 'sii_educator_expenses_radio',
        'sii_traditional_IRA_contributions', 'sii_educator_expenses',
        'sii_total_mortgage_payments', 'sii_total_HOA', 'sii_total_utilities', 'created_by',
        'session_id'];
}
