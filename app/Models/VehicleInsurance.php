<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VehicleInsurance extends Model
{
    protected $fillable = ['eves_Total_Auto_Insurance', 'eves_inspection_Total_Costs_radio',
        'eves_inspection_Total_Costs', 'eves_spouse_inspection_Total_Costs', 'created_by', 'session_id'];
}
