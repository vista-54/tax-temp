<?php

namespace App\Models;

class TuituinRelExp extends ChildModel
{
    protected $fillable = ['se_enrolled_in_college_checkbox','created_by','session_id'];
    protected $childModel = TuituinRelExpChild::class;
    protected $parentKey = 'tuition_rel_exp_id';

    protected $appends = ['expenses'];

    public function expenses()
    {
        return $this->hasMany($this->childModel, $this->parentKey);
    }

    public function getExpensesAttribute()
    {
        return $this->expenses()->get();
    }
}
