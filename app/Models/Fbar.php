<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fbar extends Model
{
    protected $fillable=['fbr_outside_money', 'created_by', 'session_id'];
}
