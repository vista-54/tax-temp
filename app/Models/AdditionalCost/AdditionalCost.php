<?php

namespace App\Models\AdditionalCost;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdditionalCost extends Model 
{

    protected $table = 'additional_costs';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = [
        'cost_type', 'cost_description', 'cost_amount'
    ];

}