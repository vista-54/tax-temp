<?php

namespace App\Models\ActivityLog;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActivityLog extends Model
{
    use SoftDeletes;

    protected $fillable = ['subject', 'url', 'method', 'ip', 'agent', 'request_data','response_data','created_by','updated_by'];



}
