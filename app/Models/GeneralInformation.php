<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GeneralInformation extends Model
{
    protected $fillable = ['is_student_loan','is_donated_charity','is_own_home','is_kid_dependent','is_vehicle','is_emergency_personal', 'created_by', 'session_id'];
}
