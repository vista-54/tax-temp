<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VehicleExp extends Model
{
    protected $fillable = ['ve_make', 've_model', 've_price', 've_spouse_make', 've_spouse_model', 've_spouse_price',
        'created_by', 'session_id','spouse_own_lease','own_lease'];
}
