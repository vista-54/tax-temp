<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DependantInformation extends Model
{
    protected $fillable = ['have_dependents', 'created_by', 'session_id'];
    protected $appends = ['dependents'];

    public function dependents()
    {
        return $this->hasMany(DependantInformationChild::class,'dependent_id');
    }

    public function getDependentsAttribute()
    {
        return $this->dependents()->get();
    }

    public function addDependantChild($dependants)
    {
        $result = [];
        $this->removeAllChild();
        foreach ($dependants as $item) {
            $res = DependantInformationChild::insert(array_merge(['dependent_id' => $this->id,
                'session_id' => $this->session_id,
                'created_by' => $this->created_by], $item));
            $result[] = $res;
        }
        return $result;
    }

    public function removeAllChild()
    {
        DependantInformationChild::where(['dependent_id' => $this->id])->delete();
    }
}
