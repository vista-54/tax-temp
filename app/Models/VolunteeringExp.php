<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VolunteeringExp extends Model
{
    protected $fillable = ['ve_volunteering_checkbox', 'created_by', 'session_id'];

    protected $appends = ['expenses'];

    public function expenses()
    {
        return $this->hasMany(VolunteeringExpChild::class, 'vol_exp_id');
    }

    public function getExpensesAttribute()
    {
        return $this->expenses()->get();
    }

    public function addExpenseChild($dependants)
    {
        $result = [];
        $this->removeAllChild();
        foreach ($dependants as $item) {
            $res = VolunteeringExpChild::insert(array_merge(['vol_exp_id' => $this->id,
                'session_id' => $this->session_id,
                'created_by' => $this->created_by], $item));
            $result[] = $res;
        }
        return $result;
    }

    public function removeAllChild()
    {
        VolunteeringExpChild::where(['vol_exp_id' => $this->id])->delete();
    }
}
