<?php

namespace App\Http\Controllers;

use App\Models\UserSession;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Auth;

class ApiController extends Controller
{
    protected $model = Model::class;

    public function store(Request $request)
    {
        $userId = Auth::user()->id;
        $session = UserSession::where(['user_id' => $userId, 'status' => UserSession::STATUS_ACTIVE])->first();
        $mainparams = [
            'created_by' => $userId,
            'session_id' => $session->id
        ];
        return $this->model::updateOrCreate($mainparams, $request->post());
    }
    
    /**
     * Display the specified resource.
     *
     * @param  integer  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $userId = Auth::user()->id;
            // dd($userId);
            $modalName = class_basename($this->model);
            $modelInfo = $this->model::where('created_by' , $userId)->first();
            if(!$modelInfo){
                return Helper::notFoundResponse('We can\'t find '.$modalName.' with given id.');
            }
            
            return Helper::getSuccessResponse($modalName.' Information.',$modelInfo);
        } catch (\Exception $ex) {
            return Helper::serverErrorResponse($ex->getMessage());
        }
    }
}
