<?php

namespace App\Http\Controllers;

use App\Models\Entertainment;
use Illuminate\Http\Request;

class EntertainmentController extends ApiController
{
    protected $model= Entertainment::class;

    public function store(Request $request)
    {
        $savedRecord = parent::store($request);
        if ($savedRecord->eme_gift_Description_radio == '1') {
            return $savedRecord->addExpenseChild($request->input('entertainment_expenses'));
        } else {
            $savedRecord->removeAllChild();
            return $savedRecord;
        }
    }
}
