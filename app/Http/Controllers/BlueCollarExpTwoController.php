<?php

namespace App\Http\Controllers;

use App\Models\BlueCollarExpTwo;
use Illuminate\Http\Request;

class BlueCollarExpTwoController extends ApiController
{
    protected $model = BlueCollarExpTwo::class;


    public function store(Request $request)
    {
        $savedRecord = parent::store($request);
        $result = [];
        $savedRecord->removeAllChild();
        if ($savedRecord->expenses_uniforms_radio == '1') {
            array_push($result, $savedRecord->addExpenseChild($request->input('expenses_uniforms'), 'expenses_uniforms'));
        }
        if ($savedRecord->work_clothing_radio == '1') {
            array_push($result, $savedRecord->addExpenseChild($request->input('work_clothing'), 'work_clothing'));
        }
        if ($savedRecord->dry_cleaning_radio == '1') {
            array_push($result, $savedRecord->addExpenseChild($request->input('dry_cleaning'), 'dry_cleaning'));
        } else {
            $savedRecord->removeAllChild();
            return $result;
        }
        return $result;
    }
}
