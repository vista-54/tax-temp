<?php

namespace App\Http\Controllers;

use App\Models\BlueCollarExp;
use Illuminate\Http\Request;

class BlueCollarExpController extends ApiController
{
    protected $model = BlueCollarExp::class;


    public function store(Request $request)
    {
        $savedRecord = parent::store($request);
        $result = [];
        $savedRecord->removeAllChild();
        if ($savedRecord->expenses_equipment_radio == '1') {
            array_push($result, $savedRecord->addExpenseChild($request->input('expenses_equipment'), 'expenses_equipment'));
        }
        if ($savedRecord->purchasing_machinery_radio == '1') {
            array_push($result, $savedRecord->addExpenseChild($request->input('purchasing_machinery'), 'purchasing_machinery'));
        }
        if($savedRecord->expenses_equipment_radio != '1' && $savedRecord->purchasing_machinery_radio != '1') {
            $savedRecord->removeAllChild();
            return $result;
        }
        return $result;
    }
}
