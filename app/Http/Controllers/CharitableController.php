<?php

namespace App\Http\Controllers;

use App\Models\Charitabledonations;
use Illuminate\Http\Request;

class CharitableController extends ApiController
{
    protected $model = Charitabledonations::class;

    public function store(Request $request)
    {
        $savedRecord = parent::store($request);
        if ($savedRecord->make_cache == '1') {
            return $savedRecord->addExpenseChild($request->input('donations'));
        } else {
            $savedRecord->removeAllChild();
            return $savedRecord;
        }
    }
}
