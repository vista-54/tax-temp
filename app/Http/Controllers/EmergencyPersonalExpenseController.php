<?php

namespace App\Http\Controllers;

use App\Models\EmergencyPersonalExpense;
use Illuminate\Http\Request;

class EmergencyPersonalExpenseController extends ApiController
{
    protected $model = EmergencyPersonalExpense::class;

    public function store(Request $request)
    {
        $savedRecord = parent::store($request);
        $result = [];
        $savedRecord->removeAllChild();
        if ($savedRecord->weapons_radio == '1') {
            array_push($result, $savedRecord->addExpenseChild($request->input('weapons_arr'), 'weapons'));
        }
        if (count($request->input('emergency_cost')) >0) {
            array_push($result, $savedRecord->addExpenseChild($request->input('emergency_cost'), 'emergency_cost'));
        }
        if (count($request->input('training_cost')) >0) {
            array_push($result, $savedRecord->addExpenseChild($request->input('training_cost'), 'training_cost'));
        } else {
            $savedRecord->removeAllChild();
            return $result;
        }
        return $result;
    }
}
