<?php

namespace App\Http\Controllers;

use App\Models\VolunteeringExp;
use Illuminate\Http\Request;

class VolunteeringExpController extends ApiController
{
    protected $model = VolunteeringExp::class;

    public function store(Request $request)
    {
        $savedRecord = parent::store($request);
        if ($savedRecord->ve_volunteering_checkbox == '1') {
            return $savedRecord->addExpenseChild($request->input('expenses'));
        } else {
            $savedRecord->removeAllChild();
            return $savedRecord;
        }
    }
}
