<?php

namespace App\Http\Controllers;

use App\Models\PropertyDonations;
use Illuminate\Http\Request;

class PropertyController extends ApiController
{
    protected $model = PropertyDonations::class;

    public function store(Request $request)
    {
        $savedRecord = parent::store($request);
        if ($savedRecord->pd_checkbox == '1') {
            return $savedRecord->addDonationsChild($request->input('donations'));
        } else {
            $savedRecord->removeAllChild();
            return $savedRecord;
        }
    }
}
