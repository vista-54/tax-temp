<?php

namespace App\Http\Controllers;

use App\Models\PocketPersonalTwo;

class PocketPersonalTwoController extends ApiController
{
    protected $model = PocketPersonalTwo::class;
}
