<?php

namespace App\Http\Controllers;

use App\Models\VehicleMaintenance;

class VehicleMaintenanceController extends ApiController
{
    protected $model = VehicleMaintenance::class;
}
