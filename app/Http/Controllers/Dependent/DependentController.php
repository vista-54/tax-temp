<?php 

namespace App\Http\Controllers\Dependent;

use App\Helpers\Helper;
use App\Models\User\User;
use Illuminate\Http\Request;
use App\Models\Dependent\Depedent;
use App\Http\Requests\Spouse\AddRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;

class DependentController extends Controller 
{
  protected $dependentObj = null;

  /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Depedent $dependentObj)
    {
        $this->dependentObj = $dependentObj;
    }
  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
    try {

        DB::beginTransaction();
        $data_arr = $request->only($this->dependentObj->getFillable());
        $dependent = Dependent::create($data_arr);
        DB::commit();
        
        return Helper::successResponse('Dependent saved successfully',$dependent);
    } catch (\Exception $ex) {
        DB::rollback();
        Log::info('error_while_save_dependent' . $ex->getMessage());
        return Helper::serverErrorResponse($ex->getMessage());
    }
    
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    try {
        $dependent = $this->dependentObj->find($id);
        // var_dump($spouse);exit;
        if(!$dependent){
            return Helper::notFoundResponse('We can\'t find Dependent with that id.');
        }
        return Helper::successResponse('Single Dependent Insormation.',$dependent);
    } catch (\Exception $ex) {
        return Helper::serverErrorResponse($ex->getMessage());
    }
    
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    
  }
  
}

?>