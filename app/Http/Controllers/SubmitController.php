<?php

namespace App\Http\Controllers;

use App\Models\AdditionalTransportation;
use App\Models\AddOptionExp;
use App\Models\BasicInformation;
use App\Models\BlueCollarExp;
use App\Models\BlueCollarExpTwo;
use App\Models\BusinessExp;
use App\Models\Charitabledonations;
use App\Models\DependantInformation;
use App\Models\DepositInfromation;
use App\Models\EmergencyPersonal;
use App\Models\EmergencyPersonalExpense;
use App\Models\EmployeeBusinessExpOne;
use App\Models\EmployeeBusinessExpTwo;
use App\Models\Entertainment;
use App\Models\Fbar;
use App\Models\GenerationActivities;
use App\Models\HomeExpense;
use App\Models\PersonalInformation;
use App\Models\PocketMedicalExp;
use App\Models\PocketPersonalTwo;
use App\Models\PropertyDonations;
use App\Models\Rpi;
use App\Models\SmallItems;
use App\Models\Spouse\Spouse;
use App\Models\TuituinExp;
use App\Models\TuituinRelExp;
use App\Models\User\User;
use App\Models\UserAdditionalInfo\UserAdditionalInfo;
use App\Models\UserSession;
use App\Models\VehicleExp;
use App\Models\VehicleInsurance;
use App\Models\VehicleMaintenance;
use App\Models\VolunteeringExp;
use App\Models\WorkToolExam;
use App\Models\WorkToolResearch;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use JWTAuth;
use PDF;
use Tymon\JWTAuth\Exceptions\JWTException;

class SubmitController extends Controller
{

    public function unlock()
    {
        $userId = Auth::user()->id;
        $user = User::where(['id'=>$userId])->first();
        $session = UserSession::where(['user_id' => $userId, 'status' => UserSession::STATUS_INACTIVE])->orderby('id', 'DESC')->first();
        $admins = User::where(['role'=>'ADMIN'])->pluck('email')->toArray();
        Mail::send('emails.unlock', array('username' => $user->first_name.' '.$user->last_name), function ($message) use ($admins) {
            $message->to($admins)->subject('Unlock Application');
        });
        return response([
            'status' =>
                UserSession::create([
                    'prev_session_id' => $session->id,
                    'user_id' => $userId,
                    'status' => UserSession::STATUS_ACTIVE
                ]),
            'msg' => 'app unlocked successfully']);
    }

    public function finish()
    {
        $userId = Auth::user()->id;
        $session = UserSession::where(['user_id' => $userId, 'status' => UserSession::STATUS_ACTIVE])->first();
        $session->status = UserSession::STATUS_INACTIVE;
        $user = User::where(['id' => $userId])->first();
        $user->updated_at = now();
        $user->save();
        //make sure that all screens are fill correct
        $data = $this->preparePDFdata($session->id);
        if(isset($session->prev_session_id)){
            $admins = User::where(['role'=>'ADMIN'])->pluck('email')->toArray();
            Mail::send('emails.submit_unlock', array('username' => $user->first_name.' '.$user->last_name), function ($message) use ($admins) {
                $message->to($admins)->subject('Unlock Application submitted');
            });
        }
        if (count($this->validateFinish($data))) {
            return response()->json(['message' => json_encode($this->validateFinish($data)), 'fields' => $this->validateFinish($data)], 400);
        }
        if ($session->save()) {
            UserSession::updateOrCreate([
                'user_id' => $userId,
                'status' => 1
            ]);
            return response()->json(['message' => 'Current session finished', 'status' => true]);
        } else {
            return response()->json(['message' => 'Something went wrong', 'status' => false], 400);
        }
    }

    public function generatePdfByUser(\Illuminate\Http\Request $request, User $user)
    {
        $token = $request->input('token');
        try {
            JWTAuth::setToken($token);

            $session = UserSession::where(['user_id' => $user->id, 'status' => UserSession::STATUS_INACTIVE])->orderBy('id', 'DESC')->first();
            if ($session) {
                $data = $this->preparePDFdata($session->id);
                $data['submited_at'] = $session->updated_at;
                if(isset($session->prev_session_id)){
                    $prev_data = $this->preparePDFdata($session->prev_session_id);
                    $data['prev_data'] = $prev_data;
                }else{
                    $data['prev_data']= [];
                }

                $pdf = PDF::loadView('pdf/pdf', $data);
                return $pdf->stream('/document.pdf');
            } else {
                return response()->json(
                    [
                        'status' => false,
                        'message' => 'User does not have any reports'
                    ]
                );
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['success' => false, 'error' => 'Access denied'], 500);
        }
    }


    protected function preparePDFdata($sessionId)
    {

        return [
            'personal_info' => PersonalInformation::where(['session_id' => $sessionId])->first(),
            'spouse_info' => Spouse::where(['session_id' => $sessionId])->first(),
            'additional_info' => UserAdditionalInfo::where(['session_id' => $sessionId])->first(),
            'basic_info' => BasicInformation::where(['session_id' => $sessionId])->first(),
            'dependent_info' => DependantInformation::where(['session_id' => $sessionId])->first(),
            'fbar' => Fbar::where(['session_id' => $sessionId])->first(),
            'rpi' => Rpi::where(['session_id' => $sessionId])->first(),
            'siga' => GenerationActivities::where(['session_id' => $sessionId])->first(),
            'cd' => Charitabledonations::where(['session_id' => $sessionId])->first(),
            'property_donations' => PropertyDonations::where(['session_id' => $sessionId])->first(),
            'volonteering_exp' => VolunteeringExp::where(['session_id' => $sessionId])->first(),
            'education_tution' => TuituinExp::where(['session_id' => $sessionId])->first(),
            'tuition_loan_expenses' => TuituinRelExp::where(['session_id' => $sessionId])->first(),
            'add_options_exp' => AddOptionExp::where(['session_id' => $sessionId])->first(),
            'home_expense' => HomeExpense::where(['session_id' => $sessionId])->first(),
            'small_items' => SmallItems::where(['session_id' => $sessionId])->first(),
            'business_expense' => BusinessExp::where(['session_id' => $sessionId])->first(),
            'vehicle_expense' => VehicleExp::where(['session_id' => $sessionId])->first(),
            'vehicle_maintenance' => VehicleMaintenance::where(['session_id' => $sessionId])->first(),
            'vehicle_insurance' => VehicleInsurance::where(['session_id' => $sessionId])->first(),
            'additional_transport' => AdditionalTransportation::where(['session_id' => $sessionId])->first(),
            'entertainment' => Entertainment::where(['session_id' => $sessionId])->first(),
            'work_tool' => EmployeeBusinessExpOne::where(['session_id' => $sessionId])->first(),
            'work_tool_cell' => EmployeeBusinessExpTwo::where(['session_id' => $sessionId])->first(),
            'work_tool_exam' => WorkToolExam::where(['session_id' => $sessionId])->first(),
            'work_tool_research' => WorkToolResearch::where(['session_id' => $sessionId])->first(),
            'blue_collar_expense' => BlueCollarExp::where(['session_id' => $sessionId])->first(),
            'blue_collar_expense_two' => BlueCollarExpTwo::where(['session_id' => $sessionId])->first(),
            'emergency_personal' => EmergencyPersonal::where(['session_id' => $sessionId])->first(),
            'emergency_personal_exp' => EmergencyPersonalExpense::where(['session_id' => $sessionId])->first(),
            'pocket_medical' => PocketMedicalExp::where(['session_id' => $sessionId])->first(),
            'pocket_medical_two' => PocketPersonalTwo::where(['session_id' => $sessionId])->first(),
            'deposit_infromation' => DepositInfromation::where(['session_id' => $sessionId])->first(),
        ];
    }

    private function validateFinish($data)
    {
        $result = [];
        foreach ($data as $key => $item) {
            if ($item === null) {
                if ($key !== 'spouse_info' && $key !== 'emergency_personal_exp' && $key !== 'basic_info' && $key !== 'deposit_infromation')
                    $result[] = $key . ' is required to fill';
            }
        }
        return $result;
    }

    public function getSubmitStatus(){

        $session = UserSession::where(['user_id' => Auth::id()])->first();
        return response()->json(['status' => true, 'message' => 'User Session Information', 'data' => $session]);
        dd($session);
    }
}
