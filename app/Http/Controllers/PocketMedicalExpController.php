<?php

namespace App\Http\Controllers;

use App\Models\PocketMedicalExp;
use Illuminate\Http\Request;

class PocketMedicalExpController extends ApiController
{
    protected $model = PocketMedicalExp::class;
}
