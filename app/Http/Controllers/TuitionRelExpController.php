<?php

namespace App\Http\Controllers;

use App\Models\TuituinRelExp;
use Illuminate\Http\Request;

class TuitionRelExpController extends ApiController
{
    protected $model = TuituinRelExp::class;

    public function store(Request $request)
    {
        $savedRecord = parent::store($request);
        if ($savedRecord->se_enrolled_in_college_checkbox == '1') {
            return $savedRecord->addExpenseChild($request->input('tuition_expenses'));
        } else {
            $savedRecord->removeAllChild();
            return $savedRecord;
        }
    }
}
