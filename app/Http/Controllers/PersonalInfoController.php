<?php

namespace App\Http\Controllers;

use App\Models\PersonalInformation;
use App\Models\UserSession;
use Illuminate\Http\Request;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Auth;

class PersonalInfoController extends ApiController
{
    protected $model=PersonalInformation::class;
    public function store(Request $request)
    {
        $userId = Auth::user()->id;
        $session = UserSession::where(['user_id' => $userId, 'status' => UserSession::STATUS_ACTIVE])->first();
        if($session){
            $mainparams = [
                'created_by' => $userId,
                'session_id' => $session->id
            ];
            return PersonalInformation::updateOrCreate($mainparams, $request->post());
        }else{
            return response()->json(['message'=>'You current session is closed, please sign in again']);
        }

    }


}
