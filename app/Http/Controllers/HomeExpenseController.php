<?php

namespace App\Http\Controllers;

use App\Models\HomeExpense;
use Illuminate\Http\Request;

class HomeExpenseController extends ApiController
{
    protected $model = HomeExpense::class;


    public function store(Request $request)
    {
        $savedRecord = parent::store($request);
        if ($savedRecord->hm_energy_efficient_upgrades_radio == '1') {
            return $savedRecord->addExpenseChild($request->input('totals'));
        } else {
            $savedRecord->removeAllChild();
            return $savedRecord;
        }
    }
}
