<?php

namespace App\Http\Controllers;

use App\Models\EmployeeBusinessExpThree;
use Illuminate\Http\Request;

class EmployeeBusinessExpThreeController extends ApiController
{
    protected $model = EmployeeBusinessExpThree::class;


    public function store(Request $request)
    {
        $savedRecord = parent::store($request);
        $result = [];
        $savedRecord->removeAllChild();
        if ($savedRecord->exams_or_licence_radio == '1') {
            array_push($result, $savedRecord->addExpenseChild($request->input('license_renewal'), 'license_renewal'));
        }
        if ($savedRecord->membership_fees_radio == '1') {
            array_push($result, $savedRecord->addExpenseChild($request->input('membership_fees'), 'membership_fees'));
        } else {
            $savedRecord->removeAllChild();
            return $result;
        }
        return $result;
    }
}
