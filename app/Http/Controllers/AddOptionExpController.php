<?php

namespace App\Http\Controllers;

use App\Models\AddOptionExp;
use Illuminate\Http\Request;

class AddOptionExpController extends ApiController
{
    protected $model = AddOptionExp::class;

    public function store(Request $request)
    {
        $savedRecord = parent::store($request);
        if ($savedRecord->fm_add_description_checkbox == '1') {
            $savedRecord->addExpenseChild($request->input('day_care_expenses'));
        } else {
            $savedRecord->removeAllChild();
        }
        if ($savedRecord->fm_add_adoptions_checkbox == '1') {
            $savedRecord->addAdditionalChild($request->input('add_adoptions_expenses'));
        } else {
            $savedRecord->removeAdditionalChild();
        }

        return $savedRecord;

    }
}
