<?php

namespace App\Http\Controllers\Document;

use DB;
use App\Helpers\Helper;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\HtmlString;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Models\User\User;
use App\Models\Document\Document;
use App\Models\OperatingAgreement\OperatingAgreement;

class DocumentController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
            
    }


    public static function _partialGenerateViews($filename = null,$viewName = null,$data = null, $user = null){

        try{

        
            ini_set('max_execution_time', '300');
            
            $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
            $fontData = $defaultFontConfig['fontdata'];

            $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
            $fontDirs = $defaultConfig['fontDir'];

            $times = array('times_new_roman' => array(
                'R' => 'times.ttf',
                'B' => 'timesbd.ttf',
                'I' => 'timesi.ttf'
            ));

            $arial = array('arial' => array(
                'R' => 'arial.ttf',
                'B' => 'arialbd.ttf',
                'BI' => 'arialbi.ttf'
            ));

            $calibri = array('calibri' => array(
                'R' => 'calibri.ttf',
                'B' => 'calibrib.ttf',
                'I' => 'calibrii.ttf'

            ));

            $arial_narrow = array('arial_narrow' => array(
                'R' => 'arialn.ttf',
                'B' => 'arialnb.ttf',
                'BI' => 'arialnbi.ttf'

            ));

            $arialmt = array('arialmt' => array(
                'R' => 'arialmt.ttf',
                'B' => 'arialmtbd.ttf'
            ));

            $cour = array('cour' => array(
                'R' => 'cour.ttf'
            ));

            $tahoma = array('tahoma' => array(
                'R' => 'tahoma.ttf'
            ));

            $helvetica = array('helvetica' => array(
                'R' => 'helvetica.ttf',
                'B' => 'helveticabd.ttf'
            ));

            $garamond = array('garamond' => array(
                'R' => 'gara.ttf',
                'B' => 'garabd.ttf',
                'I' => 'garai.ttf'
            ));

            $myriad = array('myriadpro' => array(
                'R' => 'myriadpro.ttf',
                'B' => 'myriadprobd.ttf'
            ));


            $newfontdata = $fontData + $times + $arial + $calibri + $arial_narrow + $arialmt + $cour + $tahoma + $helvetica + $garamond + $myriad;
            $customfontdir =  array("1"=>__DIR__.'/../../../resources/fonts');
            $newfontdir = $fontDirs + $customfontdir;
            
            $mpdf = new \Mpdf\Mpdf([
                'fontDir' => $newfontdir,
                'fontdata' => $newfontdata,
                'default_font' => 'times',
                'format' => array(216,279),
                'tempDir' => base_path('temp')
            ]);

            // $mpdf->SetWatermarkImage(url('local/resources/images/water-mark.png'), 0.8, 'D', array(35,110));
            // $mpdf->showWatermarkImage = true;
            $dataObj = json_decode($data->result);
            $html = view($viewName, ['viewData' => $dataObj, 'patient' => $user])->render();
            // var_dump($html);exit;
            ini_set('memory_limit', '-1');

            $mpdf->WriteHTML($html);
            // $mpdf->WriteHTML('');

             return $content = $mpdf->Output();


            $content = $mpdf->Output('', 'S');

            Storage::put($filename, $content);
            return true;
        }catch(\Exception $ex){
            return ['error' => $ex->getMessage(), 'line' => $ex->getLine()];
        }
        // $b64Doc = chunk_split(base64_encode($content));
        // return $b64Doc;

    }

    
    public  function operatingAgreement(Request $request, $id){
        
        try{
            $operatingAgreement = NULL;
            $operatingAgreementResp = OperatingAgreement::where(['created_by' => $id])->first(); 
            // dd($operatingAgreementResp);
            if(!$operatingAgreementResp){
                return Helper::notFoundResponse('No operating agreement with this ID.');
            }

            $user = NULL;
            if($operatingAgreementResp != null){
                $user = User::find($operatingAgreementResp->created_by);
            }
            
            
            $files=array();
            $name="operating_agreement";
            $viewName = 'documents.operating_agreement';
            $filename = $name .'_'. $id .'_'.time().'.pdf';
            $uploadedResp = self::_partialGenerateViews($filename,$viewName,$operatingAgreementResp, $user);
            
            if($uploadedResp === true){
                $path = Storage::url($filename);
                $document = Document::updateDocument($path, $filename, 'operating-agreement');
                return Helper::successResponse('Operating Agreement generated successfully.', $document);
            }else{
                return Helper::serverErrorResponse($uploadedResp);
            }
            
        }catch(\Exception $ex){
            return Helper::serverErrorResponse($ex->getMessage());
        }
        
    }

    public static function _partialGenerateAiDocs($filename = null,$viewName = null,$data = null, $user = null){

        try{

        
            ini_set('max_execution_time', '300');
            
            $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
            $fontData = $defaultFontConfig['fontdata'];

            $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
            $fontDirs = $defaultConfig['fontDir'];

            $times = array('times_new_roman' => array(
                'R' => 'times.ttf',
                'B' => 'timesbd.ttf',
                'I' => 'timesi.ttf'
            ));

            $arial = array('arial' => array(
                'R' => 'arial.ttf',
                'B' => 'arialbd.ttf',
                'BI' => 'arialbi.ttf'
            ));

            $calibri = array('calibri' => array(
                'R' => 'calibri.ttf',
                'B' => 'calibrib.ttf',
                'I' => 'calibrii.ttf'

            ));

            $arial_narrow = array('arial_narrow' => array(
                'R' => 'arialn.ttf',
                'B' => 'arialnb.ttf',
                'BI' => 'arialnbi.ttf'

            ));

            $arialmt = array('arialmt' => array(
                'R' => 'arialmt.ttf',
                'B' => 'arialmtbd.ttf'
            ));

            $cour = array('cour' => array(
                'R' => 'cour.ttf'
            ));

            $tahoma = array('tahoma' => array(
                'R' => 'tahoma.ttf'
            ));

            $helvetica = array('helvetica' => array(
                'R' => 'helvetica.ttf',
                'B' => 'helveticabd.ttf'
            ));

            $garamond = array('garamond' => array(
                'R' => 'gara.ttf',
                'B' => 'garabd.ttf',
                'I' => 'garai.ttf'
            ));

            $myriad = array('myriadpro' => array(
                'R' => 'myriadpro.ttf',
                'B' => 'myriadprobd.ttf'
            ));


            $newfontdata = $fontData + $times + $arial + $calibri + $arial_narrow + $arialmt + $cour + $tahoma + $helvetica + $garamond + $myriad;
            $customfontdir =  array("1"=>__DIR__.'/../../../resources/fonts');
            $newfontdir = $fontDirs + $customfontdir;
            
            $mpdf = new \Mpdf\Mpdf([
                'fontDir' => $newfontdir,
                'fontdata' => $newfontdata,
                'default_font' => 'times',
                'format' => array(216,279),
                'tempDir' => base_path('temp')
            ]);

            // $mpdf->SetWatermarkImage(url('local/resources/images/water-mark.png'), 0.8, 'D', array(35,110));
            // $mpdf->showWatermarkImage = true;
            $dataObj = json_decode($data);
            //dd($dataObj);
            $html = view($viewName, ['viewData' => $dataObj, 'patient' => $user])->render();
            // var_dump($html);exit;
            ini_set('memory_limit', '-1');

            $mpdf->WriteHTML($html);
            // $mpdf->WriteHTML('');

             //return $content = $mpdf->Output();


            $content = $mpdf->Output('', 'S');

            Storage::put($filename, $content);
            return true;
        }catch(\Exception $ex){
            return ['error' => $ex->getMessage(), 'line' => $ex->getLine()];
        }
        // $b64Doc = chunk_split(base64_encode($content));
        // return $b64Doc;

    }

    public  function aiTaxOnlineDoc(Request $request){
            
        try{
            
            $id = $request->user_id;
            $aiTaxOnlineDocResp = json_encode($request->aitax_online);
            //dd($aiTaxOnlineDocResp);
            /*$aiTaxOnlineDocResp = OperatingAgreement::where(['created_by' => $id])->first(); 
             dd($operatingAgreementResp);
            if(!$aiTaxOnlineDocResp){
                return Helper::notFoundResponse('No document with this ID.');
            }*/

            $user = NULL;
            if($aiTaxOnlineDocResp != null){
                $user = User::find($id);
            }
            //dd($user);            
            
            $files=array();
            $name="aitax_online_doc";
            $viewName = 'documents.aitax_online_doc';
            $filename = $name .'_'. $id .'_'.time().'.pdf';
            $uploadedResp = self::_partialGenerateAiDocs($filename,$viewName,$aiTaxOnlineDocResp, $user);
            
            if($uploadedResp === true){
                $path = Storage::url($filename);
                $document = Document::updateDocument($path, $filename, 'aitax-online');
                return Helper::successResponse('Document generated successfully.', $document);
            }else{
                return Helper::serverErrorResponse($uploadedResp);
            }
            
        }catch(\Exception $ex){
            return Helper::serverErrorResponse($ex->getMessage());
        }
        
    }

    public  function getAiTaxOnlineDoc(Request $request, $id){
            
        try{
            
            $aiTaxOnlineDocResp = Document::where(['type' => 'aitax-online','created_by' => $id])->first(); 

            if(!$aiTaxOnlineDocResp){
                return Helper::notFoundResponse('No document with this ID.');
            }else{
                return Helper::successResponse('Success', $aiTaxOnlineDocResp);
            }
            
        }catch(\Exception $ex){
            return Helper::serverErrorResponse($ex->getMessage());
        }
        
    }
}
