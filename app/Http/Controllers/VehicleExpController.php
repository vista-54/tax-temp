<?php

namespace App\Http\Controllers;

use App\Models\VehicleExp;

class VehicleExpController extends ApiController
{
    protected $model = VehicleExp::class;
}
