<?php

namespace App\Http\Controllers;

use App\Models\EmployeeBusinessExpOne;
use Illuminate\Http\Request;

class EmployeeBusinessExpOneController extends ApiController
{
    protected $model = EmployeeBusinessExpOne::class;


    public function store(Request $request)
    {
        $savedRecord = parent::store($request);
        $result = [];
        $savedRecord->removeAllChild();
        if ($savedRecord->et_office_furniture_description_radio == '1') {
            array_push($result, $savedRecord->addExpenseChild($request->input('office_furniture'), 'office_furniture'));
        }
        if ($savedRecord->et_office_equipment_description_radio == '1') {
            array_push($result, $savedRecord->addExpenseChild($request->input('office_equipment'), 'office_equipment'));
        }
        if ($savedRecord->et_seeking_expense_description_radio == '1') {
            array_push($result, $savedRecord->addExpenseChild($request->input('seeking_expense'), 'seeking_expense'));
        } else {
            $savedRecord->removeAllChild();
            return $result;
        }
        return $result;
    }
}
