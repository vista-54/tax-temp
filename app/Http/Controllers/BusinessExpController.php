<?php

namespace App\Http\Controllers;

use App\Models\BusinessExp;
use Illuminate\Http\Request;

class BusinessExpController extends ApiController
{
    protected $model = BusinessExp::class;

    public function store(Request $request)
    {
        $savedRecord = parent::store($request);
        $result = [];
        $savedRecord->removeAllChild();
        if ($savedRecord->ebe_total_local_transportation_radio == '1') {
            array_push($result, $savedRecord->addExpenseChild($request->input('ebe_total_local_transportation'), 'ebe_total_local_transportation'));
        }
        if ($savedRecord->ebe_total_travel_costs_radio == '1') {
            array_push($result, $savedRecord->addExpenseChild($request->input('ebe_total_travel_costs'), 'ebe_total_travel_costs'));
        } else {
            $savedRecord->removeAllChild();
            return $result;
        }
        return $result;
    }
}
