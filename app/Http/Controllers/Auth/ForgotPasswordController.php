<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Models\User\User;
use Illuminate\Http\Request;
use DB;


class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {   
        $this->middleware('guest');
    }
    
    public function getResetToken(Request $request)
    {
        
        $input =  $request->all();

        $user = User::where('email', $request->input('email'))->first();
       
        if(!$user){

           return response()->json([
                'success' => false,
                'message' => "We can't find a user with that e-mail address.",
            ], 401);
        }

        if(!empty($input['email'])){
           $this->validateEmail($request);
           $response = $this->broker()->sendResetLink(
              $request->only('email')
          );
        }

        return response()->json([
            'success' => true,
            'message' => 'We have e-mailed your password reset link!',
        ]);
    }
}