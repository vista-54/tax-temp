<?php

namespace App\Http\Controllers;

use App\Models\BasicInformation;

class BasicInformationController extends ApiController
{
    protected $model = BasicInformation::class;

}
