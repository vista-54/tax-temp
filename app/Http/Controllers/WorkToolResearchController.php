<?php

namespace App\Http\Controllers;

use App\Models\WorkToolResearch;
use Illuminate\Http\Request;

class WorkToolResearchController extends ApiController
{
    protected $model = WorkToolResearch::class;

    public function store(Request $request)
    {
        $savedRecord = parent::store($request);
        if ($savedRecord->research_radio == '1') {
            return $savedRecord->addExpenseChild($request->input('research'));
        } else {
            $savedRecord->removeAllChild();
            return $savedRecord;
        }
    }
}
