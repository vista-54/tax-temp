<?php

namespace App\Http\Controllers\User;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegistrationFormRequest;
use App\Http\Requests\User\SingleUser;
use App\Models\User\User;
use App\Models\UserSession;
use App\Models\VerifyUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class UserController extends Controller
{
    use SendsPasswordResetEmails;

    /**
     * @var bool
     */
    public $loginAfterSignUp = true;

    protected $userObj = null;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(User $userObj)
    {
        $this->userObj = $userObj;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {


        $token = null;
        $credentials = $request->only('email', 'password');

        // dd($credentials);
        $credentials['status'] = 1;
        try {

            if (!$token = JWTAuth::attempt($credentials))
                return Helper::unAuthResponse('Invalid Credentials');

        } catch (JWTException $ex) {
            return Helper::serverErrorResponse($ex->getMessage());
        }

        $user_id = Auth::id();
        $user = User::find($user_id);
        if (!$token)
            return Helper::notFoundResponse('Invalid username or password');

        $session = UserSession::updateOrCreate([
            'user_id' => $user->id,
            'status' => 1]);
        $data = ['token' => $token, 'user' => $user, 'session_id' => $session->id];
        /**
         * Create session if doesn't have
         */

        return Helper::successResponse('User Login successfully', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function logout(Request $request)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);

        try {
            JWTAuth::invalidate($request->token);

            return response()->json([
                'success' => true,
                'message' => 'User logged out successfully'
            ]);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, the user cannot be logged out'
            ], 500);
        }
    }

    /**
     * @param RegistrationFormRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function register(RegistrationFormRequest $request)
    {
        try {
            DB::beginTransaction();
            $data_arr = $request->only($this->userObj->getFillable());
            // dd($data_arr);
            $data_arr['password'] = Hash::make($data_arr['password']);
            // $user = User::create($data_arr);
            $data_arr['addresses'] = $request->input('addresses');
            $token = md5(time());
            $user = User::saveUser($data_arr);
            DB::commit();

            VerifyUser::create([
                'user_id' => $user->id,
                'token' => $token
            ]);

            Mail::send('emails.email_verification', array('token' => $token), function ($message) use ($data_arr) {
                $message->to($data_arr['email'], $data_arr['first_name'] . ' ' . $data_arr['last_name'])->subject('Account Verification');
            });
            return Helper::successResponse('User registered successfully', $user);
        } catch (\Exception $ex) {
            DB::rollback();
            Log::info('error_while_register_user' . $ex->getMessage());
            return Helper::serverErrorResponse($ex->getMessage());
        }

    }

    /**
     * Get user list with filters.
     *
     * @param  [string]  name
     * @param  [string]  email
     * @param  [integer] per_page
     * @param  [integer] page
     * @param  [boolean] pagination
     * @return [json] resutl;
     */
    public function list(Request $request)
    {
        try {

            $users = User::searchUsers($request);
            return Helper::successResponse('Searched User List.', $users, $request);
        } catch (\Exception $ex) {
            return Helper::serverErrorResponse($ex->getMessage());
        }

    }

    public function changeStatus(SingleUser $request)
    {
        $user = User::changeStatus($request);
        // var_dump($user);exit;
        return Helper::successResponse('Status changed!', $user);

    }


    public function verify($token)
    {
        $verifyUser = VerifyUser::where('token', $token)->first();
        if (isset($verifyUser)) {
            $user = User::find($verifyUser->user_id);
            if (!$user->status) {
                $user->status = 1;
                $user->email_verified_at = time();
                if ($user->save()) {
                    sleep(3);
                    return redirect('https://aitaxonline.com/#/login/verified');
                }
                return 'Something went wrong';

            }
        }
    }

    public function sendResetToken(Request $request, $email)
    {

        $input =  $request->all();

        $user = User::where('email', $request->input('email'))->first();

        if(!$user){

            return response()->json([
                'success' => false,
                'message' => "We can't find a user with that e-mail address.",
            ], 401);
        }

        if(!empty($input['email'])){
            $this->validateEmail($request);
            $response = $this->broker()->sendResetLink(
                $request->only('email')
            );
        }

        return response()->json([
            'success' => true,
            'message' => 'We have e-mailed your password reset link!',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  array  $ids
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            User::where('id',$id)->delete();
            return Helper::successResponse('Users deleted successfylly.');
        } catch (\Exception $ex) {
            return Helper::serverErrorResponse($ex->getMessage());
        }
    }
}