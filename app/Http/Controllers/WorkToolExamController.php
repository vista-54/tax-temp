<?php

namespace App\Http\Controllers;

use App\Models\WorkToolExam;
use Illuminate\Http\Request;

class WorkToolExamController extends ApiController
{

    protected $model = WorkToolExam::class;

    public function store(Request $request)
    {
        $savedRecord = parent::store($request);
        $result = [];
        $savedRecord->removeAllChild();
        if ($savedRecord->professional_fees_radio == '1') {
            array_push($result, $savedRecord->addExpenseChild($request->input('professional_fees'), 'professional_fees'));
        }
        if ($savedRecord->membership_fees_radio == '1') {
            array_push($result, $savedRecord->addExpenseChild($request->input('membership_fees'), 'membership_fees'));
        } else {
            $savedRecord->removeAllChild();
            return $result;
        }
        return $result;
    }
}
