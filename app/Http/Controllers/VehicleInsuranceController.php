<?php

namespace App\Http\Controllers;

use App\Models\VehicleInsurance;

class VehicleInsuranceController extends ApiController
{
    protected $model = VehicleInsurance::class;
}
