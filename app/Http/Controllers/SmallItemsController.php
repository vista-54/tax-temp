<?php

namespace App\Http\Controllers;

use App\Models\SmallItems;

class SmallItemsController extends ApiController
{
    protected $model = SmallItems::class;
}
