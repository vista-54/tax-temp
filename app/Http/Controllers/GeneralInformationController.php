<?php

namespace App\Http\Controllers;

use App\Models\GeneralInformation;
use Illuminate\Http\Request;

class GeneralInformationController extends ApiController
{
    protected $model = GeneralInformation::class;
}
