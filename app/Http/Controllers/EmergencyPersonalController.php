<?php

namespace App\Http\Controllers;

use App\Models\EmergencyPersonal;
use Illuminate\Http\Request;

class EmergencyPersonalController extends ApiController
{
    protected $model = EmergencyPersonal::class;
}
