<?php

namespace App\Http\Controllers;

use App\Models\DependantInformation;
use Illuminate\Http\Request;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Auth;

class DependantController extends ApiController
{
    protected $model = DependantInformation::class;

    public function store(Request $request)
    {
        $savedRecord = parent::store($request);
        if ($savedRecord->have_dependents == '1') {
            return $savedRecord->addDependantChild($request->input('dependents'));
        } else {
            return $savedRecord;
        }
    }
}
