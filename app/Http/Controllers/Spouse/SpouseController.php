<?php

namespace App\Http\Controllers\Spouse;

use App\Helpers\Helper;
use App\Models\User\User;
use App\Models\UserSession;
use Illuminate\Http\Request;
use App\Models\Spouse\Spouse;
use App\Http\Requests\Spouse\AddRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
// use App\Http\Controllers\ApiController;


class SpouseController extends Controller
{

    
    protected $spouseObj = null;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Spouse $spouseObj)
    {
        $this->spouseObj = $spouseObj;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddRequest $request)
    {
        $userId = Auth::user()->id;
        $session = UserSession::where(['user_id' => $userId, 'status' => UserSession::STATUS_ACTIVE])->first();
        $mainparams = [
            'created_by' => $userId,
            'session_id' => $session->id
        ];
        return Spouse::updateOrCreate($mainparams, $request->post());
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $userId = Auth::user()->id;
            $spouseInfo = Spouse::where('created_by' , $userId)->first();
            if(!$spouseInfo){
                return Helper::notFoundResponse('We can\'t find a Spouse Information with that id.');
            }
            return Helper::successResponse('User Spouse Information.',$spouseInfo);
        } catch (\Exception $ex) {
            return Helper::serverErrorResponse($ex->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Spouse\Souse  $souse
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Souse $souse)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Spouse\Souse  $souse
     * @return \Illuminate\Http\Response
     */
    public function destroy(Souse $souse)
    {
        //
    }
}
