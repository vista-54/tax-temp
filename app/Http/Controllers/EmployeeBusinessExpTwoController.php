<?php

namespace App\Http\Controllers;

use App\Models\EmployeeBusinessExpTwo;
use Illuminate\Http\Request;

class EmployeeBusinessExpTwoController extends ApiController
{
    protected $model = EmployeeBusinessExpTwo::class;


    public function store(Request $request)
    {
        $savedRecord = parent::store($request);
        if ($savedRecord->ets_education_expenses_description_checkbox == '1') {
            return $savedRecord->addExpenseChild($request->input('certification_expenses'));
        } else {
            $savedRecord->removeAllChild();
            return $savedRecord;
        }
    }
}
