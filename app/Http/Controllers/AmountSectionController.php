<?php

namespace App\Http\Controllers;

use App\Models\AmountSection;
use App\Models\UserSession;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AmountSectionController extends ApiController
{
    protected $model = AmountSection::class;

    public function store(Request $request)
    {
        $userId = Auth::user()->id;
        $session = UserSession::where(['user_id' => $userId, 'status' => UserSession::STATUS_ACTIVE])->first();
        $mainparams = [
            'created_by' => $userId,
            'session_id' => $session->id,
        ];
        return $this->model::updateOrCreate(array_merge($mainparams, $request->post()), ['status' => AmountSection::AMOUNT_ACTIVE]);
    }

    public function update(Request $request)
    {
        $userId = Auth::user()->id;
        $session = UserSession::where(['user_id' => $userId, 'status' => UserSession::STATUS_ACTIVE])->orderby('id', 'DESC')->first();
        $model = AmountSection::where(['created_by' => $userId, 'session_id' => $session->id, 'status' => AmountSection::AMOUNT_ACTIVE, 'section_name' => $request->input('section_name')])->first();
        $model->status = AmountSection::AMOUNT_INACTIVE;
        return response(['status' => $model->save()]);
    }
}
