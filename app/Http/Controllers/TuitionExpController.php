<?php

namespace App\Http\Controllers;

use App\Models\TuituinExp;
use Illuminate\Http\Request;

class TuitionExpController extends ApiController
{
    protected $model = TuituinExp::class;

    public function store(Request $request)
    {
        $savedRecord = parent::store($request);
        if ($savedRecord->se_load_checkbox == '1') {
            return $savedRecord->addExpenseChild($request->input('loan_expenses'));
        } else {
            $savedRecord->removeAllChild();
            return $savedRecord;
        }
    }
}
