<?php

namespace App\Http\Controllers;

use App\Models\W2Form;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class W2FormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'w2image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        
        try{

            /** delete old */
            $w2forms = W2Form::where('created_by', Auth::id())->orderBy('created_at', 'desc')->get();
            if($w2forms){
                foreach($w2forms as $w2form){
                    Storage::delete($w2form->image_path);
                    $w2form->delete();
                }
            }
            
            $imageName = 'w2form-upload'.time().'.'.$request->w2image->getClientOriginalExtension();
            $request->w2image->move(storage_path('app/public'), $imageName);
            $w2form = W2Form::create(['created_by' => Auth::id(), 'image_path' => $imageName, 'is_submit' => $request->is_submit]);
            
            return response()->json(['status'=> true, 'message'=> 'Saved successfully.','data' => $w2form]);
        }catch(Exception $ex){
            return response()->json(['status'=> false, 'message' => $ex->getMessage()]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\W2Form  $w2Form
     * @return \Illuminate\Http\Response
     */
    public function show(W2Form $w2Form, $id)
    {
        $w2form = W2Form::where('created_by', Auth::id())->orderBy('created_at', 'desc')->first();
        $w2form->image_path = Storage::url($w2form->image_path);
        return response()->json(['status'=> true, 'message'=> 'Saved successfully.', 'data' => $w2form]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\W2Form  $w2Form
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, W2Form $w2Form)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\W2Form  $w2Form
     * @return \Illuminate\Http\Response
     */
    public function destroy(W2Form $w2Form)
    {
        //
    }
}
