<?php

namespace App\Http\Controllers;

use App\Models\AdditionalTransportation;
use Illuminate\Http\Request;

class AdditionalTranspostationQuestionController extends ApiController
{
    protected $model = AdditionalTransportation::class;

    public function store(Request $request)
    {
        $savedRecord = parent::store($request);
        if ($savedRecord->other_dist_checkbox == '1') {
            return $savedRecord->addExpenseChild($request->input('other'));
        } else {
            $savedRecord->removeAllChild();
            return $savedRecord;
        }
    }
}
