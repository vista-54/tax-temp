<?php

namespace App\Http\Controllers\OperatingAgreement;


use JWTAuth;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Requests\RegistrationFormRequest;
// use App\Http\Requests\OperatingAgreement\AddRequest;
use App\Models\OperatingAgreement\OperatingAgreement;


class OperatingAgreementController extends Controller
{
    protected $oprAgrObj = null;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(OperatingAgreement $oprAgrObj)
    {
        $this->oprAgrObj = $oprAgrObj;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $id = Auth::id();
            // var_dump($id);exit;
            $data_arr['result'] = json_encode($request->all());
            $operatingAgreement = $this->oprAgrObj->where('created_by',$id)->first();
            if($operatingAgreement != null){
                $data_arr['updated_by'] = $id;
                $operatingAgreement->update($data_arr); 
            }else{
                $data_arr['created_by'] = $id;
                // dd($data_arr);
                $operatingAgreement = OperatingAgreement::create($data_arr);
            }
            
            DB::commit();
            return Helper::successResponse('Operating Agreement saved successfully',$operatingAgreement);
        } catch (\Exception $ex) {
            DB::rollback();
            Log::info('error_while_operating_agreement' . $ex->getMessage());
            return Helper::serverErrorResponse($ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OperatingAgreement\OperatingAgreement  $operatingAgreement
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $operatingAgreement = $this->oprAgrObj->where('created_by',$id)->first();
            // var_dump($operatingAgreement);exit;
            if(!$operatingAgreement){
                return Helper::notFoundResponse('We can\'t find a Case Patient Insurance with that id.');
            }
            return Helper::successResponse('Single Case Patient Insurance Insormation.',$operatingAgreement->result);
        } catch (\Exception $ex) {
            return Helper::serverErrorResponse($ex->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OperatingAgreement\OperatingAgreement  $operatingAgreement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OperatingAgreement $operatingAgreement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OperatingAgreement\OperatingAgreement  $operatingAgreement
     * @return \Illuminate\Http\Response
     */
    public function destroy(OperatingAgreement $operatingAgreement)
    {
        //
    }
}
