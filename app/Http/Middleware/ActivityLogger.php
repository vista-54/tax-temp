<?php

namespace App\Http\Middleware;

use Closure;
use App\Helpers\Helper;

class ActivityLogger
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        $log = Helper::addToLog($request);
//        $request->request->add(['log_id' => $log->id]);
        $response =  $next($request);
//        $log->response_data = $response->original;
        // dd($log);
//        $log->save();
        return $response;
    }
}
