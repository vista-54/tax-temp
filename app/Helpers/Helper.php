<?php

namespace App\Helpers;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\ActivityLog\ActivityLog;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Models\OvadaCase\OvadaCase;


class Helper {

    public static function random_key(){
        $key = 'abcdefghijklmnopqrstuvwxyz0123456789';
        $string = '';
        $max = strlen($key) - 1;
        for ($i = 0; $i < 36; $i++) {
            $string .= $key[mt_rand(0, $max)];
        }
        return $string;
    }

    public static function successResponse($message, $data = [], $request = null)
    {
        if ($request != null) {
            if($request->has('pagination') && $request->input('pagination') == 1){
                $data = self::paginatedResponse($request->input('per_page'),$request->input('page'),$data);
            return response()->json(['status' => true, 'message' => $message, 'result' => $data],config('httpstatuscodes.ok_status'));
            }
            else{
                return response()->json(['status' => true, 'message' => $message, 'result' => ['data' => $data->get()]],config('httpstatuscodes.ok_status'));
            }
        } else {
            return response()->json(['status' => true, 'message' => $message, 'result' => ['data' => $data]],config('httpstatuscodes.ok_status'));
        }
    }
    
    public static function getSuccessResponse($message, $data = [], $request = null)
    {
        if ($request != null) {
            if($request->has('pagination') && $request->input('pagination') == 1){
                $data = self::paginatedResponse($request->input('per_page'),$request->input('page'),$data);
            return response()->json(['status' => true, 'message' => $message, 'result' => $data],config('httpstatuscodes.ok_status'));
            }
            else{
                return response()->json(['status' => true, 'message' => $message, 'data' => $data->get()],config('httpstatuscodes.ok_status'));
            }
        } else {
            return response()->json(['status' => true, 'message' => $message, 'data' => $data],config('httpstatuscodes.ok_status'));
        }
    }
    
    public static function paginatedResponse($per_page = 20, $page = 1, $object)
    {  
        $count =   $object->count();
        $object =  $object->take($per_page)->skip((($page - 1) * $per_page))->get();
        return ['total' => $count, 'data' => $object];       
    }

    public static function validationResponse($errors)
    {
        return response(['status' => false, 'message' => $errors], config('httpstatuscodes.not_acceptable_status'));
    }

    public static function unAuthResponse($errors)
    {
        return response(['status' => false, 'message' => $errors], config('httpstatuscodes.unauthorized_status'));
    }

    public static function serverErrorResponse($errors)
    {
        return response()->json(['status' => false, 'message' => $errors],config('httpstatuscodes.internal_server_error'));
    }
    
    public static function notFoundResponse($errors)
    {
        return response()->json(['status' => false, 'message' => $errors],config('httpstatuscodes.not_found_status'));
    }

    public static function addToLog($request,$subject="")
    {
        $log = [];
        $log['subject'] = $subject;
        $log['url'] = $request->fullUrl();
        $log['method'] = $request->method();
        $log['ip'] = $request->ip();
        $log['agent'] = $request->header('user-agent');
        $log['request_data'] =  json_encode($request->all());
        $log['response_data'] = "";
        // dd($log);
       return ActivityLog::create($log);
    }
}