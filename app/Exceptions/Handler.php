<?php

namespace App\Exceptions;

use App\Helpers\Helper;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        // dd($exception);exit;
        if ($exception) {
            // dd(Str::contains($request->path(), 'api'));
        
            // check if url from api
            if (Str::contains($request->path(), 'api')){

                // check is there any validation errors
                if($exception instanceOf ValidationException) {
                    return Helper::validationResponse($exception->validator->errors()->all());
                }
                if($exception instanceOf NotFoundHttpException) {
                    return Helper::notFoundResponse('Route not found.');
                }



                if ($exception instanceof UnauthorizedHttpException) {
                    // detect previous instance
                    if ($exception->getPrevious() instanceof TokenExpiredException) {
                        return Helper::unAuthResponse('Token has expired.');
                        // return response()->json(['error' => 'TOKEN_EXPIRED'], $exception->getStatusCode());
                    } else if ($exception->getPrevious() instanceof TokenInvalidException) {
                        return Helper::unAuthResponse('Token is invalid.');
                        // return response()->json(['error' => 'TOKEN_INVALID'], $exception->getStatusCode());
                    } else if ($exception->getPrevious() instanceof TokenBlacklistedException) {
                        return Helper::unAuthResponse('Token has been blacklisted.');
                        // return response()->json(['error' => 'TOKEN_BLACKLISTED'], $exception->getStatusCode());
                    } else {
                        return Helper::unAuthResponse('Unauthorized request.');
                        // return response()->json(['error' => "UNAUTHORIZED_REQUEST"], 401);
                    }
                }


                // if($exception instanceOf UnautorizedHttpException) {
                //     dd(123);
                //     return Helper::unAuthResponse('Token has expired.');
                // }
                // dd(11);
                // send errors in json request from api
                return Helper::serverErrorResponse($exception->getMessage());

            }
            return parent::render($request, $exception);
        }
        return parent::render($request, $exception);
    }
}
