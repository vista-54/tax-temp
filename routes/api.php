<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('User')->group(function () {
    Route::post('register', 'UserController@register');
    Route::post('login', 'UserController@login');
});

Route::post('/password/email', 'Auth\ForgotPasswordController@getResetToken');
Route::post('/reset/password', 'Auth\ResetPasswordController@callResetPassword');
Route::namespace('User')->group(function () {
    Route::get('user/verify/{token}', 'UserController@verify');
    Route::get('user/password/forgot/{email}', 'UserController@sendResetToken');
    Route::get('user/reset/password/{user}', 'UserController@sendResetToken');
});

Route::group(['middleware' => ['auth.jwt', 'logActivity']], function () {

    Route::namespace('User')->group(function () {
        Route::get('users', 'UserController@list');
        Route::delete('users/{id}', 'UserController@destroy');
        Route::get('logout', 'UserController@logout');
        Route::post('user/change-status', 'UserController@changeStatus');
    });

    Route::namespace('Spouse')->group(function () {
        Route::resource('spouses', 'SpouseController');
    });

    Route::namespace('OperatingAgreement')->group(function () {
        Route::resource('operating-agreement', 'OperatingAgreementController');
    });

    Route::namespace('Document')->group(function () {
        Route::get('operating-agreement-doc/{id}', 'DocumentController@operatingAgreement');
    });

    Route::namespace('Document')->group(function () {
        Route::post('aitax-online-doc', 'DocumentController@aiTaxOnlineDoc');
    });

    Route::namespace('Document')->group(function () {
        Route::get('get-aitax-online-doc/{id}', 'DocumentController@getAiTaxOnlineDoc');
    });

    Route::resource('personal', 'PersonalInfoController');
    Route::resource('user-additional-info', 'UserAdditionalInfoController');
    Route::resource('basic-info', 'BasicInformationController');
    Route::resource('dependant', 'DependantController');
    Route::resource('fbar', 'FbarController');
    Route::resource('rpi', 'RpiController');
    Route::resource('ga', 'GAController');
    Route::resource('charitable-donations', 'CharitableController');
    Route::resource('property-donations', 'PropertyController');
    Route::resource('vol-exp', 'VolunteeringExpController');
    Route::resource('tuition-rel-exp', 'TuitionRelExpController');
    Route::resource('tuition-exp', 'TuitionExpController');
    Route::resource('add-option-exp', 'AddOptionExpController');
    Route::resource('home-expense', 'HomeExpenseController');
    Route::resource('small-items', 'SmallItemsController');
    Route::resource('business-exp', 'BusinessExpController');
    Route::resource('vehicle-exp', 'VehicleExpController');
    Route::resource('vehicle-maintenance', 'VehicleMaintenanceController');
    Route::resource('vehicle-insurance', 'VehicleInsuranceController');
    Route::resource('entertainment-exp', 'EntertainmentController');
    Route::resource('employee-business-part1', 'EmployeeBusinessExpOneController');
    Route::resource('employee-business-part2', 'EmployeeBusinessExpTwoController');
    Route::resource('employee-business-part3', 'EmployeeBusinessExpThreeController');
    Route::resource('blue-collar-exp', 'BlueCollarExpController');
    Route::resource('blue-collar-exp-two', 'BlueCollarExpTwoController');
    Route::resource('emergency-personal', 'EmergencyPersonalController');
    Route::resource('emergency-personal-expenses', 'EmergencyPersonalExpenseController');
    Route::resource('pocket-medical-expenses', 'PocketMedicalExpController');
    Route::resource('pocket-medical-expenses-two', 'PocketPersonalTwoController');
    Route::resource('work-tool-exam', 'WorkToolExamController');
    Route::resource('work-tool-research', 'WorkToolResearchController');
    Route::resource('add-trans-question', 'AdditionalTranspostationQuestionController');
    Route::resource('deposit-information', 'DepositInfromationController');
    Route::resource('general-information', 'GeneralInformationController');
    Route::post('submit', 'SubmitController@finish');
    Route::post('unlock', 'SubmitController@unlock');
    Route::get('submit-status', 'SubmitController@getSubmitStatus');
    Route::resource('contact-us', 'ContactUsController');
    Route::resource('w2form-upload', 'W2FormController');
    Route::post('amount-section', 'AmountSectionController@store');
    Route::post('amount-section-update', 'AmountSectionController@update');

});
Route::get('generate-pdf/{user}', 'SubmitController@generatePdfByUser');

// Route::middleware('logActivity')->group(function(){
//     Route::namespace('TestLog')->group(function () {
//         Route::post('test-logs', 'TestLogController@index');
//     });
// });
